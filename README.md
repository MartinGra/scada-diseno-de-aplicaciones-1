# Commit Format: #

## Standard format ##
[action] ([type]): [optional_description]
  - [list_of_changes]

action = |Update, New, Delete, Fix|
type = |Doc, Code, UI|

## TDD format ##
Test <NameTest> [STATUS]

STATUS = |NOT COMPILING -> ERROR -> OK -> REFACTORING|