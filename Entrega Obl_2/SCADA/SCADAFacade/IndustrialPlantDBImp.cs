﻿using System;
using System.Collections.Generic;
using SCADADomain;
using SCADAPersistence.Domain_Handlers;
using SCADAPersistence.Domain_InterfaceHandlers;
using SCADAPersistence.Incident_Managers;

namespace SCADAFacade
{
    public class IndustrialPlantDBImp : IndustrialPlant
    {        
        private readonly IComponentHandler componentHandler;
        private readonly IDeviceTypeHandler deviceTypeHandler;
        private readonly IIncidentHandler incidentHandler;
        private readonly IControlVariableHandler controlVariableHandler;
        private static IndustrialPlantDBImp instance;

        public static IndustrialPlantDBImp ObtainInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new IndustrialPlantDBImp();
                }
                return instance;
            }
        }

        public ICollection<Component> Components
        {
            get
            {
                return componentHandler.GetComponents();
            }
            set
            {
            }
        }

        public ICollection<Component> Plants
        {
            get
            {
                return componentHandler.GetPlants();
            }
            set
            {
            }
        }

        public ICollection<DeviceType> DeviceTypes
        {
            get
            {
                return deviceTypeHandler.GetDeviceTypes();
            }
            set
            {
            }
        }

        public ICollection<Component> Devices
        {
            get
            {
                return componentHandler.GetDevices();
            }
            set
            {
            }
        }

        public ICollection<Component> Installations
        {
            get
            {
                return componentHandler.GetInstallations();
            }
            set
            {             
            }
        }

        private IndustrialPlantDBImp()
        {
            componentHandler = ComponentHandler.ObtainInstance;
            deviceTypeHandler = DeviceTypeHandler.ObtainInstance;
            incidentHandler = IncidentHandler.ObtainInstance;
            controlVariableHandler = ControlVariableHandler.ObtainInstance;
        }

        public void CreatePlant(string name, string city, string address)
        {
            componentHandler.CreatePlant(name, city, address);
        }

        public void CreateInstallation(string newInstallationName, Component selectedComponent)
        {
            componentHandler.CreateInstallation(newInstallationName, selectedComponent);
        }

        public void CreateDeviceType(string name, string description)
        {
            deviceTypeHandler.CreateDeviceType(name, description);
        }

        public void CreateDevice(string newDeviceName, DeviceType selectedType)
        {
            componentHandler.CreateDevice(newDeviceName, selectedType);
        }

        public void AssignChildToFatherHierarchy(Component child, Component father)
        {
            componentHandler.AssignChildToFatherHierarchy(child, father);
        }

        public void CreateControlVariableInComponent(Component component, string nameVariable, float minValueOfVariable, float maxValueOfVariable)
        {
            controlVariableHandler.CreateControlVariableInComponent(component, nameVariable, minValueOfVariable, maxValueOfVariable);
        }
        public int GetTotalAlarms(Component comp)
        {
           return componentHandler.GetTotalAlarms(comp);
        }
        public void ControlVariableAssignValueInComponent(Component component, ControlVariable controlVariable, float newControlVariableValue)
        {
            controlVariableHandler.ControlVariableAssignValueInComponent(component, controlVariable, newControlVariableValue);
        }
        public ICollection<Component> Hierarchy(Component comp)
        {
            return componentHandler.Hierarchy(comp);
        }

        public void ModifyInstallationName(Component installationToModify, string newInstallationName)
        {
            componentHandler.ModifyInstallationName(installationToModify, newInstallationName);
        }

        public void ModifyDeviceName(Component deviceToModify, string newDeviceName)
        {
            componentHandler.ModifyDeviceName(deviceToModify, newDeviceName);        
        }

        public void ModifyDeviceType(DeviceType deviceTypeToModify, string newDeviceTypeName, string newDeviceTypeDescription)
        {
            deviceTypeHandler.ModifyDeviceType(deviceTypeToModify, newDeviceTypeName, newDeviceTypeDescription);
        }

        public void ModifyControlVariable(Component component, ControlVariable variable, string name, float minRange, float maxRange)
        {
            controlVariableHandler.ModifyControlVariable(component, variable, name, minRange, maxRange);
        }

        public void EraseDevice(Component component)
        {
            componentHandler.EraseDevice(component);
        }

        public void EraseControlVariable(Component component, ControlVariable controlVariable)
        {
            controlVariableHandler.EraseControlVariable(component, controlVariable);
        }

        public void EraseInstallation(Component component)
        {
            componentHandler.EraseInstallation(component);
        }

        public void EraseDeviceType(DeviceType deviceType)
        {
            deviceTypeHandler.EraseDeviceType(deviceType);
        }

        public void ErasePlant(Component component)
        {
            componentHandler.ErasePlant(component);
        }

        public void CreateIncident(string severity, string description, Component comp, IncidentManager storage)
        {
            incidentHandler.CreateIncident(severity, description, comp, storage);
        }

        public List<string> IncidentReports(Component comp, IncidentManager storage)
        {
            return incidentHandler.GetReports(comp, storage);
        }

        public void ModifyPlant(Component component, string name)
        {
            componentHandler.ModifyPlant(component, name);
        }

        public void CreateControlVariableWithWarningRanges(Component comp, string name, float minAlarm, float maxAlarm, float minWarning, float maxWarning)
        {
            controlVariableHandler.CreateControlVariableWithWarningRanges(comp, name, minAlarm, maxAlarm, minWarning, maxWarning);
        }

        public void ModifyControlVariableWithWarning(Component comp, ControlVariable variable, string name, float minAlarm, float maxAlarm, float minWarning, float maxWarning)
        {
            controlVariableHandler.ModifyControlVariableWithWarning(comp, variable, name, minAlarm, maxAlarm, minWarning, maxWarning);
        }

        public ICollection<AssignValue> HistoricValues(Component component, ControlVariable variable)
        {
            return componentHandler.HistoricValues(component, variable);
        }

        public int GetTotalWarnings(Component comp)
        {
            return componentHandler.GetTotalWarnings(comp);
        }
    }
}