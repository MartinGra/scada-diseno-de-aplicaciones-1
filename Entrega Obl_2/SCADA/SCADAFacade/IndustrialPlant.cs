﻿using System.Collections.Generic;
using SCADADomain;
using SCADAPersistence.Incident_Managers;

namespace SCADAFacade
{
    public interface IndustrialPlant
    {
        ICollection<Component> Components { get; set; }
        ICollection<Component> Plants { get; set; }
        ICollection<Component> Installations { get; set; }
        ICollection<DeviceType> DeviceTypes { get; set; }        
        ICollection<Component> Devices { get; set; }

        void CreatePlant(string name, string city, string address);

        void CreateInstallation(string newInstallationName, Component selectedComponent);

        void CreateDeviceType(string name, string description);

        void CreateDevice(string newDeviceName, DeviceType selectedType);

        void AssignChildToFatherHierarchy(Component child, Component father);

        void CreateControlVariableInComponent(Component component, string nameVariable, float minValueOfVariable, float maxValueOfVariable);

        void ControlVariableAssignValueInComponent(Component component, ControlVariable controlVariable, float newControlVariableValue);

        ICollection<Component> Hierarchy(Component comp);

        int GetTotalAlarms(Component comp);    

        void ModifyInstallationName(Component installationToModify, string newInstallationName);

        void ModifyDeviceName(Component deviceToModify, string newDeviceName);

        void ModifyDeviceType(DeviceType deviceTypeToModify, string newDeviceTypeName, string newDeviceTypeDescription);

        void ModifyControlVariable(Component component, ControlVariable variable, string name, float minRange, float maxRange);

        void EraseDevice(Component component);

        void EraseControlVariable(Component component, ControlVariable controlVariable);

        void EraseInstallation(Component component);

        void EraseDeviceType(DeviceType deviceType);

        void ErasePlant(Component component);

        void CreateIncident(string severity, string description, Component comp, IncidentManager storage);

        List<string> IncidentReports(Component comp, IncidentManager storage);

        void ModifyPlant(Component component, string name);
        void CreateControlVariableWithWarningRanges(Component comp, string name, float minAlarm, float maxAlarm, float minWarning, float maxWarning);
        void ModifyControlVariableWithWarning(Component comp, ControlVariable variable, string name, float minAlarm, float maxAlarm, float minWarning, float maxWarning);
        ICollection<AssignValue> HistoricValues(Component component, ControlVariable variable);
        int GetTotalWarnings(Component comp);
    }
}