﻿using System;

namespace SCADA
{
    partial class UISCADA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UISCADA));
            this.panelMainMenu = new System.Windows.Forms.Panel();
            this.ScadaMenu = new System.Windows.Forms.ToolStrip();
            this.PlantMenu = new System.Windows.Forms.ToolStripDropDownButton();
            this.createPlantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updatePlantStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removePlantStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.InstallationMenu = new System.Windows.Forms.ToolStripDropDownButton();
            this.createInstallationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateInstallationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeInstallationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeviceTypeMenu = new System.Windows.Forms.ToolStripDropDownButton();
            this.createDeviceTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateDeviceTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeDeviceTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeviceMenu = new System.Windows.Forms.ToolStripDropDownButton();
            this.createDeviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateDeviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeDeviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.removeControlVariableMenu = new System.Windows.Forms.ToolStripDropDownButton();
            this.createControlVariableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateControlVariableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeControlVariableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inputValueControlVariableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historicalValueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.InicidentMenu = new System.Windows.Forms.ToolStripDropDownButton();
            this.createIncidentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportIncidentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hierarchyManagmenttoolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.panelAlarmStatus = new System.Windows.Forms.Panel();
            this.treeHierarchy = new System.Windows.Forms.TreeView();
            this.labelAlarmsStatus = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.ScadaMenu.SuspendLayout();
            this.panelAlarmStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMainMenu
            // 
            this.panelMainMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelMainMenu.AutoSize = true;
            this.panelMainMenu.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelMainMenu.Location = new System.Drawing.Point(317, 32);
            this.panelMainMenu.Name = "panelMainMenu";
            this.panelMainMenu.Size = new System.Drawing.Size(560, 517);
            this.panelMainMenu.TabIndex = 0;
            // 
            // ScadaMenu
            // 
            this.ScadaMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PlantMenu,
            this.InstallationMenu,
            this.DeviceTypeMenu,
            this.DeviceMenu,
            this.toolStripLabel2,
            this.removeControlVariableMenu,
            this.InicidentMenu,
            this.hierarchyManagmenttoolStripLabel1,
            this.toolStripSeparator1});
            this.ScadaMenu.Location = new System.Drawing.Point(0, 0);
            this.ScadaMenu.Name = "ScadaMenu";
            this.ScadaMenu.Size = new System.Drawing.Size(889, 25);
            this.ScadaMenu.TabIndex = 1;
            this.ScadaMenu.Text = "toolStrip1";
            // 
            // PlantMenu
            // 
            this.PlantMenu.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.PlantMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createPlantToolStripMenuItem,
            this.updatePlantStripMenuItem,
            this.removePlantStripMenuItem});
            this.PlantMenu.Image = ((System.Drawing.Image)(resources.GetObject("PlantMenu.Image")));
            this.PlantMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.PlantMenu.Name = "PlantMenu";
            this.PlantMenu.Size = new System.Drawing.Size(47, 22);
            this.PlantMenu.Text = "Plant";
            this.PlantMenu.ToolTipText = "Plant";
            // 
            // createPlantToolStripMenuItem
            // 
            this.createPlantToolStripMenuItem.Name = "createPlantToolStripMenuItem";
            this.createPlantToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.createPlantToolStripMenuItem.Text = "Create";
            this.createPlantToolStripMenuItem.Click += new System.EventHandler(this.createPlantToolStripMenuItem_Click_1);
            // 
            // updatePlantStripMenuItem
            // 
            this.updatePlantStripMenuItem.Name = "updatePlantStripMenuItem";
            this.updatePlantStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.updatePlantStripMenuItem.Text = "Update";
            this.updatePlantStripMenuItem.Click += new System.EventHandler(this.updatePlantStripMenuItem_Click);
            // 
            // removePlantStripMenuItem
            // 
            this.removePlantStripMenuItem.Name = "removePlantStripMenuItem";
            this.removePlantStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.removePlantStripMenuItem.Text = "Remove";
            this.removePlantStripMenuItem.Click += new System.EventHandler(this.removePlantStripMenuItem_Click);
            // 
            // InstallationMenu
            // 
            this.InstallationMenu.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.InstallationMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createInstallationToolStripMenuItem,
            this.updateInstallationToolStripMenuItem,
            this.removeInstallationToolStripMenuItem});
            this.InstallationMenu.Image = ((System.Drawing.Image)(resources.GetObject("InstallationMenu.Image")));
            this.InstallationMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.InstallationMenu.Name = "InstallationMenu";
            this.InstallationMenu.Size = new System.Drawing.Size(78, 22);
            this.InstallationMenu.Text = "Installation";
            this.InstallationMenu.ToolTipText = "Installation Menu";
            // 
            // createInstallationToolStripMenuItem
            // 
            this.createInstallationToolStripMenuItem.Name = "createInstallationToolStripMenuItem";
            this.createInstallationToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.createInstallationToolStripMenuItem.Text = "Create";
            this.createInstallationToolStripMenuItem.Click += new System.EventHandler(this.createPlantToolStripMenuItem_Click);
            // 
            // updateInstallationToolStripMenuItem
            // 
            this.updateInstallationToolStripMenuItem.Name = "updateInstallationToolStripMenuItem";
            this.updateInstallationToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.updateInstallationToolStripMenuItem.Text = "Update";
            this.updateInstallationToolStripMenuItem.Click += new System.EventHandler(this.updatePlantToolStripMenuItem_Click);
            // 
            // removeInstallationToolStripMenuItem
            // 
            this.removeInstallationToolStripMenuItem.Name = "removeInstallationToolStripMenuItem";
            this.removeInstallationToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.removeInstallationToolStripMenuItem.Text = "Remove";
            this.removeInstallationToolStripMenuItem.Click += new System.EventHandler(this.removePlantToolStripMenuItem_Click);
            // 
            // DeviceTypeMenu
            // 
            this.DeviceTypeMenu.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.DeviceTypeMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createDeviceTypeToolStripMenuItem,
            this.updateDeviceTypeToolStripMenuItem,
            this.removeDeviceTypeToolStripMenuItem});
            this.DeviceTypeMenu.Image = ((System.Drawing.Image)(resources.GetObject("DeviceTypeMenu.Image")));
            this.DeviceTypeMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DeviceTypeMenu.Name = "DeviceTypeMenu";
            this.DeviceTypeMenu.Size = new System.Drawing.Size(83, 22);
            this.DeviceTypeMenu.Text = "Device Type";
            this.DeviceTypeMenu.ToolTipText = "Device Type Menu";
            // 
            // createDeviceTypeToolStripMenuItem
            // 
            this.createDeviceTypeToolStripMenuItem.Name = "createDeviceTypeToolStripMenuItem";
            this.createDeviceTypeToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.createDeviceTypeToolStripMenuItem.Text = "Create";
            this.createDeviceTypeToolStripMenuItem.Click += new System.EventHandler(this.createDeviceTypeToolStripMenuItem_Click);
            // 
            // updateDeviceTypeToolStripMenuItem
            // 
            this.updateDeviceTypeToolStripMenuItem.Name = "updateDeviceTypeToolStripMenuItem";
            this.updateDeviceTypeToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.updateDeviceTypeToolStripMenuItem.Text = "Update";
            this.updateDeviceTypeToolStripMenuItem.Click += new System.EventHandler(this.updateDeviceTypeToolStripMenuItem_Click);
            // 
            // removeDeviceTypeToolStripMenuItem
            // 
            this.removeDeviceTypeToolStripMenuItem.Name = "removeDeviceTypeToolStripMenuItem";
            this.removeDeviceTypeToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.removeDeviceTypeToolStripMenuItem.Text = "Remove";
            this.removeDeviceTypeToolStripMenuItem.Click += new System.EventHandler(this.removeDeviceTypeToolStripMenuItem_Click);
            // 
            // DeviceMenu
            // 
            this.DeviceMenu.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.DeviceMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createDeviceToolStripMenuItem,
            this.updateDeviceToolStripMenuItem,
            this.removeDeviceToolStripMenuItem});
            this.DeviceMenu.Image = ((System.Drawing.Image)(resources.GetObject("DeviceMenu.Image")));
            this.DeviceMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DeviceMenu.Name = "DeviceMenu";
            this.DeviceMenu.Size = new System.Drawing.Size(55, 22);
            this.DeviceMenu.Text = "Device";
            // 
            // createDeviceToolStripMenuItem
            // 
            this.createDeviceToolStripMenuItem.Name = "createDeviceToolStripMenuItem";
            this.createDeviceToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.createDeviceToolStripMenuItem.Text = "Create";
            this.createDeviceToolStripMenuItem.Click += new System.EventHandler(this.createDeviceToolStripMenuItem_Click);
            // 
            // updateDeviceToolStripMenuItem
            // 
            this.updateDeviceToolStripMenuItem.Name = "updateDeviceToolStripMenuItem";
            this.updateDeviceToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.updateDeviceToolStripMenuItem.Text = "Update";
            this.updateDeviceToolStripMenuItem.Click += new System.EventHandler(this.updateDeviceToolStripMenuItem_Click);
            // 
            // removeDeviceToolStripMenuItem
            // 
            this.removeDeviceToolStripMenuItem.Name = "removeDeviceToolStripMenuItem";
            this.removeDeviceToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.removeDeviceToolStripMenuItem.Text = "Remove";
            this.removeDeviceToolStripMenuItem.Click += new System.EventHandler(this.removeDeviceToolStripMenuItem_Click);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(0, 22);
            // 
            // removeControlVariableMenu
            // 
            this.removeControlVariableMenu.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.removeControlVariableMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createControlVariableToolStripMenuItem,
            this.updateControlVariableToolStripMenuItem,
            this.removeControlVariableToolStripMenuItem,
            this.inputValueControlVariableToolStripMenuItem,
            this.historicalValueToolStripMenuItem});
            this.removeControlVariableMenu.Image = ((System.Drawing.Image)(resources.GetObject("removeControlVariableMenu.Image")));
            this.removeControlVariableMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.removeControlVariableMenu.Name = "removeControlVariableMenu";
            this.removeControlVariableMenu.Size = new System.Drawing.Size(104, 22);
            this.removeControlVariableMenu.Text = "Control Variable";
            // 
            // createControlVariableToolStripMenuItem
            // 
            this.createControlVariableToolStripMenuItem.Name = "createControlVariableToolStripMenuItem";
            this.createControlVariableToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.createControlVariableToolStripMenuItem.Text = "Create";
            this.createControlVariableToolStripMenuItem.Click += new System.EventHandler(this.createControlVariableToolStripMenuItem_Click);
            // 
            // updateControlVariableToolStripMenuItem
            // 
            this.updateControlVariableToolStripMenuItem.Name = "updateControlVariableToolStripMenuItem";
            this.updateControlVariableToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.updateControlVariableToolStripMenuItem.Text = "Update";
            this.updateControlVariableToolStripMenuItem.Click += new System.EventHandler(this.updateControlVariableToolStripMenuItem_Click);
            // 
            // removeControlVariableToolStripMenuItem
            // 
            this.removeControlVariableToolStripMenuItem.Name = "removeControlVariableToolStripMenuItem";
            this.removeControlVariableToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.removeControlVariableToolStripMenuItem.Text = "Remove";
            this.removeControlVariableToolStripMenuItem.Click += new System.EventHandler(this.removeControlVariableToolStripMenuItem_Click);
            // 
            // inputValueControlVariableToolStripMenuItem
            // 
            this.inputValueControlVariableToolStripMenuItem.Name = "inputValueControlVariableToolStripMenuItem";
            this.inputValueControlVariableToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.inputValueControlVariableToolStripMenuItem.Text = "Input Value";
            this.inputValueControlVariableToolStripMenuItem.Click += new System.EventHandler(this.inputValueControlVariableToolStripMenuItem_Click);
            // 
            // historicalValueToolStripMenuItem
            // 
            this.historicalValueToolStripMenuItem.Name = "historicalValueToolStripMenuItem";
            this.historicalValueToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.historicalValueToolStripMenuItem.Text = "Historical Value";
            this.historicalValueToolStripMenuItem.Click += new System.EventHandler(this.historicalValueToolStripMenuItem_Click);
            // 
            // InicidentMenu
            // 
            this.InicidentMenu.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.InicidentMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createIncidentToolStripMenuItem,
            this.reportIncidentToolStripMenuItem});
            this.InicidentMenu.Image = ((System.Drawing.Image)(resources.GetObject("InicidentMenu.Image")));
            this.InicidentMenu.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.InicidentMenu.Name = "InicidentMenu";
            this.InicidentMenu.Size = new System.Drawing.Size(63, 22);
            this.InicidentMenu.Text = "Incident";
            // 
            // createIncidentToolStripMenuItem
            // 
            this.createIncidentToolStripMenuItem.Name = "createIncidentToolStripMenuItem";
            this.createIncidentToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.createIncidentToolStripMenuItem.Text = "Create";
            this.createIncidentToolStripMenuItem.Click += new System.EventHandler(this.createIncidentToolStripMenuItem_Click);
            // 
            // reportIncidentToolStripMenuItem
            // 
            this.reportIncidentToolStripMenuItem.Name = "reportIncidentToolStripMenuItem";
            this.reportIncidentToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.reportIncidentToolStripMenuItem.Text = "Report";
            this.reportIncidentToolStripMenuItem.Click += new System.EventHandler(this.reportIncidentToolStripMenuItem_Click);
            // 
            // hierarchyManagmenttoolStripLabel1
            // 
            this.hierarchyManagmenttoolStripLabel1.Name = "hierarchyManagmenttoolStripLabel1";
            this.hierarchyManagmenttoolStripLabel1.Size = new System.Drawing.Size(126, 22);
            this.hierarchyManagmenttoolStripLabel1.Text = "Hierarchy Managment";
            this.hierarchyManagmenttoolStripLabel1.Click += new System.EventHandler(this.hierarchyManagmenttoolStripLabel1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // panelAlarmStatus
            // 
            this.panelAlarmStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panelAlarmStatus.AutoScroll = true;
            this.panelAlarmStatus.BackColor = System.Drawing.SystemColors.HighlightText;
            this.panelAlarmStatus.Controls.Add(this.treeHierarchy);
            this.panelAlarmStatus.Controls.Add(this.labelAlarmsStatus);
            this.panelAlarmStatus.Font = new System.Drawing.Font("Segoe UI Historic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelAlarmStatus.Location = new System.Drawing.Point(2, 32);
            this.panelAlarmStatus.Name = "panelAlarmStatus";
            this.panelAlarmStatus.Size = new System.Drawing.Size(309, 517);
            this.panelAlarmStatus.TabIndex = 3;
            // 
            // treeHierarchy
            // 
            this.treeHierarchy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.treeHierarchy.Location = new System.Drawing.Point(4, 30);
            this.treeHierarchy.Name = "treeHierarchy";
            this.treeHierarchy.Size = new System.Drawing.Size(303, 484);
            this.treeHierarchy.TabIndex = 1;
            // 
            // labelAlarmsStatus
            // 
            this.labelAlarmsStatus.AutoSize = true;
            this.labelAlarmsStatus.Location = new System.Drawing.Point(3, 14);
            this.labelAlarmsStatus.Name = "labelAlarmsStatus";
            this.labelAlarmsStatus.Size = new System.Drawing.Size(109, 13);
            this.labelAlarmsStatus.TabIndex = 0;
            this.labelAlarmsStatus.Text = "Production Plant";
            // 
            // UISCADA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(889, 561);
            this.Controls.Add(this.panelAlarmStatus);
            this.Controls.Add(this.ScadaMenu);
            this.Controls.Add(this.panelMainMenu);
            this.Font = new System.Drawing.Font("Segoe UI Historic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "UISCADA";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds;
            this.Text = "SCADA";
            this.ScadaMenu.ResumeLayout(false);
            this.ScadaMenu.PerformLayout();
            this.panelAlarmStatus.ResumeLayout(false);
            this.panelAlarmStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelMainMenu;
        private System.Windows.Forms.ToolStrip ScadaMenu;
        private System.Windows.Forms.ToolStripDropDownButton InstallationMenu;
        private System.Windows.Forms.ToolStripMenuItem createInstallationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeInstallationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateInstallationToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton DeviceTypeMenu;
        private System.Windows.Forms.ToolStripMenuItem createDeviceTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateDeviceTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeDeviceTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton DeviceMenu;
        private System.Windows.Forms.ToolStripMenuItem createDeviceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateDeviceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeDeviceToolStripMenuItem;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripDropDownButton removeControlVariableMenu;
        private System.Windows.Forms.ToolStripMenuItem createControlVariableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateControlVariableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeControlVariableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inputValueControlVariableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem historicalValueToolStripMenuItem;
        private System.Windows.Forms.ToolStripLabel hierarchyManagmenttoolStripLabel1;
        private System.Windows.Forms.Panel panelAlarmStatus;
        private System.Windows.Forms.Label labelAlarmsStatus;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripDropDownButton PlantMenu;
        private System.Windows.Forms.ToolStripMenuItem createPlantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updatePlantStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removePlantStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton InicidentMenu;
        private System.Windows.Forms.ToolStripMenuItem createIncidentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportIncidentToolStripMenuItem;
        private System.Windows.Forms.TreeView treeHierarchy;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}

