﻿using System.Windows.Forms;
using SCADADomain;
using System;
using SCADAFacade;
using static System.String;

namespace SCADA
{
    internal class UCCreatePlant : UserControl
    {
        private IndustrialPlant scada;
        private Button buttonRegisterPlant;
        private TextBox textBoxName;
        private TextBox textBoxCity;
        private RichTextBox richTextBoxAddress;
        private Label labelName;
        private Label labelCreatePlant;
        private Label labelCity;
        private Label labelAdress;
        private UISCADA uISCADA;

        public UCCreatePlant(IndustrialPlant scada, UISCADA uISCADA)
        {
            this.scada = scada;
            this.uISCADA = uISCADA;
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.buttonRegisterPlant = new System.Windows.Forms.Button();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxCity = new System.Windows.Forms.TextBox();
            this.richTextBoxAddress = new System.Windows.Forms.RichTextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.labelCreatePlant = new System.Windows.Forms.Label();
            this.labelCity = new System.Windows.Forms.Label();
            this.labelAdress = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonRegisterPlant
            // 
            this.buttonRegisterPlant.Location = new System.Drawing.Point(193, 259);
            this.buttonRegisterPlant.Name = "buttonRegisterPlant";
            this.buttonRegisterPlant.Size = new System.Drawing.Size(75, 23);
            this.buttonRegisterPlant.TabIndex = 0;
            this.buttonRegisterPlant.Text = "Register";
            this.buttonRegisterPlant.UseVisualStyleBackColor = true;
            this.buttonRegisterPlant.Click += new System.EventHandler(this.buttonRegisterPlant_Click);
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(168, 91);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(100, 20);
            this.textBoxName.TabIndex = 1;
            // 
            // textBoxCity
            // 
            this.textBoxCity.Location = new System.Drawing.Point(168, 131);
            this.textBoxCity.Name = "textBoxCity";
            this.textBoxCity.Size = new System.Drawing.Size(100, 20);
            this.textBoxCity.TabIndex = 2;
            // 
            // richTextBoxAddress
            // 
            this.richTextBoxAddress.Location = new System.Drawing.Point(168, 174);
            this.richTextBoxAddress.Name = "richTextBoxAddress";
            this.richTextBoxAddress.Size = new System.Drawing.Size(203, 41);
            this.richTextBoxAddress.TabIndex = 3;
            this.richTextBoxAddress.Text = "";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(106, 94);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 4;
            this.labelName.Text = "Name";
            // 
            // labelCreatePlant
            // 
            this.labelCreatePlant.AutoSize = true;
            this.labelCreatePlant.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCreatePlant.Location = new System.Drawing.Point(194, 41);
            this.labelCreatePlant.Name = "labelCreatePlant";
            this.labelCreatePlant.Size = new System.Drawing.Size(77, 13);
            this.labelCreatePlant.TabIndex = 5;
            this.labelCreatePlant.Text = "Create Plant";
            // 
            // labelCity
            // 
            this.labelCity.AutoSize = true;
            this.labelCity.Location = new System.Drawing.Point(106, 131);
            this.labelCity.Name = "labelCity";
            this.labelCity.Size = new System.Drawing.Size(24, 13);
            this.labelCity.TabIndex = 6;
            this.labelCity.Text = "City";
            // 
            // labelAdress
            // 
            this.labelAdress.AutoSize = true;
            this.labelAdress.Location = new System.Drawing.Point(106, 174);
            this.labelAdress.Name = "labelAdress";
            this.labelAdress.Size = new System.Drawing.Size(45, 13);
            this.labelAdress.TabIndex = 7;
            this.labelAdress.Text = "Address";
            // 
            // UCCreatePlant
            // 
            this.Controls.Add(this.labelAdress);
            this.Controls.Add(this.labelCity);
            this.Controls.Add(this.labelCreatePlant);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.textBoxCity);
            this.Controls.Add(this.richTextBoxAddress);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.buttonRegisterPlant);
            this.Name = "UCCreatePlant";
            this.Size = new System.Drawing.Size(475, 395);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void buttonRegisterPlant_Click(object sender, System.EventArgs e)
        {
            try
            {
                string name = textBoxName.Text;
                string city = textBoxCity.Text;
                string address = richTextBoxAddress.Text;
                scada.CreatePlant(name, city, address);
                MessageBox.Show("Success: new plant registered");
                uISCADA.LoadControlPanel();
                EmptyFields();
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
            
        }

        private void EmptyFields()
        {
            textBoxName.Text = Empty;
            textBoxCity.Text = Empty;
            richTextBoxAddress.Text = Empty;
        }
    }
}