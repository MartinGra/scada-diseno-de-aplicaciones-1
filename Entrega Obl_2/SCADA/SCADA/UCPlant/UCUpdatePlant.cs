﻿using System;
using System.Windows.Forms;
using SCADADomain;
using SCADAFacade;

namespace SCADA
{
    internal class UCUpdatePlant : UserControl
    {
        private IndustrialPlant scada;
        private Label labelUpdateInstallation;
        private Label labelSelectInstallation;
        private ListBox listBoxPlants;
        private Button buttonUpdate;
        private TextBox textNewName;
        private Label labelName;
        private Label labelUpdateInstallationData;
        private UISCADA uISCADA;

        public UCUpdatePlant(IndustrialPlant scada, UISCADA uISCADA)
        {
            this.scada = scada;
            this.uISCADA = uISCADA;
            InitializeComponent();
            listBoxPlants.DataSource = scada.Plants;
            listBoxPlants.DisplayMember = "Name";
        }

        private void EmptyFields()
        {
            listBoxPlants.DataSource = null;
            listBoxPlants.DataSource = scada.Plants;
            listBoxPlants.DisplayMember = "Name";
            uISCADA.LoadControlPanel();
        }

        private void InitializeComponent()
        {
            this.labelUpdateInstallation = new System.Windows.Forms.Label();
            this.labelSelectInstallation = new System.Windows.Forms.Label();
            this.listBoxPlants = new System.Windows.Forms.ListBox();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.textNewName = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.labelUpdateInstallationData = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelUpdateInstallation
            // 
            this.labelUpdateInstallation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelUpdateInstallation.AutoSize = true;
            this.labelUpdateInstallation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUpdateInstallation.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelUpdateInstallation.Location = new System.Drawing.Point(182, 38);
            this.labelUpdateInstallation.Name = "labelUpdateInstallation";
            this.labelUpdateInstallation.Size = new System.Drawing.Size(81, 13);
            this.labelUpdateInstallation.TabIndex = 22;
            this.labelUpdateInstallation.Text = "Update Plant";
            this.labelUpdateInstallation.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelSelectInstallation
            // 
            this.labelSelectInstallation.AutoSize = true;
            this.labelSelectInstallation.Location = new System.Drawing.Point(49, 80);
            this.labelSelectInstallation.Name = "labelSelectInstallation";
            this.labelSelectInstallation.Size = new System.Drawing.Size(64, 13);
            this.labelSelectInstallation.TabIndex = 21;
            this.labelSelectInstallation.Text = "Select Plant";
            // 
            // listBoxPlants
            // 
            this.listBoxPlants.FormattingEnabled = true;
            this.listBoxPlants.Location = new System.Drawing.Point(47, 105);
            this.listBoxPlants.Name = "listBoxPlants";
            this.listBoxPlants.Size = new System.Drawing.Size(120, 95);
            this.listBoxPlants.TabIndex = 20;
            this.listBoxPlants.SelectedIndexChanged += new System.EventHandler(this.listBoxPlants_SelectedIndexChanged);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(277, 177);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdate.TabIndex = 19;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click_1);
            // 
            // textNewName
            // 
            this.textNewName.Location = new System.Drawing.Point(271, 133);
            this.textNewName.Name = "textNewName";
            this.textNewName.Size = new System.Drawing.Size(100, 20);
            this.textNewName.TabIndex = 18;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(216, 133);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 17;
            this.labelName.Text = "Name";
            // 
            // labelUpdateInstallationData
            // 
            this.labelUpdateInstallationData.AutoSize = true;
            this.labelUpdateInstallationData.Location = new System.Drawing.Point(268, 80);
            this.labelUpdateInstallationData.Name = "labelUpdateInstallationData";
            this.labelUpdateInstallationData.Size = new System.Drawing.Size(58, 13);
            this.labelUpdateInstallationData.TabIndex = 16;
            this.labelUpdateInstallationData.Text = "Plant data:";
            // 
            // UCUpdatePlant
            // 
            this.Controls.Add(this.labelUpdateInstallation);
            this.Controls.Add(this.labelSelectInstallation);
            this.Controls.Add(this.listBoxPlants);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.textNewName);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.labelUpdateInstallationData);
            this.Name = "UCUpdatePlant";
            this.Size = new System.Drawing.Size(458, 367);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void buttonUpdate_Click_1(object sender, EventArgs e)
        {
            try
            {
                scada.ModifyPlant((Component)listBoxPlants.SelectedItem, textNewName.Text);
                MessageBox.Show("Correctly Updated: Plant");
                uISCADA.LoadControlPanel();
                EmptyFields();
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

        private void listBoxPlants_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.textNewName.Text = ((Component)this.listBoxPlants.SelectedItem).Name;
            }
            catch (Exception)
            {
            }
        }
    }
}