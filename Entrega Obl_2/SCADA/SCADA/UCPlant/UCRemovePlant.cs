﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using SCADADomain;
using SCADAFacade;

namespace SCADA
{
    internal class UCRemovePlant : UserControl
    {
        private IndustrialPlant scada;
        private Label labelRemoveInstallation;
        private Button buttonRemovePlant;
        private ListBox listPlants;
        private Label labelListOfInstallation;

        private BindingList<SCADADomain.Component> plantsBinding;
        private Label labelRegisterNewInstallation;
        private UISCADA uiScadaInstance;

        public UCRemovePlant(IndustrialPlant scada, UISCADA uISCADA)
        {
            this.scada = scada;
            this.uiScadaInstance = uISCADA;
            InitializeComponent();

            plantsBinding = new BindingList<SCADADomain.Component>();
            foreach (SCADADomain.Component plant in scada.Plants)
            {
                plantsBinding.Add(plant);
            }
            listPlants.DataSource = plantsBinding;
            listPlants.DisplayMember = "Name";
        }

        private void InitializeComponent()
        {
            this.labelRemoveInstallation = new System.Windows.Forms.Label();
            this.buttonRemovePlant = new System.Windows.Forms.Button();
            this.listPlants = new System.Windows.Forms.ListBox();
            this.labelListOfInstallation = new System.Windows.Forms.Label();
            this.labelRegisterNewInstallation = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelRemoveInstallation
            // 
            this.labelRemoveInstallation.AutoSize = true;
            this.labelRemoveInstallation.Location = new System.Drawing.Point(309, 86);
            this.labelRemoveInstallation.Name = "labelRemoveInstallation";
            this.labelRemoveInstallation.Size = new System.Drawing.Size(93, 13);
            this.labelRemoveInstallation.TabIndex = 7;
            this.labelRemoveInstallation.Text = "Remove selected:";
            // 
            // buttonRemovePlant
            // 
            this.buttonRemovePlant.Location = new System.Drawing.Point(312, 117);
            this.buttonRemovePlant.Name = "buttonRemovePlant";
            this.buttonRemovePlant.Size = new System.Drawing.Size(75, 23);
            this.buttonRemovePlant.TabIndex = 6;
            this.buttonRemovePlant.Text = "Remove";
            this.buttonRemovePlant.UseVisualStyleBackColor = true;
            this.buttonRemovePlant.Click += new System.EventHandler(this.buttonRemovePlant_Click_1);
            // 
            // listPlants
            // 
            this.listPlants.FormattingEnabled = true;
            this.listPlants.Location = new System.Drawing.Point(97, 86);
            this.listPlants.Name = "listPlants";
            this.listPlants.Size = new System.Drawing.Size(150, 303);
            this.listPlants.TabIndex = 5;
            // 
            // labelListOfInstallation
            // 
            this.labelListOfInstallation.AutoSize = true;
            this.labelListOfInstallation.Location = new System.Drawing.Point(94, 53);
            this.labelListOfInstallation.Name = "labelListOfInstallation";
            this.labelListOfInstallation.Size = new System.Drawing.Size(116, 13);
            this.labelListOfInstallation.TabIndex = 4;
            this.labelListOfInstallation.Text = "List of Plants in system:";
            // 
            // labelRegisterNewInstallation
            // 
            this.labelRegisterNewInstallation.AutoSize = true;
            this.labelRegisterNewInstallation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRegisterNewInstallation.Location = new System.Drawing.Point(221, 26);
            this.labelRegisterNewInstallation.Name = "labelRegisterNewInstallation";
            this.labelRegisterNewInstallation.Size = new System.Drawing.Size(86, 13);
            this.labelRegisterNewInstallation.TabIndex = 8;
            this.labelRegisterNewInstallation.Text = "Remove Plant";
            // 
            // UCRemovePlant
            // 
            this.Controls.Add(this.labelRegisterNewInstallation);
            this.Controls.Add(this.labelRemoveInstallation);
            this.Controls.Add(this.buttonRemovePlant);
            this.Controls.Add(this.listPlants);
            this.Controls.Add(this.labelListOfInstallation);
            this.Name = "UCRemovePlant";
            this.Size = new System.Drawing.Size(531, 421);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void EmptyFields(SCADADomain.Component plantSelected)
        {
            plantsBinding.Remove(plantSelected);
            uiScadaInstance.LoadControlPanel();
        }

        private void buttonRemovePlant_Click_1(object sender, EventArgs e)
        {
            try
            {
                SCADADomain.Component plantSelected = (SCADADomain.Component)listPlants.SelectedItem;
                scada.ErasePlant(plantSelected);
                MessageBox.Show("Correctly Deleted: Plant");
                uiScadaInstance.LoadControlPanel();
                EmptyFields(plantSelected);
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }
    }
}