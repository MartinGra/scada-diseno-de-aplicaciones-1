﻿using System.Windows.Forms;
using SCADADomain;
using System.Collections.Generic;
using System;
using SCADAFacade;
using SCADAPersistence.Incident_Managers;

namespace SCADA
{
    internal class UCCreateIncident : UserControl
    {
        private IndustrialPlant scada;
        private Label labelNewIncident;
        private Label labelComponent;
        private ComboBox comboBoxComponent;
        private Label labelSeverity;
        private RichTextBox richTextBoxDescription;
        private Label labelDescription;
        private Button buttonRegister;
        private Label labelStorage;
        private ComboBox comboBoxStorage;
        private ComboBox comboBoxSeverity;
        private Label labelListComponents;
        private ListBox listBoxComponents;
        private UISCADA uISCADA;
        private IncidentManager storage;

        public UCCreateIncident(IndustrialPlant scada, UISCADA uISCADA)
        {
            this.scada = scada;
            this.uISCADA = uISCADA;
            InitializeComponent();
            LoadComponentTypes();
            LoadSeverityTypes();
            LoadStorageTypes();
            comboBoxStorage.SelectedIndex = (uISCADA.defaultStorage); 
        }
        private void LoadComponentTypes()
        {
            List<string> compTypes = new List<string>();
            compTypes.Add("Plant");
            compTypes.Add("Installation");
            compTypes.Add("Device");
            comboBoxComponent.DataSource = compTypes;
        }
        private void LoadSeverityTypes()
        {
            List<string> sevTypes = new List<string>();
            sevTypes.Add("1 - Insignificant");
            sevTypes.Add("2 - Minor ");
            sevTypes.Add("3 - Moderate");
            sevTypes.Add("4 - Serious");
            sevTypes.Add("5 - Critical");
            comboBoxSeverity.DataSource = sevTypes;
        }
        private void LoadStorageTypes()
        {
            List<string> storageTypes = new List<string>();
            storageTypes.Add("Plain Text");
            storageTypes.Add("Data Base");
            comboBoxStorage.DataSource = storageTypes;
        }
        private void InitializeComponent()
        {
            this.labelNewIncident = new System.Windows.Forms.Label();
            this.labelComponent = new System.Windows.Forms.Label();
            this.comboBoxComponent = new System.Windows.Forms.ComboBox();
            this.labelSeverity = new System.Windows.Forms.Label();
            this.richTextBoxDescription = new System.Windows.Forms.RichTextBox();
            this.labelDescription = new System.Windows.Forms.Label();
            this.buttonRegister = new System.Windows.Forms.Button();
            this.labelStorage = new System.Windows.Forms.Label();
            this.comboBoxStorage = new System.Windows.Forms.ComboBox();
            this.comboBoxSeverity = new System.Windows.Forms.ComboBox();
            this.labelListComponents = new System.Windows.Forms.Label();
            this.listBoxComponents = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // labelNewIncident
            // 
            this.labelNewIncident.AutoSize = true;
            this.labelNewIncident.Location = new System.Drawing.Point(268, 38);
            this.labelNewIncident.Name = "labelNewIncident";
            this.labelNewIncident.Size = new System.Drawing.Size(70, 13);
            this.labelNewIncident.TabIndex = 0;
            this.labelNewIncident.Text = "New Incident";
            // 
            // labelComponent
            // 
            this.labelComponent.AutoSize = true;
            this.labelComponent.Location = new System.Drawing.Point(24, 24);
            this.labelComponent.Name = "labelComponent";
            this.labelComponent.Size = new System.Drawing.Size(84, 13);
            this.labelComponent.TabIndex = 1;
            this.labelComponent.Text = "Component type";
            // 
            // comboBoxComponent
            // 
            this.comboBoxComponent.FormattingEnabled = true;
            this.comboBoxComponent.Location = new System.Drawing.Point(27, 54);
            this.comboBoxComponent.Name = "comboBoxComponent";
            this.comboBoxComponent.Size = new System.Drawing.Size(103, 21);
            this.comboBoxComponent.TabIndex = 2;
            this.comboBoxComponent.SelectedIndexChanged += new System.EventHandler(this.comboBoxComponent_SelectedIndexChanged);
            // 
            // labelSeverity
            // 
            this.labelSeverity.AutoSize = true;
            this.labelSeverity.Location = new System.Drawing.Point(193, 138);
            this.labelSeverity.Name = "labelSeverity";
            this.labelSeverity.Size = new System.Drawing.Size(45, 13);
            this.labelSeverity.TabIndex = 3;
            this.labelSeverity.Text = "Severity";
            // 
            // richTextBoxDescription
            // 
            this.richTextBoxDescription.Location = new System.Drawing.Point(271, 181);
            this.richTextBoxDescription.Name = "richTextBoxDescription";
            this.richTextBoxDescription.Size = new System.Drawing.Size(173, 42);
            this.richTextBoxDescription.TabIndex = 4;
            this.richTextBoxDescription.Text = "";
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Location = new System.Drawing.Point(193, 184);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(60, 13);
            this.labelDescription.TabIndex = 5;
            this.labelDescription.Text = "Description";
            // 
            // buttonRegister
            // 
            this.buttonRegister.Location = new System.Drawing.Point(292, 255);
            this.buttonRegister.Name = "buttonRegister";
            this.buttonRegister.Size = new System.Drawing.Size(75, 23);
            this.buttonRegister.TabIndex = 7;
            this.buttonRegister.Text = "Register";
            this.buttonRegister.UseVisualStyleBackColor = true;
            this.buttonRegister.Click += new System.EventHandler(this.buttonRegister_Click);
            // 
            // labelStorage
            // 
            this.labelStorage.AutoSize = true;
            this.labelStorage.Location = new System.Drawing.Point(193, 94);
            this.labelStorage.Name = "labelStorage";
            this.labelStorage.Size = new System.Drawing.Size(47, 13);
            this.labelStorage.TabIndex = 8;
            this.labelStorage.Text = "Storage ";
            // 
            // comboBoxStorage
            // 
            this.comboBoxStorage.FormattingEnabled = true;
            this.comboBoxStorage.Location = new System.Drawing.Point(271, 91);
            this.comboBoxStorage.Name = "comboBoxStorage";
            this.comboBoxStorage.Size = new System.Drawing.Size(131, 21);
            this.comboBoxStorage.TabIndex = 9;
            this.comboBoxStorage.SelectedIndexChanged += new System.EventHandler(this.comboBoxStorage_SelectedIndexChanged);
            // 
            // comboBoxSeverity
            // 
            this.comboBoxSeverity.FormattingEnabled = true;
            this.comboBoxSeverity.Location = new System.Drawing.Point(271, 135);
            this.comboBoxSeverity.Name = "comboBoxSeverity";
            this.comboBoxSeverity.Size = new System.Drawing.Size(131, 21);
            this.comboBoxSeverity.TabIndex = 10;
            // 
            // labelListComponents
            // 
            this.labelListComponents.AutoSize = true;
            this.labelListComponents.Location = new System.Drawing.Point(24, 117);
            this.labelListComponents.Name = "labelListComponents";
            this.labelListComponents.Size = new System.Drawing.Size(96, 13);
            this.labelListComponents.TabIndex = 11;
            this.labelListComponents.Text = "List of components";
            // 
            // listBoxComponents
            // 
            this.listBoxComponents.FormattingEnabled = true;
            this.listBoxComponents.Location = new System.Drawing.Point(27, 147);
            this.listBoxComponents.Name = "listBoxComponents";
            this.listBoxComponents.Size = new System.Drawing.Size(120, 199);
            this.listBoxComponents.TabIndex = 12;
            // 
            // UCCreateIncident
            // 
            this.Controls.Add(this.listBoxComponents);
            this.Controls.Add(this.labelListComponents);
            this.Controls.Add(this.comboBoxSeverity);
            this.Controls.Add(this.comboBoxStorage);
            this.Controls.Add(this.labelStorage);
            this.Controls.Add(this.buttonRegister);
            this.Controls.Add(this.labelDescription);
            this.Controls.Add(this.richTextBoxDescription);
            this.Controls.Add(this.labelSeverity);
            this.Controls.Add(this.comboBoxComponent);
            this.Controls.Add(this.labelComponent);
            this.Controls.Add(this.labelNewIncident);
            this.Name = "UCCreateIncident";
            this.Size = new System.Drawing.Size(463, 436);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private IncidentManager ObtainSelectedStorage(string storage)
        {
            IncidentManager ret = null;
            if (storage.Equals("Data Base"))
                ret = new IncidentDataBaseImp();
            if (storage.Equals("Plain Text"))
                ret = new IncidentPlainTextImp();
            return ret;                
        }

        private void buttonRegister_Click(object sender, System.EventArgs e)
        {
            try
            {
                storage = ObtainSelectedStorage(comboBoxStorage.Text);
                string severity = (comboBoxSeverity.SelectedIndex + 1).ToString();
                string description = richTextBoxDescription.Text;
                Component selected = (Component)listBoxComponents.SelectedItem;
                scada.CreateIncident(severity, description, selected, storage);                
                MessageBox.Show("Success: Incident registered");
                this.richTextBoxDescription.Text = string.Empty;
            } catch(Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }


        private void comboBoxComponent_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected = (string)comboBoxComponent.SelectedValue;
            listBoxComponents.DataSource = uISCADA.ListOfSelectedComponentType(selected);
            listBoxComponents.DisplayMember = "Name";
        }

        private void comboBoxStorage_SelectedIndexChanged(object sender, EventArgs e)
        {
            storage = ObtainSelectedStorage(comboBoxStorage.SelectedText);
        }
    }
}