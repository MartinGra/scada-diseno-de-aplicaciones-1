﻿using System.Windows.Forms;
using SCADADomain;
using System.Collections.Generic;
using System;
using System.Data;
using SCADAFacade;
using SCADAPersistence.Incident_Managers;

namespace SCADA
{
    internal class UCReportIncident : UserControl
    {
        private IndustrialPlant scada;
        private ComboBox comboBoxComponentTypes;
        private ListBox listBoxComponents;
        private DataGridView dataGridIncidents;
        private Label labelComponentTypes;
        private Label labelComponentList;
        private Label labelIncidents;
        private UISCADA uISCADA;
        private ComboBox comboBoxStorage;
        private Label labelStorage;
        private DataTable reportTable;
        private CheckBox checkBox1;
        private IncidentManager storage;
        private bool recursiveReport;
        //private List<string> reportString;

        public UCReportIncident(IndustrialPlant scada, UISCADA uISCADA)
        {
            recursiveReport = false;
            this.scada = scada;
            this.uISCADA = uISCADA;
            InitializeComponent();

            LoadStorageTypes();
            LoadSimpleReportTable();
            LoadComponentTypes();
        }

        private void LoadComponentTypes()
        {
            List<string> componentTypes = new List<string>();
            componentTypes.Add("Plant");
            componentTypes.Add("Installation");
            componentTypes.Add("Device");
            comboBoxComponentTypes.DataSource = componentTypes;
        }

        private void LoadSimpleReportTable()
        {
            reportTable = new DataTable();
            reportTable.Columns.Add("Severity");
            reportTable.Columns.Add("Description");
            reportTable.Columns.Add("Date");

            dataGridIncidents.DataSource = reportTable;
            dataGridIncidents.RowHeadersVisible = false;
            dataGridIncidents.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridIncidents.AllowUserToAddRows = false;
            dataGridIncidents.ReadOnly = true;
        }

        private void InitializeComponent()
        {
            this.comboBoxComponentTypes = new System.Windows.Forms.ComboBox();
            this.listBoxComponents = new System.Windows.Forms.ListBox();
            this.dataGridIncidents = new System.Windows.Forms.DataGridView();
            this.labelComponentTypes = new System.Windows.Forms.Label();
            this.labelComponentList = new System.Windows.Forms.Label();
            this.labelIncidents = new System.Windows.Forms.Label();
            this.comboBoxStorage = new System.Windows.Forms.ComboBox();
            this.labelStorage = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridIncidents)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxComponentTypes
            // 
            this.comboBoxComponentTypes.FormattingEnabled = true;
            this.comboBoxComponentTypes.Location = new System.Drawing.Point(25, 75);
            this.comboBoxComponentTypes.Name = "comboBoxComponentTypes";
            this.comboBoxComponentTypes.Size = new System.Drawing.Size(121, 21);
            this.comboBoxComponentTypes.TabIndex = 0;
            this.comboBoxComponentTypes.SelectedIndexChanged += new System.EventHandler(this.comboBoxComponentTypes_SelectedIndexChanged);
            // 
            // listBoxComponents
            // 
            this.listBoxComponents.FormattingEnabled = true;
            this.listBoxComponents.Location = new System.Drawing.Point(22, 173);
            this.listBoxComponents.Name = "listBoxComponents";
            this.listBoxComponents.Size = new System.Drawing.Size(120, 238);
            this.listBoxComponents.TabIndex = 1;
            this.listBoxComponents.SelectedIndexChanged += new System.EventHandler(this.listBoxComponents_SelectedIndexChanged);
            // 
            // dataGridIncidents
            // 
            this.dataGridIncidents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridIncidents.Location = new System.Drawing.Point(157, 173);
            this.dataGridIncidents.Name = "dataGridIncidents";
            this.dataGridIncidents.Size = new System.Drawing.Size(405, 238);
            this.dataGridIncidents.TabIndex = 2;
            // 
            // labelComponentTypes
            // 
            this.labelComponentTypes.AutoSize = true;
            this.labelComponentTypes.Location = new System.Drawing.Point(22, 47);
            this.labelComponentTypes.Name = "labelComponentTypes";
            this.labelComponentTypes.Size = new System.Drawing.Size(84, 13);
            this.labelComponentTypes.TabIndex = 3;
            this.labelComponentTypes.Text = "Component type";
            // 
            // labelComponentList
            // 
            this.labelComponentList.AutoSize = true;
            this.labelComponentList.Location = new System.Drawing.Point(22, 152);
            this.labelComponentList.Name = "labelComponentList";
            this.labelComponentList.Size = new System.Drawing.Size(96, 13);
            this.labelComponentList.TabIndex = 4;
            this.labelComponentList.Text = "List of components";
            // 
            // labelIncidents
            // 
            this.labelIncidents.AutoSize = true;
            this.labelIncidents.Location = new System.Drawing.Point(298, 152);
            this.labelIncidents.Name = "labelIncidents";
            this.labelIncidents.Size = new System.Drawing.Size(50, 13);
            this.labelIncidents.TabIndex = 5;
            this.labelIncidents.Text = "Incidents";
            // 
            // comboBoxStorage
            // 
            this.comboBoxStorage.FormattingEnabled = true;
            this.comboBoxStorage.Location = new System.Drawing.Point(249, 75);
            this.comboBoxStorage.Name = "comboBoxStorage";
            this.comboBoxStorage.Size = new System.Drawing.Size(131, 21);
            this.comboBoxStorage.TabIndex = 11;
            this.comboBoxStorage.SelectedIndexChanged += new System.EventHandler(this.comboBoxStorage_SelectedIndexChanged);
            // 
            // labelStorage
            // 
            this.labelStorage.AutoSize = true;
            this.labelStorage.Location = new System.Drawing.Point(171, 78);
            this.labelStorage.Name = "labelStorage";
            this.labelStorage.Size = new System.Drawing.Size(47, 13);
            this.labelStorage.TabIndex = 10;
            this.labelStorage.Text = "Storage ";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(174, 126);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(135, 17);
            this.checkBox1.TabIndex = 12;
            this.checkBox1.Text = "Enable recursive report";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // UCReportIncident
            // 
            this.AutoSize = true;
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.comboBoxStorage);
            this.Controls.Add(this.labelStorage);
            this.Controls.Add(this.labelIncidents);
            this.Controls.Add(this.labelComponentList);
            this.Controls.Add(this.labelComponentTypes);
            this.Controls.Add(this.dataGridIncidents);
            this.Controls.Add(this.listBoxComponents);
            this.Controls.Add(this.comboBoxComponentTypes);
            this.Name = "UCReportIncident";
            this.Size = new System.Drawing.Size(580, 446);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridIncidents)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void comboBoxComponentTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            string selected = (string)comboBoxComponentTypes.SelectedValue;
            listBoxComponents.DataSource = uISCADA.ListOfSelectedComponentType(selected);
            listBoxComponents.DisplayMember = "Name";
        }

        private void listBoxComponents_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                LoadUCReport();
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
           
        }

        private void LoadUCReport()
        {
            LoadStorageOption();
            Component comp = (Component)listBoxComponents.SelectedValue;
            if (comp != null)
                LoadReport(comp);
        }

        private void LoadReport(Component comp)
        {
            dataGridIncidents.DataSource = null;
            reportTable.Clear();
            List <string> reports = scada.IncidentReports(comp, storage);
            foreach (string line in reports)
            {
                string[] incidParams = line.Split('_');
                if (recursiveReport)
                {
                    reportTable.Rows.Add(comp.Name, incidParams[1], incidParams[0], incidParams[2]);
                    LoadRecursiveReports(comp);
                }
                else
                    reportTable.Rows.Add(incidParams[1], incidParams[0], incidParams[2]);
            }
            dataGridIncidents.DataSource = reportTable;
        }

        private void LoadRecursiveReports(Component comp)
        {
            foreach(Component child in scada.Components)            
            {
                if(child.HasFather() && child.Father.ComponentId == comp.ComponentId)
                {
                    List<string> reportTemporary = scada.IncidentReports(child, storage);

                    foreach (string line in reportTemporary)
                    {
                        string[] incidParams = line.Split('_');
                        reportTable.Rows.Add(child.Name, incidParams[1], incidParams[0], incidParams[2]);
                    }
                    LoadRecursiveReports(child);
                }                
            }
        }

        private void LoadStorageOption()
        {
            if (comboBoxStorage.SelectedItem.ToString() == "Data Base")
                storage = new IncidentDataBaseImp();
            else
                storage = new IncidentPlainTextImp();
        }

        private void comboBoxStorage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadUCReport();
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

        private IncidentManager ObtainSelectedStorage(string storage)
        {
            LoadStorageOption();
            return this.storage;
        }

        private void LoadStorageTypes()
        {
            List<string> storageTypes = new List<string>();
            storageTypes.Add("Data Base");
            storageTypes.Add("Plain Text");
            comboBoxStorage.DataSource = storageTypes;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            recursiveReport = !recursiveReport;
            if (recursiveReport)
                LoadComplexReportTable();
            else
                LoadSimpleReportTable();
            LoadUCReport();
        }

        private void LoadComplexReportTable()
        {
            reportTable = new DataTable();
            reportTable.Columns.Add("Component");
            reportTable.Columns.Add("Severity");
            reportTable.Columns.Add("Description");
            reportTable.Columns.Add("Date");

            dataGridIncidents.DataSource = reportTable;
            dataGridIncidents.RowHeadersVisible = false;
            dataGridIncidents.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridIncidents.AllowUserToAddRows = false;
            dataGridIncidents.ReadOnly = true;
        }
    }
}