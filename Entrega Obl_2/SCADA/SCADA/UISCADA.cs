﻿using System;
using System.Collections.Generic;

//using System.ComponentModel;

using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using SCADADomain;
using SCADAFacade;

namespace SCADA
{
    public partial class UISCADA : Form
    {
        public UISCADA()
        {
            InitializeComponent();
            defaultStorage = 1;
            scada = IndustrialPlantDBImp.ObtainInstance;
            LoadMainMenu();
            LoadControlPanel();
        }
        internal void LoadControlVariablesFromSelectedComponent(Component selected, ListBox listControlVariables)
        {
            listControlVariables.DataSource = selected.ControlVariables.ToList();
            listControlVariables.DisplayMember = "Name";
        }

        internal void LoadComponentTypesOnComboBox(ComboBox combo)
        {
            Collection<string> possibleTypes = new Collection<string>();
            possibleTypes.Add("Installation");
            possibleTypes.Add("Device");
            combo.DataSource = possibleTypes;
        }
        internal void LoadSelectedComponentTypeFromComboOnList(ComboBox combo, ListBox list)
        {
            string selection = (string)combo.SelectedItem;
            if (selection != null)
            {
                if (selection.Equals("Plant"))
                {
                    list.DataSource = scada.Plants;
                }
                if (selection.Equals("Installation"))
                {
                    list.DataSource = scada.Installations;
                }
                if (selection.Equals("Device"))
                {
                    list.DataSource = scada.Devices;
                }
                list.DisplayMember = "Name";
            }
        }

        internal ICollection<Component> ListOfSelectedComponentType(string selected)
        {
            ICollection<Component> components = new List<Component>();
            if (selected.Equals("Plant"))
                components = scada.Plants;
            if (selected.Equals("Installation"))
                components = scada.Installations;
            if (selected.Equals("Device"))
                components = scada.Devices;
            return components;
        }

        public void LoadControlPanel()
        {
            treeHierarchy.Nodes.Clear();
            ControlPanelImplementation();
        }

        private Color ComponentNoticeColor(Component comp)
        {
            Color notice = Color.Green;
            if (scada.GetTotalWarnings(comp) != 0) notice = Color.DarkGoldenrod;
            if (scada.GetTotalAlarms(comp) != 0) notice = Color.Red;
            return notice;
        }
        private void ControlPanelImplementation()
        {
            int pos = 0;
            foreach (Component comp in scada.Components)
            {
                if (!comp.HasFather() && !comp.IsInLowerLevel())
                {
                    treeHierarchy.Nodes.Add(comp.Name + GetVisualTypeOfComponent(comp));
                    treeHierarchy.Nodes[pos++].ForeColor = ComponentNoticeColor(comp);
                    string deep = "";
                    InsertChildsIntoHierarchy(comp, ref pos, deep);
                }
            }
        }
        private void InsertChildsIntoHierarchy(Component comp, ref int pos, string deepness)
        {
            deepness += "....";
            foreach (Component son in scada.Components)
            {
                if (son.HasFather() && son.Father.Equals(comp))
                {
                    string childNode = deepness + son.Name + GetVisualTypeOfComponent(son);
                    Color notice = ComponentNoticeColor(son);

                    treeHierarchy.Nodes.Add(childNode);
                    treeHierarchy.Nodes[pos].ForeColor = notice;

                    if (notice.Equals(Color.DarkGoldenrod) || notice.Equals(Color.Red))
                        foreach (ControlVariable cv in son.ControlVariables)
                        {
                            string noticeVariable = cv.Name;
                            noticeVariable += " Ranges[" + cv.MinRange + "," + cv.MinRangeWarning + " - " + cv.MaxRangeWarning + "," + cv.MaxRange + "] Value = " + cv.GetLastValue().Item1;
                            if (cv.HasLastOutOfRange() && notice.Equals(Color.Red))
                            {
                                treeHierarchy.Nodes[pos].Nodes.Add(noticeVariable);
                            }
                            if (cv.HasLastValueInWarningRange() && notice.Equals(Color.DarkGoldenrod))
                            {
                                treeHierarchy.Nodes[pos].Nodes.Add(noticeVariable);
                            }
                        }
                    pos++;
                    InsertChildsIntoHierarchy(son, ref pos, deepness);
                }
            }

        }


        private string GetVisualTypeOfComponent(Component comp)
        {
            string ret = "";
            if (comp.HierarchyLevel.Equals(Globals.LOWER_LEVEL)) ret = " (D)";
            if (comp.HierarchyLevel.Equals(Globals.MIDDLE_LEVEL)) ret = " (I)";
            if (comp.HierarchyLevel.Equals(Globals.UPPER_LEVEL)) ret = " (P)";
            return ret;
        }

        private void LoadMainMenu()
        {
            panelMainMenu.Controls.Clear();
            UserControl mainView = new UCCreatePlant(scada,this);
            panelMainMenu.Controls.Add(mainView);
        }
        public IndustrialPlant scada { get; set; }
        public int defaultStorage { get; set; }

        private void createPlantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl mainView = new UCCreateInstallation(scada, this);
            panelMainMenu.Controls.Add(mainView);
        }

        private void updatePlantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl plantUpdate = new UCUpdateInstallation(scada, this);
            panelMainMenu.Controls.Add(plantUpdate);
        }

        private void createDeviceTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl deviceTypeCreation = new UCCreateDeviceType(scada);
            panelMainMenu.Controls.Add(deviceTypeCreation);
        }

        private void createDeviceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl deviceCreation = new UCCreateDevice(scada);
            panelMainMenu.Controls.Add(deviceCreation);
        }

        private void createControlVariableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl controlVariableCreation = new UCCreateControlVariable(scada,this);
            panelMainMenu.Controls.Add(controlVariableCreation);
        }

        private void hierarchyManagmenttoolStripLabel1_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl hierarchyManagment = new UCHierarchyManagment(scada, this);
            panelMainMenu.Controls.Add(hierarchyManagment);
        }
        private void updateDeviceTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl deviceTypeUpdate = new UCUpdateDeviceType(scada);
            panelMainMenu.Controls.Add(deviceTypeUpdate);
        }

        private void updateDeviceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl deviceUpdate = new UCUpdateDevice(scada, this);
            panelMainMenu.Controls.Add(deviceUpdate);
        }

        private void updateControlVariableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl controlVariableUpdate = new UCUpdateControlVariable(scada, this);
            panelMainMenu.Controls.Add(controlVariableUpdate);
        }

        private void inputValueControlVariableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl controlInputValueToControlVariable = new UCInputValueToControlVariable(scada, this);
            panelMainMenu.Controls.Add(controlInputValueToControlVariable);
        }

        private void removePlantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl controlRemovePlant = new UCRemoveInstallation(scada, this);
            panelMainMenu.Controls.Add(controlRemovePlant);
        }
        private void removeControlVariableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl removeControlVariable = new UCRemoveControlVariable(scada, this);
            panelMainMenu.Controls.Add(removeControlVariable);
        }

        private void historicalValueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl viewHistoricControlVariable = new UCViewHistoricControlVariable(scada, this);
            panelMainMenu.Controls.Add(viewHistoricControlVariable);
        }

        private void removeDeviceTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl controlRemoveDeviceType = new UCRemoveDeviceType(scada, this);
            panelMainMenu.Controls.Add(controlRemoveDeviceType);
        }

        private void removeDeviceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl eraseDevice = new UCRemoveDevice(scada, this);
            panelMainMenu.Controls.Add(eraseDevice);
        }

        private void loadTestDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadControlPanel();
            LoadMainMenu();
        }

        private void createPlantToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl createPlant = new UCCreatePlant(scada, this);
            panelMainMenu.Controls.Add(createPlant);
        }

        private void updatePlantStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl updatePlant = new UCUpdatePlant(scada, this);
            panelMainMenu.Controls.Add(updatePlant);
        }

        private void removePlantStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl removePlant = new UCRemovePlant(scada, this);
            panelMainMenu.Controls.Add(removePlant);
        }

        private void createIncidentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl createIncident = new UCCreateIncident(scada, this);
            panelMainMenu.Controls.Add(createIncident);
        }

        private void reportIncidentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelMainMenu.Controls.Clear();
            UserControl reportIncidents = new UCReportIncident(scada, this);
            panelMainMenu.Controls.Add(reportIncidents);
                
                
        }
    }
}
