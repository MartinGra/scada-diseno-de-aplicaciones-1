﻿using System;
using System.Windows.Forms;
using SCADADomain;
using System.Collections.ObjectModel;
using SCADAFacade;

namespace SCADA
{
    internal class UCHierarchyManagment : UserControl
    {
        private IndustrialPlant scada;
        private Label labelComponetType;
        private Label labelSelectChild;
        private Label labelSelectFather;
        private ListBox listChildSelection;
        private ListBox listFatherSelection;
        private Button buttonAssignToHierarchy;
        private ComboBox comboComponentType;
        private Label labelAssignOption;
        private ComboBox fatherComponentType;
        private Label label1;
        private UISCADA uiScadaInstance;

        public UCHierarchyManagment(IndustrialPlant scada, UISCADA uiscada)
        {
            this.scada = scada;
            this.uiScadaInstance = uiscada;
            InitializeComponent();
            LoadComponentTypes();
        }

        private void LoadComponentTypes()
        {
            LoadPosibleComponents();
            LoadPosibleFatherComponents();
        }

        private void LoadPosibleFatherComponents()
        {
            Collection<string> possibleFatherComponentTypes = new Collection<string>();
            possibleFatherComponentTypes.Add("Plant");
            possibleFatherComponentTypes.Add("Installation");
            fatherComponentType.DataSource = possibleFatherComponentTypes;
        }

        private void LoadPosibleComponents()
        {
            Collection<string> possibleComponentTypes = new Collection<string>();
            possibleComponentTypes.Add("Plant");
            possibleComponentTypes.Add("Installation");
            possibleComponentTypes.Add("Device");
            comboComponentType.DataSource = possibleComponentTypes;
        }

        private void InitializeComponent()
        {
            this.labelComponetType = new System.Windows.Forms.Label();
            this.labelSelectChild = new System.Windows.Forms.Label();
            this.labelSelectFather = new System.Windows.Forms.Label();
            this.listChildSelection = new System.Windows.Forms.ListBox();
            this.listFatherSelection = new System.Windows.Forms.ListBox();
            this.buttonAssignToHierarchy = new System.Windows.Forms.Button();
            this.comboComponentType = new System.Windows.Forms.ComboBox();
            this.labelAssignOption = new System.Windows.Forms.Label();
            this.fatherComponentType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelComponetType
            // 
            this.labelComponetType.AutoSize = true;
            this.labelComponetType.Location = new System.Drawing.Point(67, 37);
            this.labelComponetType.Name = "labelComponetType";
            this.labelComponetType.Size = new System.Drawing.Size(113, 13);
            this.labelComponetType.TabIndex = 0;
            this.labelComponetType.Text = "Child Component type:";
            // 
            // labelSelectChild
            // 
            this.labelSelectChild.AutoSize = true;
            this.labelSelectChild.Location = new System.Drawing.Point(67, 106);
            this.labelSelectChild.Name = "labelSelectChild";
            this.labelSelectChild.Size = new System.Drawing.Size(78, 13);
            this.labelSelectChild.TabIndex = 1;
            this.labelSelectChild.Text = "Child to assign:";
            // 
            // labelSelectFather
            // 
            this.labelSelectFather.AutoSize = true;
            this.labelSelectFather.Location = new System.Drawing.Point(260, 106);
            this.labelSelectFather.Name = "labelSelectFather";
            this.labelSelectFather.Size = new System.Drawing.Size(70, 13);
            this.labelSelectFather.TabIndex = 2;
            this.labelSelectFather.Text = "Select father:";
            // 
            // listChildSelection
            // 
            this.listChildSelection.FormattingEnabled = true;
            this.listChildSelection.Location = new System.Drawing.Point(70, 134);
            this.listChildSelection.Name = "listChildSelection";
            this.listChildSelection.Size = new System.Drawing.Size(137, 199);
            this.listChildSelection.TabIndex = 3;
            // 
            // listFatherSelection
            // 
            this.listFatherSelection.FormattingEnabled = true;
            this.listFatherSelection.Location = new System.Drawing.Point(263, 134);
            this.listFatherSelection.Name = "listFatherSelection";
            this.listFatherSelection.Size = new System.Drawing.Size(137, 199);
            this.listFatherSelection.TabIndex = 4;
            // 
            // buttonAssignToHierarchy
            // 
            this.buttonAssignToHierarchy.Location = new System.Drawing.Point(444, 161);
            this.buttonAssignToHierarchy.Name = "buttonAssignToHierarchy";
            this.buttonAssignToHierarchy.Size = new System.Drawing.Size(75, 23);
            this.buttonAssignToHierarchy.TabIndex = 5;
            this.buttonAssignToHierarchy.Text = "Assign";
            this.buttonAssignToHierarchy.UseVisualStyleBackColor = true;
            this.buttonAssignToHierarchy.Click += new System.EventHandler(this.buttonAssignToHierarchy_Click);
            // 
            // comboComponentType
            // 
            this.comboComponentType.FormattingEnabled = true;
            this.comboComponentType.Location = new System.Drawing.Point(70, 64);
            this.comboComponentType.Name = "comboComponentType";
            this.comboComponentType.Size = new System.Drawing.Size(121, 21);
            this.comboComponentType.TabIndex = 6;
            this.comboComponentType.SelectedIndexChanged += new System.EventHandler(this.comboComponentType_SelectedIndexChanged);
            // 
            // labelAssignOption
            // 
            this.labelAssignOption.AutoSize = true;
            this.labelAssignOption.Location = new System.Drawing.Point(441, 134);
            this.labelAssignOption.Name = "labelAssignOption";
            this.labelAssignOption.Size = new System.Drawing.Size(161, 13);
            this.labelAssignOption.TabIndex = 9;
            this.labelAssignOption.Text = "Assign Child To Father Hierarchy";
            // 
            // fatherComponentType
            // 
            this.fatherComponentType.FormattingEnabled = true;
            this.fatherComponentType.Location = new System.Drawing.Point(263, 63);
            this.fatherComponentType.Name = "fatherComponentType";
            this.fatherComponentType.Size = new System.Drawing.Size(121, 21);
            this.fatherComponentType.TabIndex = 11;
            this.fatherComponentType.SelectedIndexChanged += new System.EventHandler(this.fatherComponentType_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(260, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Father Component type:";
            // 
            // UCHierarchyManagment
            // 
            this.Controls.Add(this.fatherComponentType);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelAssignOption);
            this.Controls.Add(this.comboComponentType);
            this.Controls.Add(this.buttonAssignToHierarchy);
            this.Controls.Add(this.listFatherSelection);
            this.Controls.Add(this.listChildSelection);
            this.Controls.Add(this.labelSelectFather);
            this.Controls.Add(this.labelSelectChild);
            this.Controls.Add(this.labelComponetType);
            this.Name = "UCHierarchyManagment";
            this.Size = new System.Drawing.Size(641, 403);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void comboComponentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selection = (string)comboComponentType.SelectedItem;

            if (selection.Equals("Installation"))
                listChildSelection.DataSource = scada.Installations;
            if (selection.Equals("Plant"))
                listChildSelection.DataSource = scada.Plants;
            if (selection.Equals("Device"))
                listChildSelection.DataSource = scada.Devices;

            listChildSelection.DisplayMember = "Name";
            
        }

        private void buttonAssignToHierarchy_Click(object sender, EventArgs e)
        {
            try
            {
                Component childToAssign = (Component)listChildSelection.SelectedItem;
                Component fatherToAssign = (Component)listFatherSelection.SelectedItem;
                scada.AssignChildToFatherHierarchy(childToAssign, fatherToAssign);
                MessageBox.Show("Assignation done correctly");
                uiScadaInstance.LoadControlPanel();
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

        private void fatherComponentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selection = (string)fatherComponentType.SelectedItem;
            listFatherSelection.BindingContext = new BindingContext();
            if (selection.Equals("Installation"))
                listFatherSelection.DataSource = scada.Installations;
            if (selection.Equals("Plant"))
                listFatherSelection.DataSource = scada.Plants;
            listFatherSelection.DisplayMember = "Name";
        }
    }
}