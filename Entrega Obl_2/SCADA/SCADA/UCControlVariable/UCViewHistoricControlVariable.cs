﻿using System.Windows.Forms;
using SCADADomain;
using System;
using System.Data;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using SCADAFacade;

namespace SCADA
{
    internal class UCViewHistoricControlVariable : UserControl
    {
        private Label labelMinRangeOfVariable;
        private Label labelMaxRangeOfVariable;
        private Label labelNameOfVariable;
        private Label labelDataOfVariable;
        private Label labelControlVariables;
        private Label labelListOfComponents;
        private Label labelComponentTypes;
        private ListBox listControlVariables;
        private ListBox listComponents;
        private ComboBox comboComponentTypes;
        private Label labelNameControlVariable;
        private Label labelMinRangeValue;
        private Label labelMaxRangeValue;
        private DataGridView dataGridViewValuesControl;
        private IndustrialPlant scada;
        private Label labelMinWarningOfVariable;
        private Label labelMaxWarningOfVariable;
        private Label labelMinWarning;
        private Label labelMaxWarning;
        private UISCADA uiScadaInstance;

        public UCViewHistoricControlVariable(IndustrialPlant scada, UISCADA uISCADA)
        {
            this.scada = scada;
            this.uiScadaInstance = uISCADA;
            InitializeComponent();
            uiScadaInstance.LoadComponentTypesOnComboBox(comboComponentTypes);
            labelMaxRangeValue.Text = "";
            labelMinRangeValue.Text = "";
            labelMinWarning.Text = "";
            labelMaxWarning.Text = "";
            labelNameControlVariable.Text = "";
        }

        private void InitializeComponent()
        {
            this.labelMinRangeOfVariable = new System.Windows.Forms.Label();
            this.labelMaxRangeOfVariable = new System.Windows.Forms.Label();
            this.labelNameOfVariable = new System.Windows.Forms.Label();
            this.labelDataOfVariable = new System.Windows.Forms.Label();
            this.labelControlVariables = new System.Windows.Forms.Label();
            this.labelListOfComponents = new System.Windows.Forms.Label();
            this.labelComponentTypes = new System.Windows.Forms.Label();
            this.listControlVariables = new System.Windows.Forms.ListBox();
            this.listComponents = new System.Windows.Forms.ListBox();
            this.comboComponentTypes = new System.Windows.Forms.ComboBox();
            this.labelNameControlVariable = new System.Windows.Forms.Label();
            this.labelMinRangeValue = new System.Windows.Forms.Label();
            this.labelMaxRangeValue = new System.Windows.Forms.Label();
            this.dataGridViewValuesControl = new System.Windows.Forms.DataGridView();
            this.labelMinWarningOfVariable = new System.Windows.Forms.Label();
            this.labelMaxWarningOfVariable = new System.Windows.Forms.Label();
            this.labelMinWarning = new System.Windows.Forms.Label();
            this.labelMaxWarning = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewValuesControl)).BeginInit();
            this.SuspendLayout();
            // 
            // labelMinRangeOfVariable
            // 
            this.labelMinRangeOfVariable.AutoSize = true;
            this.labelMinRangeOfVariable.Location = new System.Drawing.Point(323, 83);
            this.labelMinRangeOfVariable.Name = "labelMinRangeOfVariable";
            this.labelMinRangeOfVariable.Size = new System.Drawing.Size(59, 13);
            this.labelMinRangeOfVariable.TabIndex = 28;
            this.labelMinRangeOfVariable.Text = "Min Range";
            // 
            // labelMaxRangeOfVariable
            // 
            this.labelMaxRangeOfVariable.AutoSize = true;
            this.labelMaxRangeOfVariable.Location = new System.Drawing.Point(323, 112);
            this.labelMaxRangeOfVariable.Name = "labelMaxRangeOfVariable";
            this.labelMaxRangeOfVariable.Size = new System.Drawing.Size(62, 13);
            this.labelMaxRangeOfVariable.TabIndex = 27;
            this.labelMaxRangeOfVariable.Text = "Max Range";
            // 
            // labelNameOfVariable
            // 
            this.labelNameOfVariable.AutoSize = true;
            this.labelNameOfVariable.Location = new System.Drawing.Point(323, 52);
            this.labelNameOfVariable.Name = "labelNameOfVariable";
            this.labelNameOfVariable.Size = new System.Drawing.Size(35, 13);
            this.labelNameOfVariable.TabIndex = 26;
            this.labelNameOfVariable.Text = "Name";
            // 
            // labelDataOfVariable
            // 
            this.labelDataOfVariable.AutoSize = true;
            this.labelDataOfVariable.Location = new System.Drawing.Point(323, 22);
            this.labelDataOfVariable.Name = "labelDataOfVariable";
            this.labelDataOfVariable.Size = new System.Drawing.Size(103, 13);
            this.labelDataOfVariable.TabIndex = 25;
            this.labelDataOfVariable.Text = "Data of the variable:";
            // 
            // labelControlVariables
            // 
            this.labelControlVariables.AutoSize = true;
            this.labelControlVariables.Location = new System.Drawing.Point(139, 147);
            this.labelControlVariables.Name = "labelControlVariables";
            this.labelControlVariables.Size = new System.Drawing.Size(89, 13);
            this.labelControlVariables.TabIndex = 24;
            this.labelControlVariables.Text = "Control Variables:";
            // 
            // labelListOfComponents
            // 
            this.labelListOfComponents.AutoSize = true;
            this.labelListOfComponents.Location = new System.Drawing.Point(18, 147);
            this.labelListOfComponents.Name = "labelListOfComponents";
            this.labelListOfComponents.Size = new System.Drawing.Size(100, 13);
            this.labelListOfComponents.TabIndex = 23;
            this.labelListOfComponents.Text = "List of Components:";
            // 
            // labelComponentTypes
            // 
            this.labelComponentTypes.AutoSize = true;
            this.labelComponentTypes.Location = new System.Drawing.Point(20, 22);
            this.labelComponentTypes.Name = "labelComponentTypes";
            this.labelComponentTypes.Size = new System.Drawing.Size(91, 13);
            this.labelComponentTypes.TabIndex = 22;
            this.labelComponentTypes.Text = "Component Type:";
            // 
            // listControlVariables
            // 
            this.listControlVariables.FormattingEnabled = true;
            this.listControlVariables.Location = new System.Drawing.Point(142, 169);
            this.listControlVariables.Name = "listControlVariables";
            this.listControlVariables.Size = new System.Drawing.Size(107, 212);
            this.listControlVariables.TabIndex = 17;
            this.listControlVariables.SelectedIndexChanged += new System.EventHandler(this.listControlVariables_SelectedIndexChanged);
            // 
            // listComponents
            // 
            this.listComponents.FormattingEnabled = true;
            this.listComponents.Location = new System.Drawing.Point(21, 169);
            this.listComponents.Name = "listComponents";
            this.listComponents.Size = new System.Drawing.Size(106, 212);
            this.listComponents.TabIndex = 16;
            this.listComponents.SelectedIndexChanged += new System.EventHandler(this.listComponents_SelectedIndexChanged);
            // 
            // comboComponentTypes
            // 
            this.comboComponentTypes.FormattingEnabled = true;
            this.comboComponentTypes.Location = new System.Drawing.Point(23, 48);
            this.comboComponentTypes.Name = "comboComponentTypes";
            this.comboComponentTypes.Size = new System.Drawing.Size(106, 21);
            this.comboComponentTypes.TabIndex = 15;
            this.comboComponentTypes.SelectedIndexChanged += new System.EventHandler(this.comboComponentTypes_SelectedIndexChanged);
            // 
            // labelNameControlVariable
            // 
            this.labelNameControlVariable.AutoSize = true;
            this.labelNameControlVariable.Location = new System.Drawing.Point(415, 51);
            this.labelNameControlVariable.Name = "labelNameControlVariable";
            this.labelNameControlVariable.Size = new System.Drawing.Size(33, 13);
            this.labelNameControlVariable.TabIndex = 30;
            this.labelNameControlVariable.Text = "name";
            // 
            // labelMinRangeValue
            // 
            this.labelMinRangeValue.AutoSize = true;
            this.labelMinRangeValue.Location = new System.Drawing.Point(412, 112);
            this.labelMinRangeValue.Name = "labelMinRangeValue";
            this.labelMinRangeValue.Size = new System.Drawing.Size(34, 13);
            this.labelMinRangeValue.TabIndex = 31;
            this.labelMinRangeValue.Text = "maxR";
            // 
            // labelMaxRangeValue
            // 
            this.labelMaxRangeValue.AutoSize = true;
            this.labelMaxRangeValue.Location = new System.Drawing.Point(415, 83);
            this.labelMaxRangeValue.Name = "labelMaxRangeValue";
            this.labelMaxRangeValue.Size = new System.Drawing.Size(31, 13);
            this.labelMaxRangeValue.TabIndex = 32;
            this.labelMaxRangeValue.Text = "minR";
            // 
            // dataGridViewValuesControl
            // 
            this.dataGridViewValuesControl.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewValuesControl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewValuesControl.Location = new System.Drawing.Point(307, 211);
            this.dataGridViewValuesControl.Name = "dataGridViewValuesControl";
            this.dataGridViewValuesControl.Size = new System.Drawing.Size(313, 170);
            this.dataGridViewValuesControl.TabIndex = 33;
            // 
            // labelMinWarningOfVariable
            // 
            this.labelMinWarningOfVariable.AutoSize = true;
            this.labelMinWarningOfVariable.Location = new System.Drawing.Point(323, 141);
            this.labelMinWarningOfVariable.Name = "labelMinWarningOfVariable";
            this.labelMinWarningOfVariable.Size = new System.Drawing.Size(67, 13);
            this.labelMinWarningOfVariable.TabIndex = 34;
            this.labelMinWarningOfVariable.Text = "Min Warning";
            // 
            // labelMaxWarningOfVariable
            // 
            this.labelMaxWarningOfVariable.AutoSize = true;
            this.labelMaxWarningOfVariable.Location = new System.Drawing.Point(323, 169);
            this.labelMaxWarningOfVariable.Name = "labelMaxWarningOfVariable";
            this.labelMaxWarningOfVariable.Size = new System.Drawing.Size(70, 13);
            this.labelMaxWarningOfVariable.TabIndex = 35;
            this.labelMaxWarningOfVariable.Text = "Max Warning";
            // 
            // labelMinWarning
            // 
            this.labelMinWarning.AutoSize = true;
            this.labelMinWarning.Location = new System.Drawing.Point(412, 141);
            this.labelMinWarning.Name = "labelMinWarning";
            this.labelMinWarning.Size = new System.Drawing.Size(34, 13);
            this.labelMinWarning.TabIndex = 36;
            this.labelMinWarning.Text = "minW";
            // 
            // labelMaxWarning
            // 
            this.labelMaxWarning.AutoSize = true;
            this.labelMaxWarning.Location = new System.Drawing.Point(411, 169);
            this.labelMaxWarning.Name = "labelMaxWarning";
            this.labelMaxWarning.Size = new System.Drawing.Size(37, 13);
            this.labelMaxWarning.TabIndex = 37;
            this.labelMaxWarning.Text = "maxW";
            // 
            // UCViewHistoricControlVariable
            // 
            this.Controls.Add(this.labelMaxWarning);
            this.Controls.Add(this.labelMinWarning);
            this.Controls.Add(this.labelMaxWarningOfVariable);
            this.Controls.Add(this.labelMinWarningOfVariable);
            this.Controls.Add(this.dataGridViewValuesControl);
            this.Controls.Add(this.labelMaxRangeValue);
            this.Controls.Add(this.labelMinRangeValue);
            this.Controls.Add(this.labelNameControlVariable);
            this.Controls.Add(this.labelMinRangeOfVariable);
            this.Controls.Add(this.labelMaxRangeOfVariable);
            this.Controls.Add(this.labelNameOfVariable);
            this.Controls.Add(this.labelDataOfVariable);
            this.Controls.Add(this.labelControlVariables);
            this.Controls.Add(this.labelListOfComponents);
            this.Controls.Add(this.labelComponentTypes);
            this.Controls.Add(this.listControlVariables);
            this.Controls.Add(this.listComponents);
            this.Controls.Add(this.comboComponentTypes);
            this.Name = "UCViewHistoricControlVariable";
            this.Size = new System.Drawing.Size(672, 457);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewValuesControl)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void comboComponentTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            uiScadaInstance.LoadSelectedComponentTypeFromComboOnList(comboComponentTypes, listComponents);
        }

        private void listComponents_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            Component selected = (Component)listComponents.SelectedItem;
            uiScadaInstance.LoadControlVariablesFromSelectedComponent(selected, listControlVariables);
        }

        private void listControlVariables_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            Component component = (Component)listComponents.SelectedItem;
            ControlVariable selected = (ControlVariable)listControlVariables.SelectedItem;
            ICollection<AssignValue> historicValues = scada.HistoricValues(component, selected);

            LoadUIData(selected);
            DataTable table = CreateTable(historicValues);
            dataGridViewValuesControl.DataSource = table;
            dataGridViewValuesControl.AllowUserToAddRows = false;
            dataGridViewValuesControl.RowHeadersVisible = false;
        }

        private static DataTable CreateTable(ICollection<AssignValue> historicValues)
        {
            DataTable table = new DataTable();

            DataColumn column = new DataColumn();
            column.DataType = System.Type.GetType("System.Single");
            column.ColumnName = "Value";
            column.ReadOnly = true;

            table.Columns.Add(column);

            column = new DataColumn
            {
                DataType = System.Type.GetType("System.DateTime"),
                ColumnName = "Date",
                ReadOnly = true
            };

            table.Columns.Add(column);

            if (historicValues != null)
            {
                foreach (AssignValue pairValue in historicValues)
                {
                    var row = table.NewRow();
                    row[0] = pairValue.Item1;
                    row[1] = pairValue.Item2;

                    table.Rows.Add(row);
                }
            }

            return table;
        }

        private void LoadUIData(ControlVariable selected)
        {
            labelNameControlVariable.Text = selected.Name;
            labelMinRangeValue.Text = Convert.ToString(selected.MaxRange);
            labelMaxRangeValue.Text = Convert.ToString(selected.MinRange);
            labelMinWarning.Text = Convert.ToString(selected.MinRangeWarning);
            labelMaxWarning.Text = Convert.ToString(selected.MaxRangeWarning);
        }
    }
}