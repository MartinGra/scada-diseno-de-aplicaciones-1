﻿using System.Windows.Forms;
using SCADADomain;
using System;
using SCADAFacade;

namespace SCADA
{
    internal class UCRemoveControlVariable : UserControl
    {
        private IndustrialPlant scada;
        private Button buttonRemoveControlVariable;
        private Label labelDeleteControlVariableTitle;
        private Label labelNameValue;
        private Label labelMinValue;
        private Label labelMaxValue;
        private ComboBox comboComponentTypes;
        private ListBox listComponents;
        private ListBox listControlVariables;
        private Label labelComponentTypes;
        private Label labelListOfComponents;
        private Label labelControlVariables;
        private Label labelDataOfVariable;
        private Label labelNameOfVariable;
        private Label labelMaxRangeOfVariable;
        private Label labelMinRangeOfVariable;
        private Label labelMinWarningOfVariable;
        private Label labelMaxWarningOfVariable;
        private Label labelMinWarning;
        private Label labelMaxWarning;
        private UISCADA uiScadaInstance;

        public UCRemoveControlVariable(IndustrialPlant scada, UISCADA uISCADA)
        {
            this.scada = scada;
            this.uiScadaInstance = uISCADA;
            InitializeComponent();
            SetInfoOfControlVariable("", "", "","","");
            uiScadaInstance.LoadComponentTypesOnComboBox(comboComponentTypes);
        }

        private void InitializeComponent()
        {
            this.buttonRemoveControlVariable = new System.Windows.Forms.Button();
            this.labelMinRangeOfVariable = new System.Windows.Forms.Label();
            this.labelMaxRangeOfVariable = new System.Windows.Forms.Label();
            this.labelNameOfVariable = new System.Windows.Forms.Label();
            this.labelDataOfVariable = new System.Windows.Forms.Label();
            this.labelControlVariables = new System.Windows.Forms.Label();
            this.labelListOfComponents = new System.Windows.Forms.Label();
            this.labelComponentTypes = new System.Windows.Forms.Label();
            this.listControlVariables = new System.Windows.Forms.ListBox();
            this.listComponents = new System.Windows.Forms.ListBox();
            this.comboComponentTypes = new System.Windows.Forms.ComboBox();
            this.labelDeleteControlVariableTitle = new System.Windows.Forms.Label();
            this.labelNameValue = new System.Windows.Forms.Label();
            this.labelMinValue = new System.Windows.Forms.Label();
            this.labelMaxValue = new System.Windows.Forms.Label();
            this.labelMinWarningOfVariable = new System.Windows.Forms.Label();
            this.labelMaxWarningOfVariable = new System.Windows.Forms.Label();
            this.labelMinWarning = new System.Windows.Forms.Label();
            this.labelMaxWarning = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonRemoveControlVariable
            // 
            this.buttonRemoveControlVariable.Location = new System.Drawing.Point(341, 341);
            this.buttonRemoveControlVariable.Name = "buttonRemoveControlVariable";
            this.buttonRemoveControlVariable.Size = new System.Drawing.Size(140, 23);
            this.buttonRemoveControlVariable.TabIndex = 29;
            this.buttonRemoveControlVariable.Text = "Delete Control Variable";
            this.buttonRemoveControlVariable.UseVisualStyleBackColor = true;
            this.buttonRemoveControlVariable.Click += new System.EventHandler(this.buttonRemoveControlVariable_Click);
            // 
            // labelMinRangeOfVariable
            // 
            this.labelMinRangeOfVariable.AutoSize = true;
            this.labelMinRangeOfVariable.Location = new System.Drawing.Point(338, 184);
            this.labelMinRangeOfVariable.Name = "labelMinRangeOfVariable";
            this.labelMinRangeOfVariable.Size = new System.Drawing.Size(59, 13);
            this.labelMinRangeOfVariable.TabIndex = 28;
            this.labelMinRangeOfVariable.Text = "Min Range";
            // 
            // labelMaxRangeOfVariable
            // 
            this.labelMaxRangeOfVariable.AutoSize = true;
            this.labelMaxRangeOfVariable.Location = new System.Drawing.Point(338, 228);
            this.labelMaxRangeOfVariable.Name = "labelMaxRangeOfVariable";
            this.labelMaxRangeOfVariable.Size = new System.Drawing.Size(62, 13);
            this.labelMaxRangeOfVariable.TabIndex = 27;
            this.labelMaxRangeOfVariable.Text = "Max Range";
            // 
            // labelNameOfVariable
            // 
            this.labelNameOfVariable.AutoSize = true;
            this.labelNameOfVariable.Location = new System.Drawing.Point(338, 144);
            this.labelNameOfVariable.Name = "labelNameOfVariable";
            this.labelNameOfVariable.Size = new System.Drawing.Size(35, 13);
            this.labelNameOfVariable.TabIndex = 26;
            this.labelNameOfVariable.Text = "Name";
            // 
            // labelDataOfVariable
            // 
            this.labelDataOfVariable.AutoSize = true;
            this.labelDataOfVariable.Location = new System.Drawing.Point(338, 100);
            this.labelDataOfVariable.Name = "labelDataOfVariable";
            this.labelDataOfVariable.Size = new System.Drawing.Size(103, 13);
            this.labelDataOfVariable.TabIndex = 25;
            this.labelDataOfVariable.Text = "Data of the variable:";
            // 
            // labelControlVariables
            // 
            this.labelControlVariables.AutoSize = true;
            this.labelControlVariables.Location = new System.Drawing.Point(183, 140);
            this.labelControlVariables.Name = "labelControlVariables";
            this.labelControlVariables.Size = new System.Drawing.Size(89, 13);
            this.labelControlVariables.TabIndex = 24;
            this.labelControlVariables.Text = "Control Variables:";
            // 
            // labelListOfComponents
            // 
            this.labelListOfComponents.AutoSize = true;
            this.labelListOfComponents.Location = new System.Drawing.Point(20, 140);
            this.labelListOfComponents.Name = "labelListOfComponents";
            this.labelListOfComponents.Size = new System.Drawing.Size(100, 13);
            this.labelListOfComponents.TabIndex = 23;
            this.labelListOfComponents.Text = "List of Components:";
            // 
            // labelComponentTypes
            // 
            this.labelComponentTypes.AutoSize = true;
            this.labelComponentTypes.Location = new System.Drawing.Point(20, 69);
            this.labelComponentTypes.Name = "labelComponentTypes";
            this.labelComponentTypes.Size = new System.Drawing.Size(91, 13);
            this.labelComponentTypes.TabIndex = 22;
            this.labelComponentTypes.Text = "Component Type:";
            // 
            // listControlVariables
            // 
            this.listControlVariables.FormattingEnabled = true;
            this.listControlVariables.Location = new System.Drawing.Point(186, 162);
            this.listControlVariables.Name = "listControlVariables";
            this.listControlVariables.Size = new System.Drawing.Size(118, 212);
            this.listControlVariables.TabIndex = 17;
            this.listControlVariables.SelectedIndexChanged += new System.EventHandler(this.listControlVariables_SelectedIndexChanged);
            // 
            // listComponents
            // 
            this.listComponents.FormattingEnabled = true;
            this.listComponents.Location = new System.Drawing.Point(23, 162);
            this.listComponents.Name = "listComponents";
            this.listComponents.Size = new System.Drawing.Size(121, 212);
            this.listComponents.TabIndex = 16;
            this.listComponents.SelectedIndexChanged += new System.EventHandler(this.listComponents_SelectedIndexChanged);
            // 
            // comboComponentTypes
            // 
            this.comboComponentTypes.FormattingEnabled = true;
            this.comboComponentTypes.Location = new System.Drawing.Point(23, 95);
            this.comboComponentTypes.Name = "comboComponentTypes";
            this.comboComponentTypes.Size = new System.Drawing.Size(121, 21);
            this.comboComponentTypes.TabIndex = 15;
            this.comboComponentTypes.SelectedIndexChanged += new System.EventHandler(this.comboComponentTypes_SelectedIndexChanged);
            // 
            // labelDeleteControlVariableTitle
            // 
            this.labelDeleteControlVariableTitle.AutoSize = true;
            this.labelDeleteControlVariableTitle.Location = new System.Drawing.Point(20, 30);
            this.labelDeleteControlVariableTitle.Name = "labelDeleteControlVariableTitle";
            this.labelDeleteControlVariableTitle.Size = new System.Drawing.Size(118, 13);
            this.labelDeleteControlVariableTitle.TabIndex = 30;
            this.labelDeleteControlVariableTitle.Text = "Delete Control Variable:";
            // 
            // labelNameValue
            // 
            this.labelNameValue.AutoSize = true;
            this.labelNameValue.Location = new System.Drawing.Point(416, 144);
            this.labelNameValue.Name = "labelNameValue";
            this.labelNameValue.Size = new System.Drawing.Size(35, 13);
            this.labelNameValue.TabIndex = 31;
            this.labelNameValue.Text = "label1";
            // 
            // labelMinValue
            // 
            this.labelMinValue.AutoSize = true;
            this.labelMinValue.Location = new System.Drawing.Point(416, 184);
            this.labelMinValue.Name = "labelMinValue";
            this.labelMinValue.Size = new System.Drawing.Size(35, 13);
            this.labelMinValue.TabIndex = 32;
            this.labelMinValue.Text = "label2";
            // 
            // labelMaxValue
            // 
            this.labelMaxValue.AutoSize = true;
            this.labelMaxValue.Location = new System.Drawing.Point(416, 228);
            this.labelMaxValue.Name = "labelMaxValue";
            this.labelMaxValue.Size = new System.Drawing.Size(35, 13);
            this.labelMaxValue.TabIndex = 33;
            this.labelMaxValue.Text = "label3";
            // 
            // labelMinWarningOfVariable
            // 
            this.labelMinWarningOfVariable.AutoSize = true;
            this.labelMinWarningOfVariable.Location = new System.Drawing.Point(338, 263);
            this.labelMinWarningOfVariable.Name = "labelMinWarningOfVariable";
            this.labelMinWarningOfVariable.Size = new System.Drawing.Size(67, 13);
            this.labelMinWarningOfVariable.TabIndex = 34;
            this.labelMinWarningOfVariable.Text = "Min Warning";
            // 
            // labelMaxWarningOfVariable
            // 
            this.labelMaxWarningOfVariable.AutoSize = true;
            this.labelMaxWarningOfVariable.Location = new System.Drawing.Point(338, 299);
            this.labelMaxWarningOfVariable.Name = "labelMaxWarningOfVariable";
            this.labelMaxWarningOfVariable.Size = new System.Drawing.Size(70, 13);
            this.labelMaxWarningOfVariable.TabIndex = 35;
            this.labelMaxWarningOfVariable.Text = "Max Warning";
            // 
            // labelMinWarning
            // 
            this.labelMinWarning.AutoSize = true;
            this.labelMinWarning.Location = new System.Drawing.Point(416, 263);
            this.labelMinWarning.Name = "labelMinWarning";
            this.labelMinWarning.Size = new System.Drawing.Size(35, 13);
            this.labelMinWarning.TabIndex = 36;
            this.labelMinWarning.Text = "label4";
            // 
            // labelMaxWarning
            // 
            this.labelMaxWarning.AutoSize = true;
            this.labelMaxWarning.Location = new System.Drawing.Point(419, 299);
            this.labelMaxWarning.Name = "labelMaxWarning";
            this.labelMaxWarning.Size = new System.Drawing.Size(35, 13);
            this.labelMaxWarning.TabIndex = 37;
            this.labelMaxWarning.Text = "label5";
            // 
            // UCRemoveControlVariable
            // 
            this.Controls.Add(this.labelMaxWarning);
            this.Controls.Add(this.labelMinWarning);
            this.Controls.Add(this.labelMaxWarningOfVariable);
            this.Controls.Add(this.labelMinWarningOfVariable);
            this.Controls.Add(this.labelMaxValue);
            this.Controls.Add(this.labelMinValue);
            this.Controls.Add(this.labelNameValue);
            this.Controls.Add(this.labelDeleteControlVariableTitle);
            this.Controls.Add(this.buttonRemoveControlVariable);
            this.Controls.Add(this.labelMinRangeOfVariable);
            this.Controls.Add(this.labelMaxRangeOfVariable);
            this.Controls.Add(this.labelNameOfVariable);
            this.Controls.Add(this.labelDataOfVariable);
            this.Controls.Add(this.labelControlVariables);
            this.Controls.Add(this.labelListOfComponents);
            this.Controls.Add(this.labelComponentTypes);
            this.Controls.Add(this.listControlVariables);
            this.Controls.Add(this.listComponents);
            this.Controls.Add(this.comboComponentTypes);
            this.Name = "UCRemoveControlVariable";
            this.Size = new System.Drawing.Size(546, 393);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void buttonRemoveControlVariable_Click(object sender, System.EventArgs e)
        {
            try
            {
                Component selectedComponent = (Component)listComponents.SelectedItem;
                ControlVariable selectedVariable = (ControlVariable)listControlVariables.SelectedItem;
                scada.EraseControlVariable(selectedComponent, selectedVariable);
                uiScadaInstance.LoadControlPanel();
                EmptyFields(selectedComponent);
                MessageBox.Show("Control Variable successfully Deleted");
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

        private void EmptyFields(Component selectedComponent)
        {
            this.listControlVariables.DataSource = null;
            this.listControlVariables.DataSource = selectedComponent.ControlVariables;
            this.listControlVariables.DisplayMember = "Name";
        }

        private void comboComponentTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            uiScadaInstance.LoadSelectedComponentTypeFromComboOnList(comboComponentTypes, listComponents);
        }

        private void listComponents_SelectedIndexChanged(object sender, EventArgs e)
        {
            Component selected = (Component)listComponents.SelectedItem;
            uiScadaInstance.LoadControlVariablesFromSelectedComponent(selected, listControlVariables);
            if (listControlVariables.Items.Count == 0)
                SetInfoOfControlVariable("","","","","");
        }
        private void SetInfoOfControlVariable(string name,string minVal, string maxVal,string minWarning, string maxWarning)
        {
            this.labelNameValue.Text = name;
            this.labelMinValue.Text = minVal;
            this.labelMaxValue.Text = maxVal;
            this.labelMinWarning.Text = minWarning;
            this.labelMaxWarning.Text = maxWarning;
        }

        private void listControlVariables_SelectedIndexChanged(object sender, EventArgs e)
        {
            ControlVariable selected = (ControlVariable)listControlVariables.SelectedItem;

            if (selected == null)
                SetInfoOfControlVariable("","","","","");
            else
                SetInfoOfControlVariable((selected).Name, (selected).MinRange.ToString(), 
                    (selected).MaxRange.ToString(),(selected).MinRangeWarning.ToString(),
                    (selected).MaxRangeWarning.ToString());
        }
    }
}