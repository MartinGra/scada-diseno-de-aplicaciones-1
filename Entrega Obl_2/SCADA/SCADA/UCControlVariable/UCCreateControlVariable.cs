﻿using System.Windows.Forms;
using SCADADomain;
using System.Collections.ObjectModel;
using System;
using System.Globalization;
using static System.String;
using SCADAFacade;

namespace SCADA
{
    internal class UCCreateControlVariable : UserControl
    {
        private ComboBox comboComponentType;
        private ListBox listComponentSelection;
        private Label labelChildToAssign;
        private Label labelComponent;
        private IndustrialPlant scada;
        private Label labelControlVariableData;
        private TextBox textVariableName;
        private TextBox textMaxRange;
        private TextBox textMinRange;
        private Label labelVariableName;
        private Label labelMaxRange;
        private Label labelMinRange;
        private Button buttonRegisterControlVariable;
        private Label labelMinWarning;
        private Label labelMaxWarning;
        private TextBox textMinWarning;
        private TextBox textMaxWarning;
        private UISCADA uiScadaInstance;

        public UCCreateControlVariable(IndustrialPlant scada)
        {
            this.scada = scada;
            InitializeComponent();
            LoadComponentTypeSelector();
        }

        public UCCreateControlVariable(IndustrialPlant scada, UISCADA ui) : this(scada)
        {
            this.uiScadaInstance = ui;
        }

        private void LoadComponentTypeSelector()
        {
           Collection<string> posibleComponentsTypes = new Collection<string>();
           posibleComponentsTypes.Add("Installation");
           posibleComponentsTypes.Add("Device");
           comboComponentType.DataSource = posibleComponentsTypes;
        
        }
        private void InitializeComponent()
        {
            this.comboComponentType = new System.Windows.Forms.ComboBox();
            this.listComponentSelection = new System.Windows.Forms.ListBox();
            this.labelChildToAssign = new System.Windows.Forms.Label();
            this.labelComponent = new System.Windows.Forms.Label();
            this.labelControlVariableData = new System.Windows.Forms.Label();
            this.textVariableName = new System.Windows.Forms.TextBox();
            this.textMaxRange = new System.Windows.Forms.TextBox();
            this.textMinRange = new System.Windows.Forms.TextBox();
            this.labelVariableName = new System.Windows.Forms.Label();
            this.labelMaxRange = new System.Windows.Forms.Label();
            this.labelMinRange = new System.Windows.Forms.Label();
            this.buttonRegisterControlVariable = new System.Windows.Forms.Button();
            this.labelMinWarning = new System.Windows.Forms.Label();
            this.labelMaxWarning = new System.Windows.Forms.Label();
            this.textMinWarning = new System.Windows.Forms.TextBox();
            this.textMaxWarning = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // comboComponentType
            // 
            this.comboComponentType.FormattingEnabled = true;
            this.comboComponentType.Location = new System.Drawing.Point(19, 37);
            this.comboComponentType.Name = "comboComponentType";
            this.comboComponentType.Size = new System.Drawing.Size(121, 21);
            this.comboComponentType.TabIndex = 0;
            this.comboComponentType.SelectedIndexChanged += new System.EventHandler(this.comboComponentType_SelectedIndexChanged);
            // 
            // listComponentSelection
            // 
            this.listComponentSelection.FormattingEnabled = true;
            this.listComponentSelection.Location = new System.Drawing.Point(19, 94);
            this.listComponentSelection.Name = "listComponentSelection";
            this.listComponentSelection.Size = new System.Drawing.Size(159, 225);
            this.listComponentSelection.TabIndex = 1;
            // 
            // labelChildToAssign
            // 
            this.labelChildToAssign.AutoSize = true;
            this.labelChildToAssign.Location = new System.Drawing.Point(19, 75);
            this.labelChildToAssign.Name = "labelChildToAssign";
            this.labelChildToAssign.Size = new System.Drawing.Size(37, 13);
            this.labelChildToAssign.TabIndex = 2;
            this.labelChildToAssign.Text = "Select";
            // 
            // labelComponent
            // 
            this.labelComponent.AutoSize = true;
            this.labelComponent.Location = new System.Drawing.Point(19, 18);
            this.labelComponent.Name = "labelComponent";
            this.labelComponent.Size = new System.Drawing.Size(91, 13);
            this.labelComponent.TabIndex = 3;
            this.labelComponent.Text = "Component Type:";
            // 
            // labelControlVariableData
            // 
            this.labelControlVariableData.AutoSize = true;
            this.labelControlVariableData.Location = new System.Drawing.Point(345, 54);
            this.labelControlVariableData.Name = "labelControlVariableData";
            this.labelControlVariableData.Size = new System.Drawing.Size(110, 13);
            this.labelControlVariableData.TabIndex = 4;
            this.labelControlVariableData.Text = "Control Variable Data:";
            // 
            // textVariableName
            // 
            this.textVariableName.Location = new System.Drawing.Point(355, 94);
            this.textVariableName.Name = "textVariableName";
            this.textVariableName.Size = new System.Drawing.Size(100, 20);
            this.textVariableName.TabIndex = 5;
            // 
            // textMaxRange
            // 
            this.textMaxRange.Location = new System.Drawing.Point(355, 192);
            this.textMaxRange.Name = "textMaxRange";
            this.textMaxRange.Size = new System.Drawing.Size(100, 20);
            this.textMaxRange.TabIndex = 6;
            // 
            // textMinRange
            // 
            this.textMinRange.Location = new System.Drawing.Point(355, 142);
            this.textMinRange.Name = "textMinRange";
            this.textMinRange.Size = new System.Drawing.Size(100, 20);
            this.textMinRange.TabIndex = 7;
            // 
            // labelVariableName
            // 
            this.labelVariableName.AutoSize = true;
            this.labelVariableName.Location = new System.Drawing.Point(282, 97);
            this.labelVariableName.Name = "labelVariableName";
            this.labelVariableName.Size = new System.Drawing.Size(35, 13);
            this.labelVariableName.TabIndex = 8;
            this.labelVariableName.Text = "Name";
            // 
            // labelMaxRange
            // 
            this.labelMaxRange.AutoSize = true;
            this.labelMaxRange.Location = new System.Drawing.Point(279, 195);
            this.labelMaxRange.Name = "labelMaxRange";
            this.labelMaxRange.Size = new System.Drawing.Size(62, 13);
            this.labelMaxRange.TabIndex = 9;
            this.labelMaxRange.Text = "Max Range";
            // 
            // labelMinRange
            // 
            this.labelMinRange.AutoSize = true;
            this.labelMinRange.Location = new System.Drawing.Point(282, 145);
            this.labelMinRange.Name = "labelMinRange";
            this.labelMinRange.Size = new System.Drawing.Size(59, 13);
            this.labelMinRange.TabIndex = 10;
            this.labelMinRange.Text = "Min Range";
            // 
            // buttonRegisterControlVariable
            // 
            this.buttonRegisterControlVariable.Location = new System.Drawing.Point(355, 369);
            this.buttonRegisterControlVariable.Name = "buttonRegisterControlVariable";
            this.buttonRegisterControlVariable.Size = new System.Drawing.Size(75, 23);
            this.buttonRegisterControlVariable.TabIndex = 11;
            this.buttonRegisterControlVariable.Text = "Register";
            this.buttonRegisterControlVariable.UseVisualStyleBackColor = true;
            this.buttonRegisterControlVariable.Click += new System.EventHandler(this.buttonRegisterControlVariable_Click);
            // 
            // labelMinWarning
            // 
            this.labelMinWarning.AutoSize = true;
            this.labelMinWarning.Location = new System.Drawing.Point(279, 241);
            this.labelMinWarning.Name = "labelMinWarning";
            this.labelMinWarning.Size = new System.Drawing.Size(67, 13);
            this.labelMinWarning.TabIndex = 12;
            this.labelMinWarning.Text = "Min Warning";
            // 
            // labelMaxWarning
            // 
            this.labelMaxWarning.AutoSize = true;
            this.labelMaxWarning.Location = new System.Drawing.Point(279, 282);
            this.labelMaxWarning.Name = "labelMaxWarning";
            this.labelMaxWarning.Size = new System.Drawing.Size(70, 13);
            this.labelMaxWarning.TabIndex = 13;
            this.labelMaxWarning.Text = "Max Warning";
            // 
            // textMinWarning
            // 
            this.textMinWarning.Location = new System.Drawing.Point(355, 238);
            this.textMinWarning.Name = "textMinWarning";
            this.textMinWarning.Size = new System.Drawing.Size(100, 20);
            this.textMinWarning.TabIndex = 14;
            // 
            // textMaxWarning
            // 
            this.textMaxWarning.Location = new System.Drawing.Point(355, 279);
            this.textMaxWarning.Name = "textMaxWarning";
            this.textMaxWarning.Size = new System.Drawing.Size(100, 20);
            this.textMaxWarning.TabIndex = 15;
            // 
            // UCCreateControlVariable
            // 
            this.Controls.Add(this.textMaxWarning);
            this.Controls.Add(this.textMinWarning);
            this.Controls.Add(this.labelMaxWarning);
            this.Controls.Add(this.labelMinWarning);
            this.Controls.Add(this.buttonRegisterControlVariable);
            this.Controls.Add(this.labelMinRange);
            this.Controls.Add(this.labelMaxRange);
            this.Controls.Add(this.labelVariableName);
            this.Controls.Add(this.textMinRange);
            this.Controls.Add(this.textMaxRange);
            this.Controls.Add(this.textVariableName);
            this.Controls.Add(this.labelControlVariableData);
            this.Controls.Add(this.labelComponent);
            this.Controls.Add(this.labelChildToAssign);
            this.Controls.Add(this.listComponentSelection);
            this.Controls.Add(this.comboComponentType);
            this.Name = "UCCreateControlVariable";
            this.Size = new System.Drawing.Size(626, 463);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void comboComponentType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            string selected = (string)comboComponentType.SelectedItem;

            if (selected.Equals("Installation"))
                listComponentSelection.DataSource = scada.Installations;
            else
                listComponentSelection.DataSource = scada.Devices;

            listComponentSelection.DisplayMember = "Name";
        }

        private void buttonRegisterControlVariable_Click(object sender, EventArgs e)
        {
            try
            {
                Component selectedComponent = (Component)listComponentSelection.SelectedItem;
                string variableName;
                float variableMaxRange, variableMinRange, maxWarning, minWarning;
                GrabUIData(out variableName, out variableMaxRange, out variableMinRange, out maxWarning, out minWarning); scada.CreateControlVariableWithWarningRanges(selectedComponent, variableName, variableMinRange, variableMaxRange, minWarning, maxWarning);
                MessageBox.Show("Correctly Registered: Control Variable");
                EmptyFields();
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

        private void GrabUIData(out string variableName, out float variableMaxRange, out float variableMinRange, out float maxWarning, out float minWarning)
        {
            variableName = textVariableName.Text;
            variableMaxRange = float.Parse(textMaxRange.Text, CultureInfo.InvariantCulture.NumberFormat);
            variableMinRange = float.Parse(textMinRange.Text, CultureInfo.InvariantCulture.NumberFormat);
            maxWarning = float.Parse(textMaxWarning.Text, CultureInfo.InvariantCulture.NumberFormat);
            minWarning = float.Parse(textMinWarning.Text, CultureInfo.InvariantCulture.NumberFormat);
        }

        private void EmptyFields()
        {
            textVariableName.Text = Empty;
            textMaxRange.Text = Empty;
            textMinRange.Text = Empty;
            textMaxWarning.Text = Empty;
            textMinWarning.Text = Empty;
        }
    }
}