﻿using System;
using System.Windows.Forms;
using SCADADomain;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using SCADAFacade;

namespace SCADA
{
    internal class UCUpdateControlVariable : UserControl
    {
        private IndustrialPlant scada;
        private ComboBox comboComponentTypes;
        private ListBox listComponents;
        private ListBox listControlVariables;
        private TextBox textNameOfVariable;
        private TextBox textMaxRangeOfVariable;
        private TextBox textMinRangeOfVariable;
        private Label labelComponentTypes;
        private Label labelListOfComponents;
        private Label labelControlVariables;
        private Label labelDataOfVariable;
        private Label labelNameOfVariable;
        private Label labelMaxRangeOfVariable;
        private Label labelMinRangeOfVariable;
        private Button buttonUpdateVariable;
        private Label labelMinWarning;
        private Label labelMaxWarning;
        private TextBox textMinWarning;
        private TextBox textMaxWarning;
        private UISCADA uiScadaInstance;

        public UCUpdateControlVariable(IndustrialPlant scada, UISCADA uISCADA)
        {
            this.scada = scada;
            this.uiScadaInstance = uISCADA;
            InitializeComponent();
            uiScadaInstance.LoadComponentTypesOnComboBox(comboComponentTypes);
        }

        private void InitializeComponent()
        {
            this.comboComponentTypes = new System.Windows.Forms.ComboBox();
            this.listComponents = new System.Windows.Forms.ListBox();
            this.listControlVariables = new System.Windows.Forms.ListBox();
            this.textNameOfVariable = new System.Windows.Forms.TextBox();
            this.textMaxRangeOfVariable = new System.Windows.Forms.TextBox();
            this.textMinRangeOfVariable = new System.Windows.Forms.TextBox();
            this.labelComponentTypes = new System.Windows.Forms.Label();
            this.labelListOfComponents = new System.Windows.Forms.Label();
            this.labelControlVariables = new System.Windows.Forms.Label();
            this.labelDataOfVariable = new System.Windows.Forms.Label();
            this.labelNameOfVariable = new System.Windows.Forms.Label();
            this.labelMaxRangeOfVariable = new System.Windows.Forms.Label();
            this.labelMinRangeOfVariable = new System.Windows.Forms.Label();
            this.buttonUpdateVariable = new System.Windows.Forms.Button();
            this.labelMinWarning = new System.Windows.Forms.Label();
            this.labelMaxWarning = new System.Windows.Forms.Label();
            this.textMinWarning = new System.Windows.Forms.TextBox();
            this.textMaxWarning = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // comboComponentTypes
            // 
            this.comboComponentTypes.FormattingEnabled = true;
            this.comboComponentTypes.Location = new System.Drawing.Point(36, 52);
            this.comboComponentTypes.Name = "comboComponentTypes";
            this.comboComponentTypes.Size = new System.Drawing.Size(121, 21);
            this.comboComponentTypes.TabIndex = 0;
            this.comboComponentTypes.SelectedIndexChanged += new System.EventHandler(this.comboComponentTypes_SelectedIndexChanged);
            // 
            // listComponents
            // 
            this.listComponents.FormattingEnabled = true;
            this.listComponents.Location = new System.Drawing.Point(36, 119);
            this.listComponents.Name = "listComponents";
            this.listComponents.Size = new System.Drawing.Size(121, 212);
            this.listComponents.TabIndex = 1;
            this.listComponents.SelectedIndexChanged += new System.EventHandler(this.listComponents_SelectedIndexChanged);
            // 
            // listControlVariables
            // 
            this.listControlVariables.FormattingEnabled = true;
            this.listControlVariables.Location = new System.Drawing.Point(199, 119);
            this.listControlVariables.Name = "listControlVariables";
            this.listControlVariables.Size = new System.Drawing.Size(118, 212);
            this.listControlVariables.TabIndex = 2;
            this.listControlVariables.SelectedIndexChanged += new System.EventHandler(this.listControlVariables_SelectedIndexChanged);
            // 
            // textNameOfVariable
            // 
            this.textNameOfVariable.Location = new System.Drawing.Point(420, 163);
            this.textNameOfVariable.Name = "textNameOfVariable";
            this.textNameOfVariable.Size = new System.Drawing.Size(100, 20);
            this.textNameOfVariable.TabIndex = 3;
            // 
            // textMaxRangeOfVariable
            // 
            this.textMaxRangeOfVariable.Location = new System.Drawing.Point(420, 249);
            this.textMaxRangeOfVariable.Name = "textMaxRangeOfVariable";
            this.textMaxRangeOfVariable.Size = new System.Drawing.Size(100, 20);
            this.textMaxRangeOfVariable.TabIndex = 4;
            // 
            // textMinRangeOfVariable
            // 
            this.textMinRangeOfVariable.Location = new System.Drawing.Point(421, 205);
            this.textMinRangeOfVariable.Name = "textMinRangeOfVariable";
            this.textMinRangeOfVariable.Size = new System.Drawing.Size(100, 20);
            this.textMinRangeOfVariable.TabIndex = 5;
            // 
            // labelComponentTypes
            // 
            this.labelComponentTypes.AutoSize = true;
            this.labelComponentTypes.Location = new System.Drawing.Point(33, 26);
            this.labelComponentTypes.Name = "labelComponentTypes";
            this.labelComponentTypes.Size = new System.Drawing.Size(91, 13);
            this.labelComponentTypes.TabIndex = 7;
            this.labelComponentTypes.Text = "Component Type:";
            // 
            // labelListOfComponents
            // 
            this.labelListOfComponents.AutoSize = true;
            this.labelListOfComponents.Location = new System.Drawing.Point(33, 97);
            this.labelListOfComponents.Name = "labelListOfComponents";
            this.labelListOfComponents.Size = new System.Drawing.Size(100, 13);
            this.labelListOfComponents.TabIndex = 8;
            this.labelListOfComponents.Text = "List of Components:";
            // 
            // labelControlVariables
            // 
            this.labelControlVariables.AutoSize = true;
            this.labelControlVariables.Location = new System.Drawing.Point(196, 97);
            this.labelControlVariables.Name = "labelControlVariables";
            this.labelControlVariables.Size = new System.Drawing.Size(89, 13);
            this.labelControlVariables.TabIndex = 9;
            this.labelControlVariables.Text = "Control Variables:";
            // 
            // labelDataOfVariable
            // 
            this.labelDataOfVariable.AutoSize = true;
            this.labelDataOfVariable.Location = new System.Drawing.Point(344, 122);
            this.labelDataOfVariable.Name = "labelDataOfVariable";
            this.labelDataOfVariable.Size = new System.Drawing.Size(132, 13);
            this.labelDataOfVariable.TabIndex = 10;
            this.labelDataOfVariable.Text = "Information of the variable:";
            // 
            // labelNameOfVariable
            // 
            this.labelNameOfVariable.AutoSize = true;
            this.labelNameOfVariable.Location = new System.Drawing.Point(344, 166);
            this.labelNameOfVariable.Name = "labelNameOfVariable";
            this.labelNameOfVariable.Size = new System.Drawing.Size(35, 13);
            this.labelNameOfVariable.TabIndex = 11;
            this.labelNameOfVariable.Text = "Name";
            // 
            // labelMaxRangeOfVariable
            // 
            this.labelMaxRangeOfVariable.AutoSize = true;
            this.labelMaxRangeOfVariable.Location = new System.Drawing.Point(344, 252);
            this.labelMaxRangeOfVariable.Name = "labelMaxRangeOfVariable";
            this.labelMaxRangeOfVariable.Size = new System.Drawing.Size(62, 13);
            this.labelMaxRangeOfVariable.TabIndex = 12;
            this.labelMaxRangeOfVariable.Text = "Max Range";
            // 
            // labelMinRangeOfVariable
            // 
            this.labelMinRangeOfVariable.AutoSize = true;
            this.labelMinRangeOfVariable.Location = new System.Drawing.Point(344, 208);
            this.labelMinRangeOfVariable.Name = "labelMinRangeOfVariable";
            this.labelMinRangeOfVariable.Size = new System.Drawing.Size(59, 13);
            this.labelMinRangeOfVariable.TabIndex = 13;
            this.labelMinRangeOfVariable.Text = "Min Range";
            // 
            // buttonUpdateVariable
            // 
            this.buttonUpdateVariable.Location = new System.Drawing.Point(418, 367);
            this.buttonUpdateVariable.Name = "buttonUpdateVariable";
            this.buttonUpdateVariable.Size = new System.Drawing.Size(102, 23);
            this.buttonUpdateVariable.TabIndex = 14;
            this.buttonUpdateVariable.Text = "Update";
            this.buttonUpdateVariable.UseVisualStyleBackColor = true;
            this.buttonUpdateVariable.Click += new System.EventHandler(this.buttonUpdateRanges_Click);
            // 
            // labelMinWarning
            // 
            this.labelMinWarning.AutoSize = true;
            this.labelMinWarning.Location = new System.Drawing.Point(344, 294);
            this.labelMinWarning.Name = "labelMinWarning";
            this.labelMinWarning.Size = new System.Drawing.Size(67, 13);
            this.labelMinWarning.TabIndex = 16;
            this.labelMinWarning.Text = "Min Warning";
            // 
            // labelMaxWarning
            // 
            this.labelMaxWarning.AutoSize = true;
            this.labelMaxWarning.Location = new System.Drawing.Point(344, 335);
            this.labelMaxWarning.Name = "labelMaxWarning";
            this.labelMaxWarning.Size = new System.Drawing.Size(70, 13);
            this.labelMaxWarning.TabIndex = 17;
            this.labelMaxWarning.Text = "Max Warning";
            // 
            // textMinWarning
            // 
            this.textMinWarning.Location = new System.Drawing.Point(420, 291);
            this.textMinWarning.Name = "textMinWarning";
            this.textMinWarning.Size = new System.Drawing.Size(100, 20);
            this.textMinWarning.TabIndex = 18;
            // 
            // textMaxWarning
            // 
            this.textMaxWarning.Location = new System.Drawing.Point(421, 332);
            this.textMaxWarning.Name = "textMaxWarning";
            this.textMaxWarning.Size = new System.Drawing.Size(100, 20);
            this.textMaxWarning.TabIndex = 19;
            // 
            // UCUpdateControlVariable
            // 
            this.Controls.Add(this.textMaxWarning);
            this.Controls.Add(this.textMinWarning);
            this.Controls.Add(this.labelMaxWarning);
            this.Controls.Add(this.labelMinWarning);
            this.Controls.Add(this.buttonUpdateVariable);
            this.Controls.Add(this.labelMinRangeOfVariable);
            this.Controls.Add(this.labelMaxRangeOfVariable);
            this.Controls.Add(this.labelNameOfVariable);
            this.Controls.Add(this.labelDataOfVariable);
            this.Controls.Add(this.labelControlVariables);
            this.Controls.Add(this.labelListOfComponents);
            this.Controls.Add(this.labelComponentTypes);
            this.Controls.Add(this.textMinRangeOfVariable);
            this.Controls.Add(this.textMaxRangeOfVariable);
            this.Controls.Add(this.textNameOfVariable);
            this.Controls.Add(this.listControlVariables);
            this.Controls.Add(this.listComponents);
            this.Controls.Add(this.comboComponentTypes);
            this.Name = "UCUpdateControlVariable";
            this.Size = new System.Drawing.Size(553, 506);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void comboComponentTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            uiScadaInstance.LoadSelectedComponentTypeFromComboOnList(comboComponentTypes, listComponents);
        }

        private void listComponents_SelectedIndexChanged(object sender, EventArgs e)
        {
            Component selected = (Component)listComponents.SelectedItem;
            uiScadaInstance.LoadControlVariablesFromSelectedComponent(selected,listControlVariables);
        }

        private void listControlVariables_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ControlVariable selected = (ControlVariable)listControlVariables.SelectedItem;
                textNameOfVariable.Text = selected.Name;
                textMaxRangeOfVariable.Text = Convert.ToString(selected.MaxRange);
                textMinRangeOfVariable.Text = Convert.ToString(selected.MinRange);
                textMinWarning.Text = Convert.ToString(selected.MinRangeWarning);
                textMaxWarning.Text = Convert.ToString(selected.MaxRangeWarning);
            }
            catch (Exception)
            {
            }
        }

        private void buttonUpdateRanges_Click(object sender, EventArgs e)
        {
            try
            {
                Component selectedComponent;
                ControlVariable selectedVariable;
                float updatedMinRange, updatedMaxRange, updatedMinWarning, updatedMaxWarning;
                LoadUIData(out selectedComponent, out selectedVariable, out updatedMinRange, out updatedMaxRange, out updatedMinWarning, out updatedMaxWarning);

                scada.ModifyControlVariableWithWarning(selectedComponent, selectedVariable, textNameOfVariable.Text, updatedMinRange, updatedMaxRange, updatedMinWarning, updatedMaxWarning);
                MessageBox.Show("Correctly Updated: Device ranges");
                EmptyFields();

                listControlVariables.DataSource = selectedComponent.ControlVariables.ToList();
                listControlVariables.DisplayMember = "Name";

                uiScadaInstance.LoadControlPanel();

            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

        private void LoadUIData(out Component selectedComponent, out ControlVariable selectedVariable, out float updatedMinRange, out float updatedMaxRange, out float updatedMinWarning, out float updatedMaxWarning)
        {
            selectedComponent = (Component)listComponents.SelectedItem;
            selectedVariable = (ControlVariable)listControlVariables.SelectedItem;
            updatedMinRange = float.Parse(textMinRangeOfVariable.Text, CultureInfo.InvariantCulture.NumberFormat);
            updatedMaxRange = float.Parse(textMaxRangeOfVariable.Text, CultureInfo.InvariantCulture.NumberFormat);
            updatedMinWarning = float.Parse(textMinWarning.Text, CultureInfo.InvariantCulture.NumberFormat);
            updatedMaxWarning = float.Parse(textMaxWarning.Text, CultureInfo.InvariantCulture.NumberFormat);
        }

        private void EmptyFields()
        {
            textMinRangeOfVariable.Text = string.Empty;
            textMaxRangeOfVariable.Text = string.Empty;
            textMinWarning.Text = string.Empty;
            textMaxWarning.Text = string.Empty;
        }
    }
}