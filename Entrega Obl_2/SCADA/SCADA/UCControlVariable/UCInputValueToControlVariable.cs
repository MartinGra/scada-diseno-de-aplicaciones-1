﻿using System.Windows.Forms;
using SCADADomain;
using System;
using System.Globalization;
using SCADAFacade;

namespace SCADA
{
    internal class UCInputValueToControlVariable : UserControl
    {
        private IndustrialPlant scada;
        private ComboBox comboComponentTypes;
        private ListBox listComponents;
        private ListBox listControlVariables;
        private Label labelComponentType;
        private Label labelComponentList;
        private Label labelControlVariables;
        private Label labelAssignValue;
        private Label labelInput;
        private TextBox textInputValue;
        private Button buttonAssignValue;
        private UISCADA uiScadaInstance;

        public UCInputValueToControlVariable(IndustrialPlant scada, UISCADA uISCADA)
        {
            this.scada = scada;
            this.uiScadaInstance = uISCADA;
            InitializeComponent();
            uiScadaInstance.LoadComponentTypesOnComboBox(comboComponentTypes);
        }

        private void InitializeComponent()
        {
            this.comboComponentTypes = new System.Windows.Forms.ComboBox();
            this.listComponents = new System.Windows.Forms.ListBox();
            this.listControlVariables = new System.Windows.Forms.ListBox();
            this.labelComponentType = new System.Windows.Forms.Label();
            this.labelComponentList = new System.Windows.Forms.Label();
            this.labelControlVariables = new System.Windows.Forms.Label();
            this.labelAssignValue = new System.Windows.Forms.Label();
            this.labelInput = new System.Windows.Forms.Label();
            this.textInputValue = new System.Windows.Forms.TextBox();
            this.buttonAssignValue = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboComponentTypes
            // 
            this.comboComponentTypes.FormattingEnabled = true;
            this.comboComponentTypes.Location = new System.Drawing.Point(30, 38);
            this.comboComponentTypes.Name = "comboComponentTypes";
            this.comboComponentTypes.Size = new System.Drawing.Size(121, 21);
            this.comboComponentTypes.TabIndex = 0;
            this.comboComponentTypes.SelectedIndexChanged += new System.EventHandler(this.comboComponentTypes_SelectedIndexChanged);
            // 
            // listComponents
            // 
            this.listComponents.FormattingEnabled = true;
            this.listComponents.Location = new System.Drawing.Point(30, 101);
            this.listComponents.Name = "listComponents";
            this.listComponents.Size = new System.Drawing.Size(120, 225);
            this.listComponents.TabIndex = 1;
            this.listComponents.SelectedIndexChanged += new System.EventHandler(this.listComponents_SelectedIndexChanged);
            // 
            // listControlVariables
            // 
            this.listControlVariables.FormattingEnabled = true;
            this.listControlVariables.Location = new System.Drawing.Point(173, 101);
            this.listControlVariables.Name = "listControlVariables";
            this.listControlVariables.Size = new System.Drawing.Size(120, 225);
            this.listControlVariables.TabIndex = 2;
            // 
            // labelComponentType
            // 
            this.labelComponentType.AutoSize = true;
            this.labelComponentType.Location = new System.Drawing.Point(27, 13);
            this.labelComponentType.Name = "labelComponentType";
            this.labelComponentType.Size = new System.Drawing.Size(87, 13);
            this.labelComponentType.TabIndex = 3;
            this.labelComponentType.Text = "Component type:";
            // 
            // labelComponentList
            // 
            this.labelComponentList.AutoSize = true;
            this.labelComponentList.Location = new System.Drawing.Point(27, 75);
            this.labelComponentList.Name = "labelComponentList";
            this.labelComponentList.Size = new System.Drawing.Size(79, 13);
            this.labelComponentList.TabIndex = 4;
            this.labelComponentList.Text = "Component list:";
            // 
            // labelControlVariables
            // 
            this.labelControlVariables.AutoSize = true;
            this.labelControlVariables.Location = new System.Drawing.Point(170, 75);
            this.labelControlVariables.Name = "labelControlVariables";
            this.labelControlVariables.Size = new System.Drawing.Size(88, 13);
            this.labelControlVariables.TabIndex = 5;
            this.labelControlVariables.Text = "Control variables:";
            // 
            // labelAssignValue
            // 
            this.labelAssignValue.AutoSize = true;
            this.labelAssignValue.Location = new System.Drawing.Point(333, 101);
            this.labelAssignValue.Name = "labelAssignValue";
            this.labelAssignValue.Size = new System.Drawing.Size(151, 13);
            this.labelAssignValue.TabIndex = 6;
            this.labelAssignValue.Text = "Assign real number to variable:";
            // 
            // labelInput
            // 
            this.labelInput.AutoSize = true;
            this.labelInput.Location = new System.Drawing.Point(333, 142);
            this.labelInput.Name = "labelInput";
            this.labelInput.Size = new System.Drawing.Size(31, 13);
            this.labelInput.TabIndex = 7;
            this.labelInput.Text = "Input";
            // 
            // textInputValue
            // 
            this.textInputValue.Location = new System.Drawing.Point(384, 139);
            this.textInputValue.Name = "textInputValue";
            this.textInputValue.Size = new System.Drawing.Size(100, 20);
            this.textInputValue.TabIndex = 8;
            // 
            // buttonAssignValue
            // 
            this.buttonAssignValue.Location = new System.Drawing.Point(370, 190);
            this.buttonAssignValue.Name = "buttonAssignValue";
            this.buttonAssignValue.Size = new System.Drawing.Size(75, 23);
            this.buttonAssignValue.TabIndex = 9;
            this.buttonAssignValue.Text = "Assign";
            this.buttonAssignValue.UseVisualStyleBackColor = true;
            this.buttonAssignValue.Click += new System.EventHandler(this.buttonAssignValue_Click);
            // 
            // UCInputValueToControlVariable
            // 
            this.Controls.Add(this.buttonAssignValue);
            this.Controls.Add(this.textInputValue);
            this.Controls.Add(this.labelInput);
            this.Controls.Add(this.labelAssignValue);
            this.Controls.Add(this.labelComponentType);
            this.Controls.Add(this.labelComponentList);
            this.Controls.Add(this.labelControlVariables);
            this.Controls.Add(this.listComponents);
            this.Controls.Add(this.listControlVariables);
            this.Controls.Add(this.comboComponentTypes);
            this.Name = "UCInputValueToControlVariable";
            this.Size = new System.Drawing.Size(533, 429);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void comboComponentTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            uiScadaInstance.LoadSelectedComponentTypeFromComboOnList(comboComponentTypes, listComponents);
        }

        private void listComponents_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            Component selected = (Component)listComponents.SelectedItem;
            uiScadaInstance.LoadControlVariablesFromSelectedComponent(selected,listControlVariables);
        }

        private void buttonAssignValue_Click(object sender, System.EventArgs e)
        {
            try
            {
                Component selectedComponent;
                ControlVariable selectedVariable;
                float inputValue;
                LoadUIData(out selectedComponent, out selectedVariable, out inputValue); scada.ControlVariableAssignValueInComponent(selectedComponent, selectedVariable, inputValue);
                uiScadaInstance.LoadControlPanel();
                this.textInputValue.Text = string.Empty;
                MessageBox.Show("Value assigned successfully");
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

        private void LoadUIData(out Component selectedComponent, out ControlVariable selectedVariable, out float inputValue)
        {
            selectedComponent = (Component)listComponents.SelectedItem;
            selectedVariable = (ControlVariable)listControlVariables.SelectedItem;
            inputValue = float.Parse(textInputValue.Text, CultureInfo.InvariantCulture.NumberFormat);
        }
    }
}