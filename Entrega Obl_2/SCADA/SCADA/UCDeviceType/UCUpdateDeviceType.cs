﻿using System.Windows.Forms;
using SCADADomain;
using System;
using SCADAFacade;

namespace SCADA
{
    internal class UCUpdateDeviceType : UserControl
    {
        private IndustrialPlant scada;
        private Button buttonUpdateDeviceType;
        private TextBox textBoxName;
        private Label labelDescription;
        private Label labelName;
        private Label labelUpdateDeviceType;
        private ListBox listBoxDeviceTypes;
        private Label labelDeviceTypeData;
        private RichTextBox richTextDescription;
        private Label labelListDeviceTypes;

        public UCUpdateDeviceType(IndustrialPlant scada)
        {
            this.scada = scada;
            InitializeComponent();
            listBoxDeviceTypes.DataSource = scada.DeviceTypes;
            listBoxDeviceTypes.DisplayMember = "Name";
            ListBoxDeviceTypes_SelectedValueChanged(null,null);
            listBoxDeviceTypes.SelectedValueChanged +=
                new EventHandler(ListBoxDeviceTypes_SelectedValueChanged);
        }

        private void ListBoxDeviceTypes_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                this.textBoxName.Text = ((DeviceType)listBoxDeviceTypes.SelectedItem).Name;
                this.richTextDescription.Text = ((DeviceType)listBoxDeviceTypes.SelectedItem).Description;
            }
            catch(Exception)
            {
            }
        }

        private void InitializeComponent()
        {
            this.buttonUpdateDeviceType = new System.Windows.Forms.Button();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.labelDescription = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.labelUpdateDeviceType = new System.Windows.Forms.Label();
            this.listBoxDeviceTypes = new System.Windows.Forms.ListBox();
            this.labelListDeviceTypes = new System.Windows.Forms.Label();
            this.labelDeviceTypeData = new System.Windows.Forms.Label();
            this.richTextDescription = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // buttonUpdateDeviceType
            // 
            this.buttonUpdateDeviceType.Location = new System.Drawing.Point(253, 291);
            this.buttonUpdateDeviceType.Name = "buttonUpdateDeviceType";
            this.buttonUpdateDeviceType.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdateDeviceType.TabIndex = 11;
            this.buttonUpdateDeviceType.Text = "Update";
            this.buttonUpdateDeviceType.UseVisualStyleBackColor = true;
            this.buttonUpdateDeviceType.Click += new System.EventHandler(this.buttonUpdateDeviceType_Click);
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(337, 165);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(100, 20);
            this.textBoxName.TabIndex = 9;
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Location = new System.Drawing.Point(250, 206);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(60, 13);
            this.labelDescription.TabIndex = 8;
            this.labelDescription.Text = "Description";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(250, 168);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 7;
            this.labelName.Text = "Name";
            // 
            // labelUpdateDeviceType
            // 
            this.labelUpdateDeviceType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelUpdateDeviceType.AutoSize = true;
            this.labelUpdateDeviceType.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelUpdateDeviceType.Location = new System.Drawing.Point(56, 52);
            this.labelUpdateDeviceType.Name = "labelUpdateDeviceType";
            this.labelUpdateDeviceType.Size = new System.Drawing.Size(106, 13);
            this.labelUpdateDeviceType.TabIndex = 6;
            this.labelUpdateDeviceType.Text = "Update Device Type";
            this.labelUpdateDeviceType.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // listBoxDeviceTypes
            // 
            this.listBoxDeviceTypes.FormattingEnabled = true;
            this.listBoxDeviceTypes.Location = new System.Drawing.Point(59, 122);
            this.listBoxDeviceTypes.Name = "listBoxDeviceTypes";
            this.listBoxDeviceTypes.Size = new System.Drawing.Size(132, 225);
            this.listBoxDeviceTypes.TabIndex = 12;
            // 
            // labelListDeviceTypes
            // 
            this.labelListDeviceTypes.AutoSize = true;
            this.labelListDeviceTypes.Location = new System.Drawing.Point(56, 92);
            this.labelListDeviceTypes.Name = "labelListDeviceTypes";
            this.labelListDeviceTypes.Size = new System.Drawing.Size(104, 13);
            this.labelListDeviceTypes.TabIndex = 13;
            this.labelListDeviceTypes.Text = "Select Device Type:";
            // 
            // labelDeviceTypeData
            // 
            this.labelDeviceTypeData.AutoSize = true;
            this.labelDeviceTypeData.Location = new System.Drawing.Point(250, 122);
            this.labelDeviceTypeData.Name = "labelDeviceTypeData";
            this.labelDeviceTypeData.Size = new System.Drawing.Size(123, 13);
            this.labelDeviceTypeData.TabIndex = 14;
            this.labelDeviceTypeData.Text = "Device Type Information";
            // 
            // richTextDescription
            // 
            this.richTextDescription.Location = new System.Drawing.Point(337, 203);
            this.richTextDescription.Name = "richTextDescription";
            this.richTextDescription.Size = new System.Drawing.Size(193, 57);
            this.richTextDescription.TabIndex = 15;
            this.richTextDescription.Text = "";
            // 
            // UCUpdateDeviceType
            // 
            this.Controls.Add(this.richTextDescription);
            this.Controls.Add(this.labelDeviceTypeData);
            this.Controls.Add(this.labelListDeviceTypes);
            this.Controls.Add(this.listBoxDeviceTypes);
            this.Controls.Add(this.buttonUpdateDeviceType);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelDescription);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.labelUpdateDeviceType);
            this.Name = "UCUpdateDeviceType";
            this.Size = new System.Drawing.Size(598, 441);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void buttonUpdateDeviceType_Click(object sender, EventArgs e)
        {
            try
            {
                scada.ModifyDeviceType((DeviceType)listBoxDeviceTypes.SelectedItem, textBoxName.Text, richTextDescription.Text);
                MessageBox.Show("Correctly Updated: Type of Device");
                UpdateVisual();
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

        private void UpdateVisual()
        {
            textBoxName.Text = string.Empty;
            richTextDescription.Text = string.Empty;
            listBoxDeviceTypes.DataSource = null;
            listBoxDeviceTypes.DataSource = scada.DeviceTypes;
            listBoxDeviceTypes.DisplayMember = "Name";
        }
    }
}