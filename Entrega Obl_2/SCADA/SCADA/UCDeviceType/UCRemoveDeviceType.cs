﻿using System.Windows.Forms;
using SCADADomain;
using System.ComponentModel;
using System;
using SCADAFacade;

namespace SCADA
{
    internal class UCRemoveDeviceType : UserControl
    {
        private IndustrialPlant scada;
        private UISCADA uiScadaInstance;
        private Label label1;
        private ListBox listDeviceTypes;
        private Label labelDescription;
        private RichTextBox richTextDescription;
        private Label labelRemoveSelected;
        private Button buttonRemoveDeviceType;
        private BindingList<DeviceType> deviceTypesBinding;

        public UCRemoveDeviceType(IndustrialPlant scada, UISCADA uiscada)
        {
            this.scada = scada;
            this.uiScadaInstance = uiscada;
            InitializeComponent();

            deviceTypesBinding = new BindingList<DeviceType>();
            foreach (DeviceType deviceType in scada.DeviceTypes)
            {
                deviceTypesBinding.Add(deviceType);
            }
            listDeviceTypes.DataSource = deviceTypesBinding;
            listDeviceTypes.DisplayMember = "Name";
        }

        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.listDeviceTypes = new System.Windows.Forms.ListBox();
            this.labelDescription = new System.Windows.Forms.Label();
            this.richTextDescription = new System.Windows.Forms.RichTextBox();
            this.labelRemoveSelected = new System.Windows.Forms.Label();
            this.buttonRemoveDeviceType = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "List of device types in system:";
            // 
            // listDeviceTypes
            // 
            this.listDeviceTypes.FormattingEnabled = true;
            this.listDeviceTypes.Location = new System.Drawing.Point(60, 68);
            this.listDeviceTypes.Name = "listDeviceTypes";
            this.listDeviceTypes.Size = new System.Drawing.Size(144, 251);
            this.listDeviceTypes.TabIndex = 1;
            this.listDeviceTypes.SelectedIndexChanged += new System.EventHandler(this.listDeviceTypes_SelectedIndexChanged);
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Location = new System.Drawing.Point(266, 38);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(63, 13);
            this.labelDescription.TabIndex = 2;
            this.labelDescription.Text = "Description:";
            // 
            // richTextDescription
            // 
            this.richTextDescription.Location = new System.Drawing.Point(269, 68);
            this.richTextDescription.Name = "richTextDescription";
            this.richTextDescription.ReadOnly = true;
            this.richTextDescription.Size = new System.Drawing.Size(264, 81);
            this.richTextDescription.TabIndex = 3;
            this.richTextDescription.Text = "";
            // 
            // labelRemoveSelected
            // 
            this.labelRemoveSelected.AutoSize = true;
            this.labelRemoveSelected.Location = new System.Drawing.Point(266, 201);
            this.labelRemoveSelected.Name = "labelRemoveSelected";
            this.labelRemoveSelected.Size = new System.Drawing.Size(151, 13);
            this.labelRemoveSelected.TabIndex = 4;
            this.labelRemoveSelected.Text = "Remove selected device type:";
            // 
            // buttonRemoveDeviceType
            // 
            this.buttonRemoveDeviceType.Location = new System.Drawing.Point(269, 227);
            this.buttonRemoveDeviceType.Name = "buttonRemoveDeviceType";
            this.buttonRemoveDeviceType.Size = new System.Drawing.Size(75, 23);
            this.buttonRemoveDeviceType.TabIndex = 5;
            this.buttonRemoveDeviceType.Text = "Remove";
            this.buttonRemoveDeviceType.UseVisualStyleBackColor = true;
            this.buttonRemoveDeviceType.Click += new System.EventHandler(this.buttonRemoveDeviceType_Click);
            // 
            // UCRemoveDeviceType
            // 
            this.Controls.Add(this.buttonRemoveDeviceType);
            this.Controls.Add(this.labelRemoveSelected);
            this.Controls.Add(this.richTextDescription);
            this.Controls.Add(this.labelDescription);
            this.Controls.Add(this.listDeviceTypes);
            this.Controls.Add(this.label1);
            this.Name = "UCRemoveDeviceType";
            this.Size = new System.Drawing.Size(575, 481);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void listDeviceTypes_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                DeviceType selected = (DeviceType)listDeviceTypes.SelectedItem;
                richTextDescription.Text = selected.Description;
            }
            catch (Exception)
            {
            }
        }

        private void buttonRemoveDeviceType_Click(object sender, System.EventArgs e)
        {
            try
            {
                DeviceType selected = (DeviceType)listDeviceTypes.SelectedItem;
                scada.EraseDeviceType(selected);
                MessageBox.Show("Correctly Deleted: Device Type");
                UpdateUI(selected);
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

        private void UpdateUI(DeviceType selected)
        {
            deviceTypesBinding.Remove(selected);
            richTextDescription.Text = "";
        }
    }
}