﻿using System.Windows.Forms;
using SCADADomain;
using System;
using SCADAFacade;

namespace SCADA
{
    internal class UCCreateDeviceType : UserControl
    {
        private Label labelName;
        private Label labelDescription;
        private TextBox textBoxName;
        private Button buttonRegisterdeviceType;
        private Label labelCreatDeviceType;
        private RichTextBox richTextDescription;
        private IndustrialPlant scada;

        public UCCreateDeviceType(IndustrialPlant scada)
        {
            this.scada = scada;
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.labelCreatDeviceType = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.labelDescription = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.buttonRegisterdeviceType = new System.Windows.Forms.Button();
            this.richTextDescription = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // labelCreatDeviceType
            // 
            this.labelCreatDeviceType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelCreatDeviceType.AutoSize = true;
            this.labelCreatDeviceType.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelCreatDeviceType.Location = new System.Drawing.Point(56, 43);
            this.labelCreatDeviceType.Name = "labelCreatDeviceType";
            this.labelCreatDeviceType.Size = new System.Drawing.Size(135, 13);
            this.labelCreatDeviceType.TabIndex = 0;
            this.labelCreatDeviceType.Text = "Register New Device Type";
            this.labelCreatDeviceType.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(57, 81);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Name";
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Location = new System.Drawing.Point(57, 119);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(60, 13);
            this.labelDescription.TabIndex = 2;
            this.labelDescription.Text = "Description";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(138, 78);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(100, 20);
            this.textBoxName.TabIndex = 3;
            // 
            // buttonRegisterdeviceType
            // 
            this.buttonRegisterdeviceType.Location = new System.Drawing.Point(152, 206);
            this.buttonRegisterdeviceType.Name = "buttonRegisterdeviceType";
            this.buttonRegisterdeviceType.Size = new System.Drawing.Size(75, 23);
            this.buttonRegisterdeviceType.TabIndex = 5;
            this.buttonRegisterdeviceType.Text = "Register";
            this.buttonRegisterdeviceType.UseVisualStyleBackColor = true;
            this.buttonRegisterdeviceType.Click += new System.EventHandler(this.buttonRegisterdeviceType_Click);
            // 
            // richTextDescription
            // 
            this.richTextDescription.Location = new System.Drawing.Point(138, 116);
            this.richTextDescription.Name = "richTextDescription";
            this.richTextDescription.Size = new System.Drawing.Size(225, 50);
            this.richTextDescription.TabIndex = 6;
            this.richTextDescription.Text = "";
            // 
            // UCCreateDeviceType
            // 
            this.Controls.Add(this.richTextDescription);
            this.Controls.Add(this.buttonRegisterdeviceType);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelDescription);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.labelCreatDeviceType);
            this.Name = "UCCreateDeviceType";
            this.Size = new System.Drawing.Size(520, 381);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void buttonRegisterdeviceType_Click(object sender, System.EventArgs e)
        {
            try
            {
                scada.CreateDeviceType(textBoxName.Text, richTextDescription.Text);
                MessageBox.Show("Correctly Registered: Type of Device");
                EmptyFields();
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

        private void EmptyFields()
        {
            textBoxName.Text = string.Empty;
            richTextDescription.Text = string.Empty;
        }
    }
}