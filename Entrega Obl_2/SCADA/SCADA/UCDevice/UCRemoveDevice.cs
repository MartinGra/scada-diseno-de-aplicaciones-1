﻿using System.Windows.Forms;
using SCADADomain;
using System;
using SCADAFacade;

namespace SCADA
{
    internal class UCRemoveDevice : UserControl
    {
        private IndustrialPlant scada;
        private Label labelListDevices;
        private ListBox listBoxDevices;
        private Label labelDeleteDevice;
        private Button buttonDeleteDevice;
        private Label labelType;
        private Label labelName;
        private Label labelUpdateDeviceTitle;
        private Label labelNameValue;
        private Label labelDeviceTypeValue;
        private UISCADA uiScadaInstance;

        public UCRemoveDevice(IndustrialPlant scada, UISCADA uISCADA)
        {
            this.scada = scada;
            this.uiScadaInstance = uISCADA;
            InitializeComponent();
            this.listBoxDevices.DataSource = scada.Devices;
            this.listBoxDevices.DisplayMember = "Name";
            listBoxDevices_SelectedIndexChanged(null, null);
        }

        private void InitializeComponent()
        {
            this.labelListDevices = new System.Windows.Forms.Label();
            this.listBoxDevices = new System.Windows.Forms.ListBox();
            this.labelDeleteDevice = new System.Windows.Forms.Label();
            this.buttonDeleteDevice = new System.Windows.Forms.Button();
            this.labelType = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.labelUpdateDeviceTitle = new System.Windows.Forms.Label();
            this.labelNameValue = new System.Windows.Forms.Label();
            this.labelDeviceTypeValue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelListDevices
            // 
            this.labelListDevices.AutoSize = true;
            this.labelListDevices.Location = new System.Drawing.Point(27, 76);
            this.labelListDevices.Name = "labelListDevices";
            this.labelListDevices.Size = new System.Drawing.Size(77, 13);
            this.labelListDevices.TabIndex = 25;
            this.labelListDevices.Text = "Select Device:";
            // 
            // listBoxDevices
            // 
            this.listBoxDevices.FormattingEnabled = true;
            this.listBoxDevices.Location = new System.Drawing.Point(30, 92);
            this.listBoxDevices.Name = "listBoxDevices";
            this.listBoxDevices.Size = new System.Drawing.Size(132, 160);
            this.listBoxDevices.TabIndex = 24;
            this.listBoxDevices.SelectedIndexChanged += new System.EventHandler(this.listBoxDevices_SelectedIndexChanged);
            // 
            // labelDeleteDevice
            // 
            this.labelDeleteDevice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelDeleteDevice.AutoSize = true;
            this.labelDeleteDevice.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelDeleteDevice.Location = new System.Drawing.Point(27, 36);
            this.labelDeleteDevice.Name = "labelDeleteDevice";
            this.labelDeleteDevice.Size = new System.Drawing.Size(75, 13);
            this.labelDeleteDevice.TabIndex = 23;
            this.labelDeleteDevice.Text = "Delete Device";
            this.labelDeleteDevice.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // buttonDeleteDevice
            // 
            this.buttonDeleteDevice.Location = new System.Drawing.Point(243, 229);
            this.buttonDeleteDevice.Name = "buttonDeleteDevice";
            this.buttonDeleteDevice.Size = new System.Drawing.Size(111, 23);
            this.buttonDeleteDevice.TabIndex = 22;
            this.buttonDeleteDevice.Text = "Delete Device";
            this.buttonDeleteDevice.UseVisualStyleBackColor = true;
            this.buttonDeleteDevice.Click += new System.EventHandler(this.buttonDeleteDevice_Click);
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.Location = new System.Drawing.Point(223, 150);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(68, 13);
            this.labelType.TabIndex = 19;
            this.labelType.Text = "Device Type";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(223, 112);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 18;
            this.labelName.Text = "Name";
            // 
            // labelUpdateDeviceTitle
            // 
            this.labelUpdateDeviceTitle.AutoSize = true;
            this.labelUpdateDeviceTitle.Location = new System.Drawing.Point(223, 76);
            this.labelUpdateDeviceTitle.Name = "labelUpdateDeviceTitle";
            this.labelUpdateDeviceTitle.Size = new System.Drawing.Size(96, 13);
            this.labelUpdateDeviceTitle.TabIndex = 17;
            this.labelUpdateDeviceTitle.Text = "Device Information";
            // 
            // labelNameValue
            // 
            this.labelNameValue.AutoSize = true;
            this.labelNameValue.Location = new System.Drawing.Point(297, 112);
            this.labelNameValue.Name = "labelNameValue";
            this.labelNameValue.Size = new System.Drawing.Size(35, 13);
            this.labelNameValue.TabIndex = 26;
            this.labelNameValue.Text = "label1";
            // 
            // labelDeviceTypeValue
            // 
            this.labelDeviceTypeValue.AutoSize = true;
            this.labelDeviceTypeValue.Location = new System.Drawing.Point(297, 150);
            this.labelDeviceTypeValue.Name = "labelDeviceTypeValue";
            this.labelDeviceTypeValue.Size = new System.Drawing.Size(35, 13);
            this.labelDeviceTypeValue.TabIndex = 27;
            this.labelDeviceTypeValue.Text = "label2";
            // 
            // UCEraseDevice
            // 
            this.Controls.Add(this.labelDeviceTypeValue);
            this.Controls.Add(this.labelNameValue);
            this.Controls.Add(this.labelListDevices);
            this.Controls.Add(this.listBoxDevices);
            this.Controls.Add(this.labelDeleteDevice);
            this.Controls.Add(this.buttonDeleteDevice);
            this.Controls.Add(this.labelType);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.labelUpdateDeviceTitle);
            this.Name = "UCEraseDevice";
            this.Size = new System.Drawing.Size(468, 331);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void listBoxDevices_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if((Device)listBoxDevices.SelectedItem != null && ((Device)listBoxDevices.SelectedItem).Classification != null)
            {
                this.labelNameValue.Text = ((Device)listBoxDevices.SelectedItem).Name;
                this.labelDeviceTypeValue.Text = ((Device)listBoxDevices.SelectedItem).Classification.Name;
            }
            else
            {
                this.labelNameValue.Text = "";
                this.labelDeviceTypeValue.Text = "";
            }            
        }

        private void buttonDeleteDevice_Click(object sender, System.EventArgs e)
        {
            try
            {
                scada.EraseDevice((Device)listBoxDevices.SelectedItem);
                MessageBox.Show("Correctly Deleted: Device");
                uiScadaInstance.LoadControlPanel();
                this.listBoxDevices.SelectedIndex = 0;
                this.listBoxDevices.DataSource = null;
                this.listBoxDevices.DataSource = scada.Devices;
                this.listBoxDevices.DisplayMember = "Name";
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }
    }
}