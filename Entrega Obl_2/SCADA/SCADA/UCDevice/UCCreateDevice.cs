﻿using System.Windows.Forms;
using SCADADomain;
using System;
using SCADAFacade;

namespace SCADA
{
    internal class UCCreateDevice : UserControl
    {
        private Label labelCreateDeviceTitle;
        private Label labelName;
        private Label labelType;
        private TextBox textBoxName;
        private ComboBox comboBoxDeviceType;
        private Button buttonRegisterDevice;
        private IndustrialPlant scada;

        public UCCreateDevice(IndustrialPlant scada)
        {
            this.scada = scada;
            InitializeComponent();
            comboBoxDeviceType.DataSource = scada.DeviceTypes;
            comboBoxDeviceType.DisplayMember = "Name";       
        }

        private void InitializeComponent()
        {
            this.labelCreateDeviceTitle = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.labelType = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.comboBoxDeviceType = new System.Windows.Forms.ComboBox();
            this.buttonRegisterDevice = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelCreateDeviceTitle
            // 
            this.labelCreateDeviceTitle.AutoSize = true;
            this.labelCreateDeviceTitle.Location = new System.Drawing.Point(47, 37);
            this.labelCreateDeviceTitle.Name = "labelCreateDeviceTitle";
            this.labelCreateDeviceTitle.Size = new System.Drawing.Size(108, 13);
            this.labelCreateDeviceTitle.TabIndex = 0;
            this.labelCreateDeviceTitle.Text = "Register New Device";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(47, 74);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Name";
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.Location = new System.Drawing.Point(47, 116);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(68, 13);
            this.labelType.TabIndex = 2;
            this.labelType.Text = "Device Type";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(132, 71);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(100, 20);
            this.textBoxName.TabIndex = 3;
            // 
            // comboBoxDeviceType
            // 
            this.comboBoxDeviceType.FormattingEnabled = true;
            this.comboBoxDeviceType.Location = new System.Drawing.Point(132, 113);
            this.comboBoxDeviceType.Name = "comboBoxDeviceType";
            this.comboBoxDeviceType.Size = new System.Drawing.Size(121, 21);
            this.comboBoxDeviceType.TabIndex = 4;
            // 
            // buttonRegisterDevice
            // 
            this.buttonRegisterDevice.Location = new System.Drawing.Point(50, 173);
            this.buttonRegisterDevice.Name = "buttonRegisterDevice";
            this.buttonRegisterDevice.Size = new System.Drawing.Size(111, 23);
            this.buttonRegisterDevice.TabIndex = 5;
            this.buttonRegisterDevice.Text = "Register Device";
            this.buttonRegisterDevice.UseVisualStyleBackColor = true;
            this.buttonRegisterDevice.Click += new System.EventHandler(this.buttonRegisterDevice_Click);
            // 
            // UCCreateDevice
            // 
            this.Controls.Add(this.buttonRegisterDevice);
            this.Controls.Add(this.comboBoxDeviceType);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelType);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.labelCreateDeviceTitle);
            this.Name = "UCCreateDevice";
            this.Size = new System.Drawing.Size(587, 443);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void buttonRegisterDevice_Click(object sender, System.EventArgs e)
        {
            try
            {
                scada.CreateDevice(textBoxName.Text, (DeviceType)comboBoxDeviceType.SelectedItem);
                MessageBox.Show("Correctly Registered: Device");
                textBoxName.Text = string.Empty;
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }
    }
}