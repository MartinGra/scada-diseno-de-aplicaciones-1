﻿using System.Windows.Forms;
using SCADADomain;
using System;
using SCADAFacade;

namespace SCADA
{
    internal class UCUpdateDevice : UserControl
    {
        private IndustrialPlant scada;
        private Button buttonUpdateDevice;
        private ComboBox comboBoxDeviceType;
        private TextBox textBoxName;
        private Label labelType;
        private Label labelName;
        private Label labelUpdateDeviceTitle;
        private Label labelListDevices;
        private ListBox listBoxDevices;
        private Label labelUpdateDevice;
        private UISCADA uiScadaInterface;

        public UCUpdateDevice(IndustrialPlant scada, UISCADA uiScadaInterface)
        {
            this.scada = scada;
            this.uiScadaInterface = uiScadaInterface;
            InitializeComponent();
            listBoxDevices.DataSource = scada.Devices;
            listBoxDevices.DisplayMember = "Name";

            try
            {
                this.textBoxName.Text = ((Device)listBoxDevices.SelectedItem).Name;
                this.comboBoxDeviceType.DataSource = scada.DeviceTypes;
                this.comboBoxDeviceType.DisplayMember = "Name";
                this.comboBoxDeviceType.SelectedItem = ((Device)listBoxDevices.SelectedItem).Classification;
                this.comboBoxDeviceType.Enabled = false;
            }
            catch (Exception)
            {
            }        
        }

        private void InitializeComponent()
        {
            this.buttonUpdateDevice = new System.Windows.Forms.Button();
            this.comboBoxDeviceType = new System.Windows.Forms.ComboBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.labelType = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.labelUpdateDeviceTitle = new System.Windows.Forms.Label();
            this.labelListDevices = new System.Windows.Forms.Label();
            this.listBoxDevices = new System.Windows.Forms.ListBox();
            this.labelUpdateDevice = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonUpdateDevice
            // 
            this.buttonUpdateDevice.Location = new System.Drawing.Point(247, 230);
            this.buttonUpdateDevice.Name = "buttonUpdateDevice";
            this.buttonUpdateDevice.Size = new System.Drawing.Size(111, 23);
            this.buttonUpdateDevice.TabIndex = 11;
            this.buttonUpdateDevice.Text = "Update Device";
            this.buttonUpdateDevice.UseVisualStyleBackColor = true;
            this.buttonUpdateDevice.Click += new System.EventHandler(this.buttonUpdateDevice_Click);
            // 
            // comboBoxDeviceType
            // 
            this.comboBoxDeviceType.FormattingEnabled = true;
            this.comboBoxDeviceType.Location = new System.Drawing.Point(302, 151);
            this.comboBoxDeviceType.Name = "comboBoxDeviceType";
            this.comboBoxDeviceType.Size = new System.Drawing.Size(121, 21);
            this.comboBoxDeviceType.TabIndex = 10;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(302, 106);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(100, 20);
            this.textBoxName.TabIndex = 9;
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.Location = new System.Drawing.Point(227, 151);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(68, 13);
            this.labelType.TabIndex = 8;
            this.labelType.Text = "Device Type";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(227, 113);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 7;
            this.labelName.Text = "Name";
            // 
            // labelUpdateDeviceTitle
            // 
            this.labelUpdateDeviceTitle.AutoSize = true;
            this.labelUpdateDeviceTitle.Location = new System.Drawing.Point(227, 77);
            this.labelUpdateDeviceTitle.Name = "labelUpdateDeviceTitle";
            this.labelUpdateDeviceTitle.Size = new System.Drawing.Size(96, 13);
            this.labelUpdateDeviceTitle.TabIndex = 6;
            this.labelUpdateDeviceTitle.Text = "Device Information";
            // 
            // labelListDevices
            // 
            this.labelListDevices.AutoSize = true;
            this.labelListDevices.Location = new System.Drawing.Point(31, 77);
            this.labelListDevices.Name = "labelListDevices";
            this.labelListDevices.Size = new System.Drawing.Size(77, 13);
            this.labelListDevices.TabIndex = 16;
            this.labelListDevices.Text = "Select Device:";
            // 
            // listBoxDevices
            // 
            this.listBoxDevices.FormattingEnabled = true;
            this.listBoxDevices.Location = new System.Drawing.Point(34, 93);
            this.listBoxDevices.Name = "listBoxDevices";
            this.listBoxDevices.Size = new System.Drawing.Size(132, 160);
            this.listBoxDevices.TabIndex = 15;
            this.listBoxDevices.SelectedIndexChanged += new System.EventHandler(this.listBoxDevices_SelectedIndexChanged);
            // 
            // labelUpdateDevice
            // 
            this.labelUpdateDevice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelUpdateDevice.AutoSize = true;
            this.labelUpdateDevice.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelUpdateDevice.Location = new System.Drawing.Point(31, 37);
            this.labelUpdateDevice.Name = "labelUpdateDevice";
            this.labelUpdateDevice.Size = new System.Drawing.Size(79, 13);
            this.labelUpdateDevice.TabIndex = 14;
            this.labelUpdateDevice.Text = "Update Device";
            this.labelUpdateDevice.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // UCUpdateDevice
            // 
            this.Controls.Add(this.labelListDevices);
            this.Controls.Add(this.listBoxDevices);
            this.Controls.Add(this.labelUpdateDevice);
            this.Controls.Add(this.buttonUpdateDevice);
            this.Controls.Add(this.comboBoxDeviceType);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelType);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.labelUpdateDeviceTitle);
            this.Name = "UCUpdateDevice";
            this.Size = new System.Drawing.Size(437, 323);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void listBoxDevices_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                this.textBoxName.Text = ((Device)listBoxDevices.SelectedItem).Name;
                this.comboBoxDeviceType.DataSource = scada.DeviceTypes;
                this.comboBoxDeviceType.DisplayMember = "Name";
                this.comboBoxDeviceType.SelectedItem = ((Device)listBoxDevices.SelectedItem).Classification;
            }
            catch (Exception)
            {
            }
        }

        private void buttonUpdateDevice_Click(object sender, System.EventArgs e)
        {
            try
            {
                scada.ModifyDeviceName((Device)listBoxDevices.SelectedItem, textBoxName.Text);//, comboBoxDeviceType.SelectedItem);
                MessageBox.Show("Correctly Updated: Device");
                UpdateUI();
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

        private void UpdateUI()
        {
            textBoxName.Text = string.Empty;
            listBoxDevices.DataSource = null;
            listBoxDevices.DataSource = scada.Devices;
            listBoxDevices.DisplayMember = "Name";
            uiScadaInterface.LoadControlPanel();
        }
    }
}