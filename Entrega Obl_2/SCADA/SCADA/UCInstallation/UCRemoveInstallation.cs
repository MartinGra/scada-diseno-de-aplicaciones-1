﻿using System.Windows.Forms;
using SCADADomain;
using System;
using System.ComponentModel;
using SCADAFacade;

namespace SCADA
{
    internal class UCRemoveInstallation : UserControl
    {
        private IndustrialPlant scada;
        private Label labelListOfInstallation;
        private ListBox listInstallation;
        private Button buttonRemoveInstallation;
        private Label labelRemoveInstallation;
        private UISCADA uiScadaInstance;
        private Label labelRegisterNewInstallation;
        private BindingList<SCADADomain.Component> installationsBinding;
        public UCRemoveInstallation(IndustrialPlant scada, UISCADA uiscada)
        {
            this.scada = scada;
            this.uiScadaInstance = uiscada;
            InitializeComponent();

            installationsBinding = new BindingList<SCADADomain.Component>();
            foreach(SCADADomain.Component installation in scada.Installations)
            {
                installationsBinding.Add(installation);
            }
            listInstallation.DataSource = installationsBinding;
            listInstallation.DisplayMember = "Name";
        }

        private void InitializeComponent()
        {
            this.labelListOfInstallation = new System.Windows.Forms.Label();
            this.listInstallation = new System.Windows.Forms.ListBox();
            this.buttonRemoveInstallation = new System.Windows.Forms.Button();
            this.labelRemoveInstallation = new System.Windows.Forms.Label();
            this.labelRegisterNewInstallation = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelListOfInstallation
            // 
            this.labelListOfInstallation.AutoSize = true;
            this.labelListOfInstallation.Location = new System.Drawing.Point(39, 45);
            this.labelListOfInstallation.Name = "labelListOfInstallation";
            this.labelListOfInstallation.Size = new System.Drawing.Size(141, 13);
            this.labelListOfInstallation.TabIndex = 0;
            this.labelListOfInstallation.Text = "List of installations in system:";
            // 
            // listInstallation
            // 
            this.listInstallation.FormattingEnabled = true;
            this.listInstallation.Location = new System.Drawing.Point(42, 78);
            this.listInstallation.Name = "listInstallation";
            this.listInstallation.Size = new System.Drawing.Size(150, 355);
            this.listInstallation.TabIndex = 1;
            // 
            // buttonRemoveInstallation
            // 
            this.buttonRemoveInstallation.Location = new System.Drawing.Point(257, 109);
            this.buttonRemoveInstallation.Name = "buttonRemoveInstallation";
            this.buttonRemoveInstallation.Size = new System.Drawing.Size(75, 23);
            this.buttonRemoveInstallation.TabIndex = 2;
            this.buttonRemoveInstallation.Text = "Remove";
            this.buttonRemoveInstallation.UseVisualStyleBackColor = true;
            this.buttonRemoveInstallation.Click += new System.EventHandler(this.buttonRemoveInstallation_Click);
            // 
            // labelRemoveInstallation
            // 
            this.labelRemoveInstallation.AutoSize = true;
            this.labelRemoveInstallation.Location = new System.Drawing.Point(254, 78);
            this.labelRemoveInstallation.Name = "labelRemoveInstallation";
            this.labelRemoveInstallation.Size = new System.Drawing.Size(93, 13);
            this.labelRemoveInstallation.TabIndex = 3;
            this.labelRemoveInstallation.Text = "Remove selected:";
            // 
            // labelRegisterNewInstallation
            // 
            this.labelRegisterNewInstallation.AutoSize = true;
            this.labelRegisterNewInstallation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRegisterNewInstallation.Location = new System.Drawing.Point(155, 17);
            this.labelRegisterNewInstallation.Name = "labelRegisterNewInstallation";
            this.labelRegisterNewInstallation.Size = new System.Drawing.Size(119, 13);
            this.labelRegisterNewInstallation.TabIndex = 4;
            this.labelRegisterNewInstallation.Text = "Remove Installation";
            // 
            // UCRemoveInstallation
            // 
            this.Controls.Add(this.labelRegisterNewInstallation);
            this.Controls.Add(this.labelRemoveInstallation);
            this.Controls.Add(this.buttonRemoveInstallation);
            this.Controls.Add(this.listInstallation);
            this.Controls.Add(this.labelListOfInstallation);
            this.Name = "UCRemoveInstallation";
            this.Size = new System.Drawing.Size(546, 494);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void buttonRemoveInstallation_Click(object sender, System.EventArgs e)
        {
            try
            {
                SCADADomain.Component plantSelected = (SCADADomain.Component)listInstallation.SelectedItem;
                scada.EraseInstallation(plantSelected);
                MessageBox.Show("Correctly Deleted: Installation");
                EmptyFields(plantSelected);
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

        private void EmptyFields(SCADADomain.Component plantSelected)
        {
            installationsBinding.Remove(plantSelected);
            uiScadaInstance.LoadControlPanel();
        }
    }
}