﻿using System.Windows.Forms;
using SCADADomain;
using System;
using System.Collections.ObjectModel;
using SCADAFacade;
using static System.String;

namespace SCADA
{
    internal class UCCreateInstallation : UserControl
    {
        private Label labelRegisterNewInstallation;
        private Label labelName;
        private TextBox textNewName;
        private Button buttonRegister;
        private IndustrialPlant scada;
        private Label labelComponent;
        private Label labelChildToAssign;
        private ListBox listComponentSelection;
        private ComboBox comboComponentType;
        private Label label1;
        private UISCADA uiScadaInstance;

        public UCCreateInstallation(IndustrialPlant scada)
        {
            this.scada = scada;
            InitializeComponent();
            LoadComponentTypeSelector();
            //comboBoxPlants.DataSource = scada.Plants;
            //comboBoxPlants.DisplayMember = "Name";
        }

        private void LoadComponentTypeSelector()
        {
            comboComponentType.DataSource = null;
            Collection <string> posibleComponentsTypes = new Collection<string>();
            posibleComponentsTypes.Add("Installation");
            posibleComponentsTypes.Add("Plant");
            comboComponentType.DataSource = posibleComponentsTypes;
            comboComponentType.DisplayMember = "Name";

        }

        public UCCreateInstallation(IndustrialPlant scada, UISCADA uISCADA) : this(scada)
        {
            this.uiScadaInstance = uISCADA;
        }

        private void InitializeComponent()
        {
            this.labelRegisterNewInstallation = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.textNewName = new System.Windows.Forms.TextBox();
            this.buttonRegister = new System.Windows.Forms.Button();
            this.labelComponent = new System.Windows.Forms.Label();
            this.labelChildToAssign = new System.Windows.Forms.Label();
            this.listComponentSelection = new System.Windows.Forms.ListBox();
            this.comboComponentType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelRegisterNewInstallation
            // 
            this.labelRegisterNewInstallation.AutoSize = true;
            this.labelRegisterNewInstallation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRegisterNewInstallation.Location = new System.Drawing.Point(248, 31);
            this.labelRegisterNewInstallation.Name = "labelRegisterNewInstallation";
            this.labelRegisterNewInstallation.Size = new System.Drawing.Size(149, 13);
            this.labelRegisterNewInstallation.TabIndex = 0;
            this.labelRegisterNewInstallation.Text = "Register New Installation";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(142, 82);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 1;
            this.labelName.Text = "Name";
            // 
            // textNewName
            // 
            this.textNewName.Location = new System.Drawing.Point(145, 102);
            this.textNewName.Name = "textNewName";
            this.textNewName.Size = new System.Drawing.Size(121, 20);
            this.textNewName.TabIndex = 2;
            // 
            // buttonRegister
            // 
            this.buttonRegister.Location = new System.Drawing.Point(268, 451);
            this.buttonRegister.Name = "buttonRegister";
            this.buttonRegister.Size = new System.Drawing.Size(75, 23);
            this.buttonRegister.TabIndex = 3;
            this.buttonRegister.Text = "Register";
            this.buttonRegister.UseVisualStyleBackColor = true;
            this.buttonRegister.Click += new System.EventHandler(this.buttonRegister_Click);
            // 
            // labelComponent
            // 
            this.labelComponent.AutoSize = true;
            this.labelComponent.Location = new System.Drawing.Point(334, 83);
            this.labelComponent.Name = "labelComponent";
            this.labelComponent.Size = new System.Drawing.Size(91, 13);
            this.labelComponent.TabIndex = 9;
            this.labelComponent.Text = "Component Type:";
            // 
            // labelChildToAssign
            // 
            this.labelChildToAssign.AutoSize = true;
            this.labelChildToAssign.Location = new System.Drawing.Point(334, 126);
            this.labelChildToAssign.Name = "labelChildToAssign";
            this.labelChildToAssign.Size = new System.Drawing.Size(37, 13);
            this.labelChildToAssign.TabIndex = 8;
            this.labelChildToAssign.Text = "Select";
            // 
            // listComponentSelection
            // 
            this.listComponentSelection.FormattingEnabled = true;
            this.listComponentSelection.Location = new System.Drawing.Point(334, 145);
            this.listComponentSelection.Name = "listComponentSelection";
            this.listComponentSelection.Size = new System.Drawing.Size(159, 225);
            this.listComponentSelection.TabIndex = 7;
            // 
            // comboComponentType
            // 
            this.comboComponentType.FormattingEnabled = true;
            this.comboComponentType.Location = new System.Drawing.Point(334, 102);
            this.comboComponentType.Name = "comboComponentType";
            this.comboComponentType.Size = new System.Drawing.Size(159, 21);
            this.comboComponentType.TabIndex = 6;
            this.comboComponentType.SelectedIndexChanged += new System.EventHandler(this.comboComponentType_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(337, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Select Father";
            // 
            // UCCreateInstallation
            // 
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelComponent);
            this.Controls.Add(this.labelChildToAssign);
            this.Controls.Add(this.listComponentSelection);
            this.Controls.Add(this.comboComponentType);
            this.Controls.Add(this.buttonRegister);
            this.Controls.Add(this.textNewName);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.labelRegisterNewInstallation);
            this.Name = "UCCreateInstallation";
            this.Size = new System.Drawing.Size(667, 504);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        

        private void buttonRegister_Click(object sender, System.EventArgs e)
        {
            try
            {
                Component selectedComponent = (Component)listComponentSelection.SelectedItem;
                string installationName = textNewName.Text;
                scada.CreateInstallation(installationName, selectedComponent);
                uiScadaInstance.LoadControlPanel();
                MessageBox.Show("Correctly Registered: Installation");
                LoadComponentTypeSelector();
                EmptyFields();
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }
        }

        private void EmptyFields()
        {
            textNewName.Text = Empty;
        }

        private void comboComponentType_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            string selected = (string)comboComponentType.SelectedItem;
            if (selected == null)
                listComponentSelection.DataSource = null;
            else
            {
                if (selected.Equals("Installation"))
                    listComponentSelection.DataSource = scada.Installations;
                if (selected.Equals("Plant"))
                    listComponentSelection.DataSource = scada.Plants;
            }
            

            listComponentSelection.DisplayMember = "Name";
        }
    }
}