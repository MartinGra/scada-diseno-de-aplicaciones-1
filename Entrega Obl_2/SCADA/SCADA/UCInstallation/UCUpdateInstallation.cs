﻿using System.Windows.Forms;
using SCADADomain;
using System;
using SCADAFacade;

namespace SCADA
{
    internal class UCUpdateInstallation : UserControl
    {
        private IndustrialPlant scada;
        private Button buttonUpdate;
        private TextBox textNewName;
        private Label labelName;
        private Label labelUpdateInstallationData;
        private ListBox listBoxInstallations;
        private Label labelSelectInstallation;
        private Label labelUpdateInstallation;
        private UISCADA uiScadaInterface;

        public UCUpdateInstallation(IndustrialPlant scada, UISCADA uISCADA)
        {
            this.scada = scada;
            this.uiScadaInterface = uISCADA;
            InitializeComponent();
            listBoxInstallations.DataSource = scada.Installations;
            listBoxInstallations.DisplayMember = "Name";
            listBox1_SelectedIndexChanged(null,null);
        }

        private void InitializeComponent()
        {
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.textNewName = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.labelUpdateInstallationData = new System.Windows.Forms.Label();
            this.listBoxInstallations = new System.Windows.Forms.ListBox();
            this.labelSelectInstallation = new System.Windows.Forms.Label();
            this.labelUpdateInstallation = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(262, 168);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdate.TabIndex = 7;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // textNewName
            // 
            this.textNewName.Location = new System.Drawing.Point(256, 124);
            this.textNewName.Name = "textNewName";
            this.textNewName.Size = new System.Drawing.Size(100, 20);
            this.textNewName.TabIndex = 6;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(201, 124);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 5;
            this.labelName.Text = "Name";
            // 
            // labelUpdateInstallationData
            // 
            this.labelUpdateInstallationData.AutoSize = true;
            this.labelUpdateInstallationData.Location = new System.Drawing.Point(253, 71);
            this.labelUpdateInstallationData.Name = "labelUpdateInstallationData";
            this.labelUpdateInstallationData.Size = new System.Drawing.Size(84, 13);
            this.labelUpdateInstallationData.TabIndex = 4;
            this.labelUpdateInstallationData.Text = "Installation data:";
            // 
            // listBoxInstallations
            // 
            this.listBoxInstallations.FormattingEnabled = true;
            this.listBoxInstallations.Location = new System.Drawing.Point(32, 96);
            this.listBoxInstallations.Name = "listBoxInstallations";
            this.listBoxInstallations.Size = new System.Drawing.Size(120, 95);
            this.listBoxInstallations.TabIndex = 8;
            this.listBoxInstallations.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // labelSelectInstallation
            // 
            this.labelSelectInstallation.AutoSize = true;
            this.labelSelectInstallation.Location = new System.Drawing.Point(34, 71);
            this.labelSelectInstallation.Name = "labelSelectInstallation";
            this.labelSelectInstallation.Size = new System.Drawing.Size(90, 13);
            this.labelSelectInstallation.TabIndex = 9;
            this.labelSelectInstallation.Text = "Select Installation";
            // 
            // labelUpdateInstallation
            // 
            this.labelUpdateInstallation.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelUpdateInstallation.AutoSize = true;
            this.labelUpdateInstallation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUpdateInstallation.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelUpdateInstallation.Location = new System.Drawing.Point(122, 25);
            this.labelUpdateInstallation.Name = "labelUpdateInstallation";
            this.labelUpdateInstallation.Size = new System.Drawing.Size(114, 13);
            this.labelUpdateInstallation.TabIndex = 15;
            this.labelUpdateInstallation.Text = "Update Installation";
            this.labelUpdateInstallation.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // UCUpdateInstallation
            // 
            this.Controls.Add(this.labelUpdateInstallation);
            this.Controls.Add(this.labelSelectInstallation);
            this.Controls.Add(this.listBoxInstallations);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.textNewName);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.labelUpdateInstallationData);
            this.Name = "UCUpdateInstallation";
            this.Size = new System.Drawing.Size(394, 334);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void listBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                this.textNewName.Text = ((Installation)this.listBoxInstallations.SelectedItem).Name;
            }
            catch (Exception)
            {
            }
        }

        private void buttonUpdate_Click(object sender, System.EventArgs e)
        {
            try
            {
                scada.ModifyInstallationName((Installation)listBoxInstallations.SelectedItem, textNewName.Text);
                MessageBox.Show("Correctly Updated: Installation");
                EmptyFields();
            }
            catch (Exception excep)
            {
                MessageBox.Show(excep.Message);
            }    
        }

        private void EmptyFields()
        {
            listBoxInstallations.DataSource = null;
            listBoxInstallations.DataSource = scada.Installations;
            listBoxInstallations.DisplayMember = "Name";
            uiScadaInterface.LoadControlPanel();
        }
    }
}