﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using SCADAException;
using System.ComponentModel.DataAnnotations;

namespace SCADADomain
{
    public class Component
    {
        public int ComponentId { get; set; }
        public string Name { get; set; }
        public virtual ICollection<ControlVariable> ControlVariables { get; set; }
        public virtual ICollection<Incident> Incidents { get; set; }
        public Component Father { get; set; }
        public ICollection<int> AcceptedLevels { get; set; }
      
        public int HierarchyLevel { get; set; }

        public bool IsAcceptedChild(Component child)
        {
            bool ret = false;
            foreach (int level in AcceptedLevels)
                if (child.HierarchyLevel == level) ret = true;
            return ret;
        }

        public void AddControlVariable(ControlVariable controlVariableToStock)
        {
            if (ControlVariables == null)
                throw new Exception("This components doesn't allow control variables");
            ControlVariables.Add(controlVariableToStock);
        }

        public bool IsInUpperLevel()
        {
            return HierarchyLevel.Equals(Globals.UPPER_LEVEL);
        }

        public int CurrentAlarms()
        {
            int currAlarms = 0;

            foreach (ControlVariable variable in this.ControlVariables)
                if (variable.HasLastOutOfRange()) currAlarms++;

            return currAlarms;
        }

        public int CurrentWarnings()
        {
            int warnings = 0;

            foreach (ControlVariable variable in this.ControlVariables)
                if (variable.HasLastValueInWarningRange()) warnings++;

            return warnings;            
        }

        public void ModifyNameOfControlVariable(ControlVariable selectedVariable, string newVariableName)
        {            
            if (!selectedVariable.Name.Equals(newVariableName))
            {
                if (!Globals.EmptyString(newVariableName) && ControlVariableIsUnique(newVariableName))
                    selectedVariable.Name = newVariableName;
                else
                    ErrorModifyingControlVariableName(newVariableName);
            }            
        }

        private void ErrorModifyingControlVariableName(string newVariableName)
        {
            if (Globals.EmptyString(newVariableName)) 
                throw new ExceptionEmptyString("Error: cannot modify empty name");
            //if (!ControlVariableIsUnique(newVariableName))
                throw new ExceptionExistingControlVariableName("Error: already exists variable name");
        }

        private bool ControlVariableIsUnique(string newVariableName)
        {
            bool isUnique = true;
            foreach(ControlVariable variable in ControlVariables)
                if (variable.Name.Equals(newVariableName))
                    isUnique = false;
            return isUnique;
        }
        public void ModifyRangesOfControlVariable(ControlVariable controlVariable, float newMinRange, float newMaxRange)
        {
            controlVariable.ModifyRanges(newMinRange, newMaxRange);
        }

        public bool HasFather()
        {
            return Father != null;
        }

        public bool IsInLowerLevel()
        {
            return HierarchyLevel.Equals(Globals.LOWER_LEVEL);
        }
        public override bool Equals(object obj)
        {
            var comp = obj as Component;
            if (comp == null)
            {
                return false;
            }
            return ComponentId == comp.ComponentId;
        }

        public void ModifyWarningRangesOfControlVariable(ControlVariable variable, float minWarning, float maxWarning)
        {
            variable.ModifyWarningRanges(minWarning, maxWarning);
        }

        public ICollection<AssignValue> HistoricValues(ControlVariable variable)
        {
            return variable.ValueDatePair;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}