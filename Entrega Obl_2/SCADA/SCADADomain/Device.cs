﻿using SCADAException;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SCADADomain
{ 
    public class Device : Component
    {
        public virtual DeviceType Classification { get; set; }
        
        public Device()
        {
            AcceptedLevels = new List<int>();
         //   Hierarchy = new Collection<Component>();
            HierarchyLevel = Globals.LOWER_LEVEL;
        }

        public Device(string newName, DeviceType selectedType)
        {
            if (!ValidDeviceParameters(newName, selectedType))
            {
                if (Globals.EmptyString(newName))
                    throw new ExceptionEmptyString("Error: name cannot be empty");
            }
            Father = null;
            Name = newName;
            ControlVariables = new Collection<ControlVariable>();
            Classification = selectedType;
            AcceptedLevels = new List<int>();
            //Hierarchy = new Collection<Component>();
            HierarchyLevel = Globals.LOWER_LEVEL;
        }

        private static bool ValidDeviceParameters(string newDeviceName, DeviceType selectedType)
        {
            return !Globals.EmptyString(newDeviceName) && selectedType != null;
        }

    }
}