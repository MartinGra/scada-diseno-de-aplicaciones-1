﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCADADomain
{
    public class Plant : Component
    {
        public string Address { get; set; }
        public string City { get; set; }

        public Plant()
        {
            Father = null;
            AcceptedLevels = new List<int>();
            AcceptedLevels.Add(Globals.LOWER_LEVEL);
            AcceptedLevels.Add(Globals.MIDDLE_LEVEL);
            AcceptedLevels.Add(Globals.UPPER_LEVEL);
            HierarchyLevel = Globals.UPPER_LEVEL;
        }
        public Plant(string newName, string newCity, string newAddress)
        {
            Father = null;
            Name = newName;
            Address = newAddress;
            City = newCity;
            AcceptedLevels = new List<int>();
            AcceptedLevels.Add(Globals.LOWER_LEVEL);
            AcceptedLevels.Add(Globals.MIDDLE_LEVEL);
            AcceptedLevels.Add(Globals.UPPER_LEVEL);
            HierarchyLevel = Globals.UPPER_LEVEL;
        }
    }
}
