﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Runtime.CompilerServices;

namespace SCADADomain
{
    public class Context : DbContext
    {
        public Context () : base("name=Context")
        {
        }

        public DbSet<DeviceType> DeviceTypes { get; set; }
        public DbSet<Component> Components { get; set; }
        public DbSet<ControlVariable> ControlVariables { get; set; }
        public DbSet<Incident> Incidents { get; set; }
        public DbSet<AssignValue> AssignValues { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Device>().ToTable("Devices");
            modelBuilder.Entity<Installation>().ToTable("Installations");
            modelBuilder.Entity<Plant>().ToTable("Plants");
            modelBuilder.Entity<Incident>().ToTable("Incidents");
            modelBuilder.Entity<AssignValue>().ToTable("AssingValues");
            base.OnModelCreating(modelBuilder);
        }
    }
}
