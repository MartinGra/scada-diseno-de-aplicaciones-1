﻿using System;

namespace SCADADomain
{
    public class AssignValue
    {
        public int AssignValueId { get; set; }
        public float Item1 { get; set; }
        public DateTime Item2 { get; set; }
        public AssignValue()
        {
            Item2 = DateTime.Now;
        }

        public AssignValue(float val, DateTime dateTime)
        {
            Item1 = val;
            Item2 = dateTime;
        }

    }
}