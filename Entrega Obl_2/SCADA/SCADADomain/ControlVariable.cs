﻿using SCADAException;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace SCADADomain
{
    public class ControlVariable
    {
        public int ControlVariableId{ get; set; }
        public float MaxRange { get; set; }
        public float MinRange { get; set; }
        public string Name { get; set; }
        public ICollection<AssignValue> ValueDatePair { get; set; }
        public float MinRangeWarning { get; set; }
        public float MaxRangeWarning { get; set; }

        public ControlVariable()
        {
            ValueDatePair = new Collection<AssignValue>();
        }

        public ControlVariable(string name, float minVaribleValue, float maxVariableValue)
        {
            if (!ValidControlVariableParameters(name, minVaribleValue, maxVariableValue))
            {
                ControlVariableCreationError(name, minVaribleValue, maxVariableValue);
            }
            Name = name;
            MinRange = minVaribleValue;
            MaxRange = maxVariableValue;
            MinRangeWarning = minVaribleValue; 
            MaxRangeWarning = maxVariableValue; 
            ValueDatePair = new Collection<AssignValue>();
        }

        public ControlVariable(string name, float minRange, float maxRange, float minWarning, float maxWarning) : this(name, minRange, maxRange)
        {
            if (ValidFloatRanges(minWarning,maxWarning) && minWarning != maxWarning && ConsistentWarningAndAlarmRanges(minRange,maxRange,minWarning, maxWarning))
            {
                MinRangeWarning = minWarning;
                MaxRangeWarning = maxWarning;
            }
            else
                ErrorWithWarningAndAlarmRanges(minRange,maxRange,minWarning,maxWarning);
        }

        private static bool ValidFloatRanges(float minRange,float maxRange)
        {
            return Globals.ValidControlFloatRange(minRange) && Globals.ValidControlFloatRange(maxRange); 
        }

        private static void ErrorWithWarningAndAlarmRanges(float minRange, float maxRange, float minWarning,float maxWarning)
        {
            if (!ValidFloatRanges(minWarning, maxWarning))
                throw new ExceptionInvalidFloatValue(Globals.MessageInvalidFloat);
            if (!ConsistentWarningAndAlarmRanges(minRange,maxRange,minWarning,maxWarning))
                throw new ExceptionInconsistentMinMaxValue(Globals.MessageInconsistentRanges);
            if (minWarning == maxWarning)
                throw new ExceptionInconsistentMinMaxValue("Error: warning ranges cant be the equals");     
        }

        public bool HasLastValueInWarningRange()
        {
            return IsAllowedToHaveWarnings() && ValueInWarningRange(GetLastValue().Item1);
        }
    
        private bool ValueInWarningRange(float variableValue)
        {
            return ValueInMaxWarningRange(variableValue) || ValueInMinWarningRange(variableValue);
        }

        private bool ValueInMinWarningRange(float variableValue)
        {
            return MaxRangeWarning < variableValue && variableValue <= MaxRange;
        }

        private bool ValueInMaxWarningRange(float variableValue)
        {
            return MinRange <= variableValue && MinRangeWarning > variableValue;
        }

        private bool IsAllowedToHaveWarnings()
        {
            return MinRange != MinRangeWarning && MaxRange != MaxRangeWarning;
        }

        private static bool ConsistentWarningAndAlarmRanges(float minRange, float maxRange, float minWarning, float maxWarning)
        {
            return Globals.ConsistentMinMaxValues(minWarning, maxWarning)
                && Globals.ConsistentMinMaxValues(minRange, minWarning)
                && Globals.ConsistentMinMaxValues(maxWarning, maxRange);
        }


        private static void ControlVariableCreationError(string name, float minVaribleValue, float maxVariableValue)
        {
            if (!Globals.ConsistentMinMaxValues(minVaribleValue, maxVariableValue))
                throw new ExceptionInconsistentMinMaxValue(Globals.MessageInconsistentRanges);

            if (!ValidFloatRanges(minVaribleValue,maxVariableValue))
                throw new ExceptionInvalidFloatValue(Globals.MessageInvalidFloat);

            if (!ValidControlVariableName(name))
                throw new ExceptionEmptyString("Error: name cannot be empty");
        }

        internal void ModifyRanges(float newMinRange, float newMaxRange)
        {
            if (ValidRanges(newMinRange, newMaxRange))
            {
                MinRange = newMinRange;
                MaxRange = newMaxRange;
            }
            else
                ErrorRanges(newMinRange, newMaxRange);
        }

        private static bool ValidRanges(float newMinRange, float newMaxRange)
        {
            return ValidFloatRanges(newMinRange, newMaxRange) &&
                            Globals.ConsistentMinMaxValues(newMinRange, newMaxRange);
        }

        private static void ErrorRanges(float newMinRange, float newMaxRange)
        {
            if (!ValidFloatRanges(newMinRange, newMaxRange))
                throw new ExceptionInvalidFloatValue(Globals.MessageInvalidFloat);

            if (!Globals.ConsistentMinMaxValues(newMinRange, newMaxRange))
                throw new ExceptionInconsistentMinMaxValue(Globals.MessageInconsistentRanges);
        }

        internal bool ValueOnRange(float newValueOfVariable)
        {
            return MaxRange >= newValueOfVariable && MinRange <= newValueOfVariable;
        }

        public bool HasLastOutOfRange()
        {
            return ValueDatePair.Count != 0 && !ValueOnRange(GetLastValue().Item1);
        }

        internal void ModifyWarningRanges(float minWarning, float maxWarning)
        {
            if (ValidFloatRanges(minWarning, maxWarning) && minWarning != maxWarning && ConsistentWarningAndAlarmRanges(MinRange, MaxRange, minWarning, maxWarning)){
                MinRangeWarning = minWarning;
                MaxRangeWarning = maxWarning;   
            }
            else
                ErrorWithWarningAndAlarmRanges(MinRange, MaxRange, minWarning, maxWarning);
        }

        private bool ValidControlVariableParameters(string nameOfVariable, float minVariableValue, float maxVariableValue)
        {
            return ValidControlVariableName(nameOfVariable) && ValidRanges(minVariableValue, maxVariableValue);
        }

        private static bool ValidControlVariableName(string nameOfVariable)
        {
            return !Globals.EmptyString(nameOfVariable);
        }

        public override bool Equals(object obj)
        {
            var controlVariable = obj as ControlVariable;
            if (controlVariable == null)
            {
                return false;
            }
            return SameControlVariable(controlVariable);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        private bool SameControlVariable(ControlVariable controlVariable)
        {
            return controlVariable.Name.Equals(Name);
        }

        public AssignValue GetLastValue()
        {
            return ValueDatePair.Last();
        }

        public void UpdateValue(float value, DateTime dateTime)
        {
            AssignValue valueDateToInsert = new AssignValue(value, dateTime);
            ValueDatePair.Add(valueDateToInsert);
        }
     
    }

}