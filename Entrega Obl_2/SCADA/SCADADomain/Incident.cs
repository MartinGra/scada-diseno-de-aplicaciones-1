﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCADAException;

namespace SCADADomain
{

    public class Incident
    {
        public int IncidentId { get; set; }
        public DateTime OccurrenceDate { get; set; }
        public int Severity { get; set; }
        public string Description { get; set; }

        public Incident()
        {
            Severity = 0;
            Description = null;
            OccurrenceDate = DateTime.Now;
        }

        public Incident(string severity, string description)
        {
            ControlIncidentParameters(severity,description);

            int severityInt;
            int.TryParse(severity, out severityInt);

            Severity = severityInt;
            Description = description;
            OccurrenceDate = DateTime.Now;
        }

        private static void ControlIncidentParameters(string severity, string description)
        {
            if (description == "")
                throw new ExceptionEmptyString("Description cannot be empty");
            int res;
            bool isNumeric = int.TryParse(severity, out res);
            if (!isNumeric)
                throw new FormatException("Severity must be a number");
        }
    }
}
