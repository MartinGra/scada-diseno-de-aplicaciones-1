﻿using SCADAException;

namespace SCADADomain
{
    public class DeviceType
    {
        public int DeviceTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public DeviceType()
        {
        }

        public DeviceType(string name, string description)
        {
            if(!ValidDeviceTypeParameters(name))
                throw new ExceptionEmptyString("Ërror: name cannot be empty");
            Name = name;
            Description = description;
        }

        public override bool Equals(object obj)
        {
            var item = obj as DeviceType;
            if (item == null)
            {
                return false;
            }
            return SameName(item);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        private bool SameName(DeviceType item)
        {
            return Name.Equals(item.Name);
        }
        public static bool ValidDeviceTypeParameters(string name)
        {
            return !Globals.EmptyString(name);
        }
    }
}