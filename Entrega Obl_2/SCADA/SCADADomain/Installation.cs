﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using SCADAException;
using System;
using System.Collections.Generic;

namespace SCADADomain
{
    public class Installation : Component
    {
     
        public Installation()
        {
            Father = null;
            ControlVariables = new Collection<ControlVariable>();
            AcceptedLevels = new List<int>();
            AcceptedLevels.Add(Globals.LOWER_LEVEL);
            AcceptedLevels.Add(Globals.MIDDLE_LEVEL);
            HierarchyLevel = Globals.MIDDLE_LEVEL;
        }
        public Installation(string newName, Component newFather)
        {
            Father = newFather;
            Name = newName;
            ControlVariables = new Collection<ControlVariable>();
            AcceptedLevels = new List<int>();
            AcceptedLevels.Add(Globals.LOWER_LEVEL);
            AcceptedLevels.Add(Globals.MIDDLE_LEVEL);
            HierarchyLevel = Globals.MIDDLE_LEVEL;

            ParametersValidation(newName, newFather);
        }

        private void ParametersValidation(string newName, Component newFather)
        {
            if (!ValidInstallationName(newName))
                throw new ExceptionEmptyString("Error: name of the Installation cannot be empty");            
            if (newFather == null)
                throw new NullReferenceException("Error: Father component not selected");
            if (!newFather.IsAcceptedChild(this))
                throw new Exception("Invalid father of component");
        }

        public static bool ValidInstallationName(string newInstallationName)
        {
            return !Globals.EmptyString(newInstallationName);
        }
    }
}
