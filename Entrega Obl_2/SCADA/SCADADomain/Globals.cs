﻿using System;
using System.Globalization;
using System.IO;

namespace SCADADomain
{
    public static class Globals
    {
        public const float MAX_FLOAT_VALUE = 10000000;
        public static int UPPER_LEVEL = 0;
        public static int MIDDLE_LEVEL = 1;
        public static int LOWER_LEVEL = 2;
        private static readonly double MaxFloatValueStr = Double.Parse(MAX_FLOAT_VALUE.ToString(CultureInfo.InvariantCulture), NumberStyles.Float);
        public static string MessageInvalidFloat = "Error: values must be in (-" + MaxFloatValueStr + "," + MaxFloatValueStr + ") Range";
        public static string MessageInconsistentRanges = "Error: inconsistent minimum and maximum values";        

        public static string IncidentsPlainTextPath()
        {
            string projectDir = Directory.GetParent(Directory.GetCurrentDirectory()).FullName;
            return Path.Combine(projectDir, "Incidents");           
        }
        
        public static bool ValidControlFloatRange(float parameter)
        {
            return parameter > - MAX_FLOAT_VALUE && parameter < MAX_FLOAT_VALUE;
        }

        public static bool ConsistentMinMaxValues(float minVariableValue, float maxVariableValue)
        {
            return minVariableValue <= maxVariableValue;
        }

        public static bool EmptyString(string parameter)
        {
            return (parameter.Trim().Length == 0);
        }

        public static bool ValidLineFromTxt(string line, Component comp)
        {
            bool valid = false;
            string[] incident = line.Split('_');
            int incidentParameters = 4;
            if(incident.Length == incidentParameters)
            {
                if (incident[incident.Length - 1].Equals(comp.ComponentId.ToString()))
                    valid = true;
            }
            return valid;
        }
    }
}
