﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCADADomain;
using SCADAPersistence.Domain_Handlers;
using SCADAPersistence.Incident_Managers;

namespace SCADAPersistence.Domain_InterfaceHandlers
{
    public interface IIncidentHandler
    {
        void CreateIncident(string severity, string description, Component comp, IncidentManager storage);
        List<string> GetReports(Component comp, IncidentManager storage);
    }
}
