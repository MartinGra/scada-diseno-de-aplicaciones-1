﻿using System.Collections.Generic;
using System.Linq;
using SCADADomain;
using SCADAException;
using SCADAPersistence.Domain_Handlers;

namespace SCADAPersistence.Domain_InterfaceHandlers
{
    public interface IDeviceTypeHandler
    {
        ICollection<DeviceType> GetDeviceTypes();
        void CreateDeviceType(string name, string description);
        void ModifyDeviceType(DeviceType deviceType, string newName, string newDescription);
        void EraseDeviceType(DeviceType deviceType);
    }
}