﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using SCADADomain;
using SCADAException;
using SCADAPersistence.Domain_Handlers;

namespace SCADAPersistence.Domain_InterfaceHandlers
{
    public interface IComponentHandler
    {
        ICollection<Component> GetPlants();
        ICollection<Component> GetComponents();
        ICollection<Component> GetDevices();
        ICollection<Component> GetInstallations();
        ICollection<Component> Hierarchy(Component comp);
        void AssignChildToFatherHierarchy(Component child, Component father);
        void CreatePlant(string name, string city, string address);
        void ErasePlant(Component component);
        void CreateInstallation(string newInstallationName, Component selectedComponent);
        void ModifyInstallationName(Component installationToModify, string newInstallationName);
        void EraseInstallation(Component component);
        void CreateDevice(string newDeviceName, DeviceType selectedType);
        void ModifyDeviceName(Component deviceToModify, string newDeviceName);
        void EraseDevice(Component component);
        void ModifyPlant(Component component, string name);
        int GetTotalAlarms(Component comp);
        ICollection<Component> FirstChilds(Component comp);
        ICollection<AssignValue> HistoricValues(Component component, ControlVariable variable);
        int GetTotalWarnings(Component comp);
    }
}