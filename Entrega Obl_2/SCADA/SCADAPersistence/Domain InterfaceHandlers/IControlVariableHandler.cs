﻿using System;
using System.Linq;
using SCADADomain;
using SCADAException;
using SCADAPersistence.Domain_Handlers;

namespace SCADAPersistence.Domain_InterfaceHandlers
{
    public interface IControlVariableHandler
    {
        ControlVariable CreateControlVariable(string nameOfVariable, float minVariableValue, float maxVariableValue);

        void ControlVariableAssignValue(ControlVariable selectedControlVariable, float newValueOfVariable);

        void CreateControlVariableInComponent(Component component, string nameVariable, float minValueOfVariable,
            float maxValueOfVariable);

        void ControlVariableAssignValueInComponent(Component component, ControlVariable controlVariable,
            float newControlVariableValue);

        void EraseControlVariable(Component component, ControlVariable controlVariable);

        bool ControlVariableHasValuesAndLastOutOfRange(ControlVariable controlVariable);

        void ErrorErasingControlVariable(Component component, ControlVariable controlVariable);

        bool ValidControlVariable(ControlVariable controlVariable);

        void ModifyControlVariable(Component component, ControlVariable variable, string name, float minRange,
            float maxRange);

        void CreateControlVariableWithWarningRanges(Component comp, string name, float minAlarm, float maxAlarm,
            float minWarning, float maxWarning);

        void ErrorCreatingControlVariableWithWarningRanges(Component comp);

        bool ValidComponentForControlVariable(Component comp);
        void ModifyControlVariableWithWarning(Component comp, ControlVariable variable, string name, float minAlarm, float maxAlarm,
            float minWarning, float maxWarning);
    }
}