﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using SCADADomain;

namespace SCADAPersistence.Incident_Managers
{
    public class IncidentPlainTextImp : IncidentManager
    { 

        public void AddIncident(Incident inc, Component comp)
        {  
            FileStream fs = new FileStream(Globals.IncidentsPlainTextPath(), FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.Close();
            StreamWriter sw = new StreamWriter(fs.Name, true, Encoding.ASCII);
            string NextLine = inc.Description + "_" + inc.Severity + "_" + inc.OccurrenceDate + "_" + comp.ComponentId;
            sw.WriteLine(NextLine);
            sw.Close();            
        }

        public List<string> ObtainReports(Component comp)
        {
            List<string> reports = new List<string>();
            using (StreamReader sr = File.OpenText(Globals.IncidentsPlainTextPath()))
            {
                string s = String.Empty;
                while ((s = sr.ReadLine()) != null)
                {
                    if (Globals.ValidLineFromTxt(s, comp))
                        reports.Add(s);
                }
            }
            return reports;
        }
    }
    
}
