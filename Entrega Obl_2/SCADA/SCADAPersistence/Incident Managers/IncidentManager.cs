﻿using System.Collections.Generic;
using SCADADomain;

namespace SCADAPersistence.Incident_Managers
{
    public interface IncidentManager
    {
        void AddIncident(Incident inc, Component comp);
        List<string> ObtainReports(Component comp);
    }
}

