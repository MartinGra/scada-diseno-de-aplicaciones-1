﻿using System.Collections.Generic;
using SCADADomain;

namespace SCADAPersistence.Incident_Managers
{
    public class IncidentDataBaseImp : IncidentManager
    {
        public void AddIncident(Incident inc, Component comp)
        {
            using (var db = new Context())
            {
                db.Components.Attach(comp);
                comp.Incidents.Add(inc);
                db.SaveChanges();
            }
        }

        public List<string> ObtainReports(Component comp)
        {
            List<string> reports = new List<string>();
            using (var db = new Context())
            {
                db.Components.Attach(comp);
                foreach (Incident inc in comp.Incidents)
                {
                    string report = inc.Description + "_" + inc.Severity + "_" + inc.OccurrenceDate + "_" + comp.ComponentId;
                    reports.Add(report);
                }
            }
            return reports;
        }    
    }
}
