﻿using System;
using System.Data.Entity;
using System.Linq;
using SCADADomain;
using SCADAException;

namespace SCADAPersistence.Domain_Handlers
{
    public class DeviceHandler
    {
        public static void CreateDevice(string newDeviceName, DeviceType selectedType)
        {
            if (selectedType == null)
                throw new NullReferenceException("Error: device type must be selected");

            using (var db = new Context())
            {
                db.DeviceTypes.Attach(selectedType);
                Device newDevice = new Device(newDeviceName, selectedType);
                db.Components.Add(newDevice);
                db.SaveChanges();
            }
        }
        public static void ModifyDeviceName(Component deviceToModify, string newDeviceName)
        {
            if (!Globals.EmptyString(newDeviceName) && ComponentHandler.ValidComponent(deviceToModify))
            {
                using (var db = new Context())
                {
                    db.Components.Attach(deviceToModify);
                    deviceToModify.Name = newDeviceName;
                    db.SaveChanges();
                }
            }
            else
                DeviceModifyError(deviceToModify, newDeviceName);
        }

        private static void DeviceModifyError(Component deviceToModify, string newDeviceName)
        {
            if (Globals.EmptyString(newDeviceName))
                throw new ExceptionEmptyString("Error: name cannot be empty");
            if (!ComponentHandler.ValidComponent(deviceToModify))
                throw new NullReferenceException("Error: device not selected");
        }

        public static void EraseDevice(Component component)
        {
            if (ComponentHandler.ValidComponent(component))
                DeleteDevice(component);
            else
                ErrorErasingDevice(component);
        }

        private static void DeleteDevice(Component component)
        {
            using (var db = new Context())
            {
                Component comp = db.Components.Where(c => c.ComponentId == component.ComponentId).Include("Incidents").Include("ControlVariables").Include("Classification").OfType<Device>().Cast<Component>().FirstOrDefault();
                db.Components.Attach(comp);
                //TO DO: SEE DIFF BETWEEN REMOVEFROMHIERARCHY AND DELETECOMP..FROMFATEHRHIERARCHY
                ComponentHandler.RemoveFromHierarchy(component);
                comp.ControlVariables.Clear();
                comp.Incidents.Clear();
                db.Components.Remove(comp);
                db.SaveChanges();
            }
        }
        private static void ErrorErasingDevice(Component component)
        {
            if (component == null)
                throw new NullReferenceException("Error: device not selected");
        }
    }
}
