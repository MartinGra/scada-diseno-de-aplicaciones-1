﻿using System;
using System.Collections.Generic;
using System.Linq;
using SCADADomain;
using SCADAException;
using SCADAPersistence.Domain_InterfaceHandlers;

namespace SCADAPersistence.Domain_Handlers
{
    public class DeviceTypeHandler : IDeviceTypeHandler
    {
        private static DeviceTypeHandler instance;

        private DeviceTypeHandler() { }

        public static IDeviceTypeHandler ObtainInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DeviceTypeHandler();
                }
                return instance;
            }
        }

        public ICollection<DeviceType> GetDeviceTypes()
        {
            using (var db = new Context())
                return db.DeviceTypes.ToList();
        }

        public void CreateDeviceType(string name, string description)
        {
            using (var dbContext = new Context())
            {
                DeviceType newDeviceType = new DeviceType(name, description);
                dbContext.DeviceTypes.Add(newDeviceType);
                dbContext.SaveChanges();
            }
        }

        public void ModifyDeviceType(DeviceType deviceType, string newName, string newDescription)
        {
            if (DeviceType.ValidDeviceTypeParameters(newName) && ValidDeviceType(deviceType))
            {
                using (var db = new Context())
                {
                    db.DeviceTypes.Attach(deviceType);
                    deviceType.Name = newName;
                    deviceType.Description = newDescription;
                    db.SaveChanges();
                }
            }
            else
                DeviceTypeModificationError(deviceType, newName);
        }

        private static void DeviceTypeModificationError(DeviceType deviceTypeToModify, string newDeviceTypeName)
        {
            if (!DeviceType.ValidDeviceTypeParameters(newDeviceTypeName))
                throw new ExceptionEmptyString("Error: name cannot be empty");
            if (!ValidDeviceType(deviceTypeToModify))
                throw new NullReferenceException("Error: no device type selected");
        }

        private static bool ValidDeviceType(DeviceType deviceType)
        {
            return deviceType != null;
        }
        public void EraseDeviceType(DeviceType deviceType)
        {

            if (DeviceTypeIsValidForErase(deviceType))
            {
                using (var db = new Context())
                {
                    db.DeviceTypes.Attach(deviceType);
                    db.DeviceTypes.Remove(deviceType);
                    db.SaveChanges();
                }
            }
            else
                ErrorErasingDeviceType(deviceType);
        }

        private bool DeviceTypeIsValidForErase(DeviceType deviceType)
        {
            return ValidDeviceType(deviceType) && !IsAssignedToDevice(deviceType);
        }
        private void ErrorErasingDeviceType(DeviceType deviceType)
        {
            if (IsAssignedToDevice(deviceType))
                throw new ExceptionErasingDeviceType("Error: cannot remove type that's being used by device");
            if (!ValidDeviceType(deviceType))
                throw new NullReferenceException("Error: device type not selected");
        }

        private bool IsAssignedToDevice(DeviceType deviceType)
        {
            bool assigned = false;
            using (var db = new Context())
            {
                IComponentHandler compHandler = ComponentHandler.ObtainInstance;
                foreach (Device device in compHandler.GetDevices())
                    if (device.Classification.DeviceTypeId.Equals(deviceType.DeviceTypeId))
                        assigned = true;
            }
            return assigned;
        }
    }
}