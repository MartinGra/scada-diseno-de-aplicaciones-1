﻿using System;
using System.Collections.Generic;
using SCADADomain;
using SCADAPersistence.Domain_InterfaceHandlers;
using SCADAPersistence.Incident_Managers;

namespace SCADAPersistence.Domain_Handlers
{
    public class IncidentHandler : IIncidentHandler
    {
        private static IncidentHandler instance;

        private IncidentHandler() { }

        public static IIncidentHandler ObtainInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new IncidentHandler();
                }
                return instance;
            }
        }
        public void CreateIncident(string severity, string description, Component comp, IncidentManager storage)
        {
            if (comp == null)
                throw new NullReferenceException("Incident must be for a selected component");
            Incident incident = new Incident(severity, description);
            storage.AddIncident(incident, comp);
        }
        public List<string> GetReports(Component comp, IncidentManager storage)
        {
            return storage.ObtainReports(comp);
        }
    }
}
