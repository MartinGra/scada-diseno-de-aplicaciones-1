﻿using System;
using SCADADomain;
using SCADAException;

namespace SCADAPersistence.Domain_Handlers
{
    public class PlantHandler
    {
        public static void CreatePlant(string name, string city, string address)
        {
            if (Globals.EmptyString(name) || Globals.EmptyString(city) || Globals.EmptyString(address))
                throw new ExceptionEmptyString("Error: all fields must be completed");              
            using (var db = new Context())
            {
                Plant plant = new Plant(name, city, address);
                db.Components.Add(plant);
                db.SaveChanges();
            }
        }

        public static void ErasePlant(Component component)
        {
            if (ComponentHandler.ValidComponent(component))
            {
                using (var db = new Context())
                {
                    db.Components.Attach(component);
                    ComponentHandler.RevokeChildsFromHierarchy(component);
                    component.ControlVariables.Clear();
                    component.Incidents.Clear();
                    db.Components.Remove(component);
                    db.SaveChanges();
                }
            }
            else
                throw new NullReferenceException("Error : Plant not selected");
        }

        internal static void ModifyPlant(Component component, string name)
        {
            ControlModifyParmeters(component, name);
            using (var db = new Context())
            {
                db.Components.Attach(component);
                component.Name = name;
                db.SaveChanges();
            }
        }

        private static void ControlModifyParmeters(Component component, string name)
        {
            if(Globals.EmptyString(name))
                throw new ExceptionEmptyString("Error : Name cannot be empty");
            if(component == null)
                throw new NullReferenceException("Error : Plant not selected");
        }
    }
}
