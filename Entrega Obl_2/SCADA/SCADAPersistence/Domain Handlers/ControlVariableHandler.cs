﻿using System;
using System.Linq;
using SCADADomain;
using SCADAException;
using SCADAPersistence.Domain_InterfaceHandlers;

namespace SCADAPersistence.Domain_Handlers
{
    public class ControlVariableHandler: IControlVariableHandler
    {
        private static ControlVariableHandler instance;

        private ControlVariableHandler() { }

        public static ControlVariableHandler ObtainInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ControlVariableHandler();
                }
                return instance;
            }
        }
        public ControlVariable CreateControlVariable(string nameOfVariable, float minVariableValue, float maxVariableValue)
        {
            return new ControlVariable(nameOfVariable, minVariableValue, maxVariableValue);
        }

        public void ControlVariableAssignValue(ControlVariable selectedControlVariable, float newValueOfVariable)
        {
            DateTime actualDateTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            using (var db = new Context())
            {
                db.ControlVariables.Attach(selectedControlVariable);
                selectedControlVariable.UpdateValue(newValueOfVariable, actualDateTime);
                db.SaveChanges();
            }
                
        }

        public void CreateControlVariableInComponent(Component component, string nameVariable, float minValueOfVariable, float maxValueOfVariable)
        {   
            ControlVariable controlVariable = CreateControlVariable(nameVariable, minValueOfVariable, maxValueOfVariable);
            using (var db = new Context())
            {
                db.Components.Attach(component);
                component.AddControlVariable(controlVariable);
                db.SaveChanges();
            }        
        }

        public void ControlVariableAssignValueInComponent(Component component, ControlVariable controlVariable, float newControlVariableValue)
        {
            if (Globals.ValidControlFloatRange(newControlVariableValue))
            {
                ControlVariableAssignValue(controlVariable, newControlVariableValue);
            }
            else
                throw new ExceptionInvalidFloatValue(Globals.MessageInvalidFloat);
        }

        public void EraseControlVariable(Component component, ControlVariable controlVariable)
        {
            if (ValidControlVariable(controlVariable) && ComponentHandler.ValidComponent(component))
            {
                using (var db = new Context()) {
                    db.Components.Attach(component);
                    int idCv = controlVariable.ControlVariableId;
                    ControlVariable selected = db.ControlVariables.Find(idCv);
                    component.ControlVariables.Remove(selected);
                    db.SaveChanges();                    
                }
            }
            else
                ErrorErasingControlVariable(component, controlVariable);
        }

        bool IControlVariableHandler.ControlVariableHasValuesAndLastOutOfRange(ControlVariable controlVariable)
        {
            return ControlVariableHasValuesAndLastOutOfRange(controlVariable);
        }

        private static bool ControlVariableHasValuesAndLastOutOfRange(ControlVariable controlVariable)
        {
            return controlVariable.HasLastOutOfRange();
        }

        public void ErrorErasingControlVariable(Component component, ControlVariable controlVariable)
        {
            if (!ValidControlVariable(controlVariable))
                throw new NullReferenceException("Error: control variable not selected");
            if (!ComponentHandler.ValidComponent(component))
                throw new NullReferenceException("Error: control variable not selected");
        }

        bool IControlVariableHandler.ValidControlVariable(ControlVariable controlVariable)
        {
            return ValidControlVariable(controlVariable);
        }

        private static bool ValidControlVariable(ControlVariable controlVariable)
        {
            return controlVariable != null;
        }        

        public void ModifyControlVariable(Component component, ControlVariable variable, string name, float minRange, float maxRange)
        {
            using (var db = new Context()) {
                db.Components.Attach(component);
                int idCv = variable.ControlVariableId;
                ControlVariable selected = db.ControlVariables.Include("ValueDatePair").SingleOrDefault(cv => cv.ControlVariableId == idCv);
                component.ModifyNameOfControlVariable(selected, name);
                component.ModifyRangesOfControlVariable(selected, minRange, maxRange);
                db.SaveChanges();
            }
        }

        public void CreateControlVariableWithWarningRanges(Component comp, string name, float minAlarm, float maxAlarm, float minWarning, float maxWarning)
        {
            if (ValidComponentForControlVariable(comp))
            {
                using (var db = new Context())
                {
                    db.Components.Attach(comp);
                    ControlVariable newVariable = new ControlVariable(name, minAlarm, maxAlarm, minWarning, maxWarning);
                    comp.ControlVariables.Add(newVariable);
                    db.SaveChanges();
                }
            }
            else
                ErrorCreatingControlVariableWithWarningRanges(comp);
        }

        void IControlVariableHandler.ErrorCreatingControlVariableWithWarningRanges(Component comp)
        {
            ErrorCreatingControlVariableWithWarningRanges(comp);
        }

        bool IControlVariableHandler.ValidComponentForControlVariable(Component comp)
        {
            return ValidComponentForControlVariable(comp);
        }

        public static void ErrorCreatingControlVariableWithWarningRanges(Component comp)
        {
            if(!ComponentHandler.ValidComponent(comp))
                throw new NullReferenceException("Error: Component not selected");
            if (comp.IsInUpperLevel())
                throw new Exception("Error: this component doesn't allow control variables");
        }

        public static bool ValidComponentForControlVariable(Component comp)
        {
            return ComponentHandler.ValidComponent(comp) && !comp.IsInUpperLevel();
        }

        public void ModifyControlVariableWithWarning(Component comp, ControlVariable variable, string name, float minAlarm, float maxAlarm, float minWarning, float maxWarning)
        {
            ModifyControlVariable(comp, variable, name, minAlarm, maxAlarm);
            ModifyWarningRanges(comp, variable, minWarning, maxWarning);
        }

        private void ModifyWarningRanges(Component comp, ControlVariable variable, float minWarning, float maxWarning)
        {
            using (var db = new Context())
            {
                db.Components.Attach(comp);
                int idCv = variable.ControlVariableId;
                ControlVariable selected = db.ControlVariables.Include("ValueDatePair").SingleOrDefault(cv => cv.ControlVariableId == idCv);               
                comp.ModifyWarningRangesOfControlVariable(variable, minWarning, maxWarning);
                db.SaveChanges();
            }
        }
    }
}
