﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using SCADADomain;
using System.Data.Entity;
using SCADAException;
using SCADAPersistence.Domain_InterfaceHandlers;

namespace SCADAPersistence.Domain_Handlers
{
    public class InstallationHandler
    {
        public static void CreateInstallation(string newInstallationName, Component selectedComponent)
        {
            using (var db = new Context())
            {
                db.Components.Attach(selectedComponent);
                Installation newInstallation = new Installation(newInstallationName, selectedComponent);
                db.Components.Add(newInstallation);
                db.SaveChanges();
            }
        }
        public static void ModifyInstallationName(Component installationToModify, string newInstallationName)
        {
            if (Installation.ValidInstallationName(newInstallationName) && ComponentHandler.ValidComponent(installationToModify))
            {
                using (var db = new Context())
                {
                    db.Components.Attach(installationToModify);
                    installationToModify.Name = newInstallationName;
                    db.SaveChanges();
                }
            }
            else
                InstallationModifyError(installationToModify, newInstallationName);
        }

        private static void InstallationModifyError(Component installation, string newInstallationName)
        {
            if (!ComponentHandler.ValidComponent(installation))
                throw new NullReferenceException("Error: a installation must be selected");

            if (Globals.EmptyString(newInstallationName))
                throw new ExceptionEmptyString("Error: installation name cannot be empty");
        }
        public static void EraseInstallation(Component component)
        {
            if (ComponentHandler.ValidComponent(component))
            {
                using (var db = new Context())
                {
                    Component comp =
                               db.Components.Include("Father")
                                   .Include("Incidents")
                                   .Include("ControlVariables")
                                   .FirstOrDefault(c => c.ComponentId == component.ComponentId);
                    db.Components.Attach(comp);

                    IComponentHandler compHandler = ComponentHandler.ObtainInstance;
                    ICollection<Component> coll = compHandler.FirstChilds(comp);
                    foreach (var child in coll)
                    {
                        db.Components.Include("Father").FirstOrDefault(c => c.ComponentId == child.ComponentId).Father = comp.Father;
                    }
                    ICollection<ControlVariable> newColl = new Collection<ControlVariable>();
                    foreach (ControlVariable variable in comp.ControlVariables)
                    {
                        ControlVariable cv =
                            db.ControlVariables.Include("ValueDatePair")
                                .FirstOrDefault(c => c.ControlVariableId == variable.ControlVariableId);
                        cv.ValueDatePair.Clear();
                        newColl.Add(cv);
                    }
                    comp.ControlVariables = newColl;
                    comp.ControlVariables.Clear();
                    comp.Incidents.Clear();
                    comp.Father = null;
                    ComponentHandler.RevokeChildsFromHierarchy(comp);
                    db.Components.Remove(comp);

                    db.SaveChanges();
                }
            }
            else
                throw new NullReferenceException("Error : Installation not selected");
            }
      }  
}
