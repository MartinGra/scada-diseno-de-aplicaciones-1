﻿using SCADADomain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using SCADAException;
using SCADAPersistence.Domain_InterfaceHandlers;

namespace SCADAPersistence.Domain_Handlers
{
    public class ComponentHandler : IComponentHandler
    {
        private static ComponentHandler instance;

        private ComponentHandler() { }

        public static IComponentHandler ObtainInstance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ComponentHandler();
                }
                return instance;
            }
        }
        public ICollection<Component> GetPlants()
        {
            using (var db = new Context())
                return db.Components.OfType<Plant>().Cast<Component>().ToList();
        }
        public ICollection<Component> GetComponents()
        {
            using (var db = new Context())
                return db.Components.Include(x => x.ControlVariables.Select(y => y.ValueDatePair)).ToList();
        }

        public ICollection<Component> GetDevices()
        {
            using (var db = new Context())
                return db.Components.Include("ControlVariables").Include("Classification").OfType<Device>().Cast<Component>().ToList();
        }

        public ICollection<Component> GetInstallations()
        {
            using (var db = new Context())
                return db.Components.Include("ControlVariables").OfType<Installation>().Cast<Component>().ToList();
        }

        public ICollection<Component> Hierarchy(Component comp)
        {
            ICollection<Component> hierarchy = new Collection<Component>();
            using (var db = new Context()) {
                foreach (Component son in this.GetComponents()) {
                    db.Components.Attach(son);
                    if (son.HasFather() && son.Father.ComponentId == comp.ComponentId)
                        hierarchy.Add(son);
                }
                foreach (Component childFound in hierarchy)
                    return hierarchy.Concat(Hierarchy(childFound)).ToList();
            }
            return hierarchy;
        }

        public ICollection<Component> FirstChilds(Component component)
        {
            ICollection<Component> firstChilds = new Collection<Component>();
            using (var db = new Context())
            {
                foreach (Component son in GetComponents())
                {
                    db.Components.Attach(son);
                    if (son.HasFather() && son.Father.Equals(component))
                        firstChilds.Add(son);
                }
            }
            return firstChilds;
        }

        private static void ErrorRemovingComponentFromHierarchy(Component selectedComponent)
        {
            if (!ValidComponent(selectedComponent))
                throw new NullReferenceException("Error: the component to remove is not selected");

            if (!ComponentHasFather(selectedComponent))
                throw new ExceptionComponentNotInHierarchy("Error: the component is not in a hierarchy");
        }

        public static bool ComponentHasFather(Component selectedComponent)
        {
            return selectedComponent.HasFather();
        }
        public void AssignChildToFatherHierarchy(Component child, Component father)
        {
            if (ValidAssignmentParameters(child, father))
            {
                using (var db = new Context())
                {
                    Component childComp = db.Components.Where(c => c.ComponentId == child.ComponentId).Include("Father").FirstOrDefault();
                    Component fatherComp = db.Components.Where(c => c.ComponentId == father.ComponentId).Include("Father").FirstOrDefault();

                    db.Components.Attach(childComp);
                    db.Components.Attach(fatherComp);

                    if (NewFatherPreviouslyChildOfNewChild(childComp, fatherComp))                    
                         fatherComp.Father = childComp.Father;
                                    
                    childComp.Father = null;
                    childComp.Father = fatherComp;
                    db.SaveChanges();
                }
            }
            else
                HierarchyAssignError(child, father);
        }

        private static bool NewFatherPreviouslyChildOfNewChild(Component childComp, Component fatherComp)
        {
            return fatherComp.HasFather() && fatherComp.Father.Equals(childComp);
        }

        public int GetTotalAlarms(Component comp)
        {
            using (var db = new Context())
            {
                db.Components.Attach(comp);
                int totalAlarms = comp.CurrentAlarms();
                ICollection<Component> hierarchy = Hierarchy(comp);
                foreach (Component child in hierarchy)
                {
                    Component childComp = db.Components
                                            .Include(x => x.ControlVariables.Select(y => y.ValueDatePair))
                                            .FirstOrDefault(z => z.ComponentId == child.ComponentId);

                    totalAlarms += childComp.CurrentAlarms();
                }
                return totalAlarms;
            }
        }

        public static bool ValidAssignmentParameters(Component child, Component father)
        {
            return ValidComponent(child) && ValidComponent(father) && 
                !child.Equals(father) && ValidFatherComponent(father)
                && father.IsAcceptedChild(child);
        }

        private static bool ValidFatherComponent(Component father)
        {
            return !father.IsInLowerLevel();
        }
        
        private static void HierarchyAssignError(Component child, Component father)
        {
            if (!ValidComponent(child) || !ValidComponent(father))
                throw new NullReferenceException("Error: component not selected");
            if (child.Equals(father))
                throw new ExceptionCantAssignComponentToItself("Error: component cannot be assigned to itself");            
             if (!ValidFatherComponent(father))
                throw new ExceptionCantAssignChildToComponent("Error: a father must be selected");
            if (!father.IsAcceptedChild(child))
                throw new ExceptionCantAssignChildToComponent("Error: the child can't be in hierarchy of selected father");
        }

        public ICollection<AssignValue> HistoricValues(Component component, ControlVariable variable)
        {
            if (ValidComponent(component) && variable != null)
            {
                using (var db = new Context())
                {
                    db.Components
                        .Include(comp => comp.ControlVariables.Select(cv => cv.ValueDatePair))
                        .FirstOrDefault(c => c.ComponentId == component.ComponentId);

                    int idCv = variable.ControlVariableId;
                    ControlVariable selected = db.ControlVariables.Find(idCv);
                    return component.HistoricValues(selected);
                }
            }
            else
                ErrorHistoricValues(component, variable);
            return null;                            
        }

        private void ErrorHistoricValues(Component component, ControlVariable variable)
        {
            if(!ValidComponent(component) || variable == null)
                throw new NullReferenceException("Error: no component or variable selected");
        }

        public static bool ChildHasFather(Component child)
        {
            return child.Father != null;
        }       

        public static void RemoveFromHierarchy(Component component)
        {
            if (ValidComponent(component.Father))
            {
                component.Father = null;
            }
        }

        public static bool ValidComponent(Component component)
        {
            return component != null;
        }

        public static void RevokeChildsFromHierarchy(Component component)
        {
            foreach (Component child in ComponentHandler.ObtainInstance.GetComponents())      
                if (child.HasFather() && child.Father.ComponentId == component.ComponentId)                    
                    child.Father = component.Father;                                        
        }
        
        public void CreatePlant(string name, string city, string address)
        {
           PlantHandler.CreatePlant(name, city, address);
        }

        public void ErasePlant(Component component)
        {
            PlantHandler.ErasePlant(component);
        }

        public void CreateInstallation(string newInstallationName, Component selectedComponent)
        {
            InstallationHandler.CreateInstallation(newInstallationName, selectedComponent);
        }

        public void ModifyInstallationName(Component installationToModify, string newInstallationName)
        {
            InstallationHandler.ModifyInstallationName(installationToModify, newInstallationName);
        }

        public void EraseInstallation(Component component)
        {
            InstallationHandler.EraseInstallation(component);
        }

        public void CreateDevice(string newDeviceName, DeviceType selectedType)
        {
            DeviceHandler.CreateDevice(newDeviceName, selectedType);
        }

        public void ModifyDeviceName(Component deviceToModify, string newDeviceName)
        {
            DeviceHandler.ModifyDeviceName(deviceToModify, newDeviceName);
        }

        public void EraseDevice(Component component)
        {
            DeviceHandler.EraseDevice(component);
        }

        public void ModifyPlant(Component component, string name)
        {
            PlantHandler.ModifyPlant(component, name);
        }

        public int GetTotalWarnings(Component comp)
        {
            if (!ValidComponent(comp))
                throw new NullReferenceException("Error: no component selected");
            using (var db = new Context())
            {
                db.Components.Attach(comp);
                int totalAlarms = comp.CurrentWarnings();
                ICollection<Component> hierarchy = Hierarchy(comp);
                foreach (Component child in hierarchy)
                {
                    Component childComp = db.Components.Include(x => x.ControlVariables.Select(y => y.ValueDatePair)).FirstOrDefault(z => z.ComponentId == child.ComponentId);                    
                    totalAlarms += childComp.CurrentWarnings();                
                }
                return totalAlarms;
            }
        }
    }
}