﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SCADADomain;
using SCADAException;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
using SCADAFacade;

namespace SCADATest
{
    [TestClass]
    public class UnitTestIndustrialPlant
    {
        private IndustrialPlant NewIndustrialPlantImp()
        {
           return IndustrialPlantDBImp.ObtainInstance;
        }

        private void EmptyDataBase(IndustrialPlant industrialPlant)
        {
            using (var db = new Context()) { 
                foreach (var entity in db.Components)   
                    db.Components.Remove(entity);

                foreach (var entity in db.ControlVariables)
                    db.ControlVariables.Remove(entity);

                foreach (var entity in db.DeviceTypes)
                    db.DeviceTypes.Remove(entity);

                foreach (var entity in db.Incidents)
                    db.Incidents.Remove(entity);

                foreach (var entity in db.AssignValues)
                    db.AssignValues.Remove(entity);

                db.SaveChanges();
            }
        }

        [TestMethod]
        public void TestCreatePlant()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            Assert.AreEqual("plant",industry.Plants.ElementAt(0).Name);
        }
        /*Test ErasePlant*/
        [TestMethod]
        public void TestErasePlant()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            Assert.AreEqual("plant", industry.Plants.ElementAt(0).Name);
        }
        /*Tests CreateInstallation*/
        [TestMethod]
        public void TestCreateInstallationWithNameValid()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("install", industry.Plants.ElementAt(0));
            Assert.AreEqual(1, industry.Installations.Count);
        }
        [ExpectedException(typeof(ExceptionEmptyString))]
        [TestMethod]
        public void TestCreateInstallationWithEmptyName()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("", industry.Plants.ElementAt(0));
        }
        [TestMethod]
        public void TestCreateInstallationWithExistingName()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("install", industry.Plants.ElementAt(0));
            industry.CreateInstallation("install", industry.Plants.ElementAt(0));
            Assert.AreEqual(industry.Installations.Count, 2);
        }
        /*Tests CreateDeviceType*/
        [TestMethod]
        public void TestCreateDeviceTypeValidNameAndDescription()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            DeviceType newType = new DeviceType("Motor", "Se utiliza para generar energia");
            industry.CreateDeviceType("Motor", "Se utiliza para generar energia");
            Assert.AreEqual(newType, industry.DeviceTypes.ElementAt(0));
        }
        [ExpectedException(typeof(ExceptionEmptyString))]
        [TestMethod]
        public void TestCreateDeviceTypeWithEmptyName()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("", "description");
        }
        [TestMethod]
        public void TestCreateDeviceTypeWithEmptyDescription()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("name", "");
            Assert.AreEqual("", industry.DeviceTypes.ElementAt(0).Description);
        }
        
        [TestMethod]
        public void TestCreateDeviceTypeWithExistingName()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("motor", "Se utiliza para generar energia");
            industry.CreateDeviceType("motor", "Se utiliza para la energia");
            Assert.AreEqual(2, industry.DeviceTypes.Count);
        }
        /*Tests CreateDevice*/
        [TestMethod]
        public void TestCreateDeviceWithNameAndTypeValid()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("Molino", "Maquina que sirve para moler utilizando energía");
            industry.CreateDevice("Molino de viento", industry.DeviceTypes.ElementAt(0));            
            Assert.AreEqual(1, industry.Devices.Count);
        }

        [ExpectedException(typeof(ExceptionEmptyString))]
        [TestMethod]
        public void TestCreateDeviceWithEmptyName()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            DeviceType selectedType = new DeviceType("Molino", "Maquina que sirve para moler utilizando energía");
            industry.CreateDevice("", selectedType);
        }

        [ExpectedException(typeof(NullReferenceException))]
        [TestMethod]
        public void TestCreateDeviceWithUnselectedType()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            DeviceType unselectedDeviceType = null;
            industry.CreateDevice("Molino de viento", unselectedDeviceType);
        }
        [TestMethod]
        public void TestCreateDeviceThatsNotUnique()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("Molino", "Generador de energia");
            DeviceType devType = industry.DeviceTypes.ElementAt(0);
            industry.CreateDevice("Molino de viento", devType);
            industry.CreateDevice("Molino de viento", devType);
            Assert.AreEqual(2, industry.Devices.Count);
        }
        /*Tests CreateControlVariable*/
        [TestMethod]
        public void TestCreateValidControlVariableInPlant()
        {
            float minTemperatureValue = 0;
            float maxTemperatureValue = 10;
            ControlVariable temperatureOfComponent = new ControlVariable("Temperatura", minTemperatureValue, maxTemperatureValue);
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("install", industry.Plants.ElementAt(0));
            Component newPlant = industry.Installations.ElementAt(0);
            industry.CreateControlVariableInComponent(newPlant, "Temperatura", minTemperatureValue, maxTemperatureValue);
            Assert.AreEqual(newPlant.ControlVariables.ElementAt(0), industry.Installations.ElementAt(0).ControlVariables.ElementAt(0));
        }
        [ExpectedException(typeof(ExceptionInvalidFloatValue))]
        [TestMethod]
        public void TestCreateInvalidControlVariableFloatValue()
        {
            float minTemperatureValue = -50000.4f;
            float maxTemperatureValue = 1000000000000000.3f;
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("install", industry.Plants.ElementAt(0));
            Component newInstallation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableInComponent(newInstallation, "Temperatura", minTemperatureValue, maxTemperatureValue);
        }
        [ExpectedException(typeof(ExceptionInconsistentMinMaxValue))]
        [TestMethod]
        public void TestInvalidCreateControlVariableMinMax()
        {
            float minTemperatureValue = 50000.4f;
            float maxTemperatureValue = -10000.3f;
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("install", industry.Plants.ElementAt(0));
            Component newPlant = industry.Installations.ElementAt(0);
            industry.CreateControlVariableInComponent(newPlant, "Temperatura", minTemperatureValue, maxTemperatureValue);

        }
        [ExpectedException(typeof(ExceptionEmptyString))]
        [TestMethod]
        public void TestInvalidCreateControlVariableName()
       {
            string newName = "  ";
            float minTemperatureValue = 3000.4f;
            float maxTemperatureValue = 10000.3f;
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("install", industry.Plants.ElementAt(0));
            Component newPlant = industry.Installations.ElementAt(0);
            industry.CreateControlVariableInComponent(newPlant, newName, minTemperatureValue, maxTemperatureValue);
        }
        /*Tests ControlVariableAssignValue*/
        [TestMethod]
        public void TestControlVariableAssignValueInRange()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            float minTemperatureValue = 0;
            float maxTemperatureValue = 10;
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("install", industry.Plants.ElementAt(0));
            Component plantCreated = industry.Installations.ElementAt(0);
            industry.CreateControlVariableInComponent(plantCreated, "Temperatura", minTemperatureValue, maxTemperatureValue);
            float newTemperatureValue = 8;
            industry.ControlVariableAssignValueInComponent(plantCreated, plantCreated.ControlVariables.ElementAt(0), newTemperatureValue);

            Assert.AreEqual(newTemperatureValue, plantCreated.ControlVariables.ElementAt(0).GetLastValue().Item1);
        }
        [TestMethod]
        public void TestControlVariableAssignValueOffRange()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            float minVariableValue = -1003.23f;
            float maxVariableValue = 5003.1235f;
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("install", industry.Plants.ElementAt(0));
            Component plantCreated = industry.Installations.ElementAt(0);
            industry.CreateControlVariableInComponent(plantCreated, "Presion", minVariableValue, maxVariableValue);

            float newVariableValue = 5003.1239f;
            industry.ControlVariableAssignValueInComponent(plantCreated, plantCreated.ControlVariables.ElementAt(0), newVariableValue);
            ControlVariable controlVariable = plantCreated.ControlVariables.ElementAt(0);
            Assert.AreEqual(5003.1239f, controlVariable.GetLastValue().Item1);
        }
        [TestMethod]
        public void TestControlVariableAssignValueInExactRange()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            float minVariableValue = -1337f;
            float maxVariableValue = 5003f;
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("install", industry.Plants.ElementAt(0));
            Component plantCreated = industry.Installations.ElementAt(0);
            industry.CreateControlVariableInComponent(plantCreated, "Presion", minVariableValue, maxVariableValue);

            float newVariableValue = -1337f;
            industry.ControlVariableAssignValueInComponent(plantCreated, plantCreated.ControlVariables.ElementAt(0), newVariableValue);

            Assert.AreEqual(newVariableValue, plantCreated.ControlVariables.ElementAt(0).GetLastValue().Item1);
        }

        [ExpectedException(typeof(NullReferenceException))]
        [TestMethod]
        public void TestFailToAssignNullChildToHierarchy()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            Component unselectedChild = null;
            industry.AssignChildToFatherHierarchy(unselectedChild, industry.Installations.ElementAt(0));
        }
        [ExpectedException(typeof(NullReferenceException))]
        [TestMethod]
        public void TestFailToAssignChildToNullFather()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            Component unselectedFather = null;
            industry.AssignChildToFatherHierarchy(industry.Installations.ElementAt(0), unselectedFather);
        }
        [ExpectedException(typeof(ExceptionCantAssignComponentToItself))]
        [TestMethod]
        public void TestFailToAssignComponentToItself()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            industry.AssignChildToFatherHierarchy(industry.Installations.ElementAt(0), industry.Installations.ElementAt(0));
        }
        [ExpectedException(typeof(ExceptionCantAssignChildToComponent))]
        [TestMethod]
        public void TestFailToAssignChildToComponentDisallowedToBeFather()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("Molinero", "generador de energia");
            industry.CreateDevice("Molino electrico", industry.DeviceTypes.ElementAt(0));
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            industry.AssignChildToFatherHierarchy(industry.Installations.ElementAt(0), industry.Devices.ElementAt(0));
        }
        
        /*Tests GetAlarms*/
        [TestMethod]
        public void TestGetAlarmsComponent()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            Component instalacion = industry.Installations.ElementAt(0);
            industry.CreateControlVariableInComponent(instalacion, "variableDeControl", 0, 10);
            ControlVariable variableDeControl = instalacion.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(instalacion, variableDeControl, -1f);
            Assert.AreEqual(1, industry.GetTotalAlarms(instalacion));
        }
        [TestMethod]
        public void TestGetAlarmsSubtractChangeInComponent()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            Component planta = industry.Installations.ElementAt(0);
            industry.CreateControlVariableInComponent(planta, "variableDeControl", 0, 10);
            ControlVariable variableDeControl = planta.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(planta, variableDeControl, -1f);
            industry.ControlVariableAssignValueInComponent(planta, variableDeControl, 9);
            Assert.AreEqual(0, industry.GetTotalAlarms(planta));
        }
        [TestMethod]
        public void TestGetAlarmsComponentWithComponents()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            industry.CreateInstallation("Molienda", industry.Plants.ElementAt(0));
            industry.CreateDeviceType("tipo 1", "tipo default");
            industry.CreateDevice("disp 1", industry.DeviceTypes.ElementAt(0));
    
            Component planta = industry.Installations.ElementAt(0);
            Component planta2 = industry.Installations.ElementAt(1);
            Component disp = industry.Devices.ElementAt(0);

            industry.AssignChildToFatherHierarchy(planta2, planta);
            industry.AssignChildToFatherHierarchy(disp, planta2);
            industry.CreateControlVariableInComponent(planta, "variableDeControl", 0, 10);
            industry.CreateControlVariableInComponent(planta2, "variableDeControl2", 0, 10);
            industry.CreateControlVariableInComponent(planta2, "variableDeControl3", 0, 10);
            industry.CreateControlVariableInComponent(disp, "variableDeControl4", 0, 10);
            ControlVariable variableDeControl1 = planta.ControlVariables.ElementAt(0);
            ControlVariable variableDeControl2 = planta2.ControlVariables.ElementAt(0);
            ControlVariable variableDeControl3 = planta2.ControlVariables.ElementAt(1);
            ControlVariable variableDeControl4 = disp.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(planta, variableDeControl1, -1f);
            industry.ControlVariableAssignValueInComponent(planta2, variableDeControl2, 9);
            industry.ControlVariableAssignValueInComponent(disp, variableDeControl4, -3f);
            industry.ControlVariableAssignValueInComponent(planta2, variableDeControl3, 22);
            industry.ControlVariableAssignValueInComponent(planta2, variableDeControl2, 21);
            industry.ControlVariableAssignValueInComponent(planta2, variableDeControl3, 8);            
            Assert.AreEqual(3, industry.GetTotalAlarms(planta));            
        }
        /* Tests ModifyInstallationName */
        [TestMethod]
        public void TestModifyInstallationNameWithValidName()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            industry.ModifyInstallationName(industry.Installations.ElementAt(0), "Instalacion 2");
            Assert.AreEqual("Instalacion 2", industry.Installations.ElementAt(0).Name);
        }
        [ExpectedException(typeof(ExceptionEmptyString))]
        [TestMethod]
        public void TestFailToModifyInstallationNameWithEmptyName()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            industry.ModifyInstallationName(industry.Installations.ElementAt(0),"");
        }
        [TestMethod]
        public void TestFailToModifyInstallationNameWithExistingName()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            industry.CreateInstallation("Molienda", industry.Plants.ElementAt(0));
            industry.ModifyInstallationName(industry.Installations.ElementAt(0), "Molienda");
            Assert.AreEqual("Molienda", industry.Installations.ElementAt(0).Name);
        }

        /* Tests ModifyDeviceName */
        [TestMethod]
        public void TestModifyDeviceNameWithValidName()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("Molino", "generador de energia");
            industry.CreateDevice("Molino de viento", industry.DeviceTypes.ElementAt(0));
            industry.ModifyDeviceName(industry.Devices.ElementAt(0), "Molino hidraulico");
            Assert.AreEqual("Molino hidraulico", industry.Devices.ElementAt(0).Name);
        }
        [ExpectedException(typeof(ExceptionEmptyString))]
        [TestMethod]
        public void TestFailToModifyDeviceNameWithEmptyName()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("Molino", "generador de energia");
            industry.CreateDevice("Molino de viento", industry.DeviceTypes.ElementAt(0));
            industry.ModifyDeviceName(industry.Devices.ElementAt(0),"");
        }
        [TestMethod]
        public void TestModifyDeviceNameWithExistingName()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("Molino", "generador de energia");
            industry.CreateDevice("Molino hidraulico", industry.DeviceTypes.ElementAt(0));
            industry.CreateDevice("Molino de viento", industry.DeviceTypes.ElementAt(0));
            industry.ModifyDeviceName(industry.Devices.ElementAt(0), "Molino de viento");
            Assert.AreEqual("Molino de viento", industry.Devices.ElementAt(0).Name);
        }
        
        /* Tests ModifyControlVariableOfComponent*/
        [TestMethod]
        public void TestModifyNameOfControlVariableOfSelectedComponent()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            industry.CreateControlVariableInComponent(industry.Installations.ElementAt(0), "Temperatura", 0, 10);
            Component selectedPlant = industry.Installations.ElementAt(0);
            ControlVariable selectedVariable = selectedPlant.ControlVariables.ElementAt(0);
            selectedPlant.ModifyNameOfControlVariable(selectedVariable, "Presion");
            Assert.AreEqual("Presion", selectedVariable.Name);
        }

        [ExpectedException(typeof(ExceptionEmptyString))]
        [TestMethod]
        public void TestFailToModifyEmptyNameOfControlVariableOfComponent()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            industry.CreateControlVariableInComponent(industry.Installations.ElementAt(0), "Temperatura", 0, 10);
            Component selectedPlant = industry.Installations.ElementAt(0);
            ControlVariable selectedVariable = selectedPlant.ControlVariables.ElementAt(0);
            selectedPlant.ModifyNameOfControlVariable(selectedVariable, "");
        }
        [ExpectedException(typeof(ExceptionExistingControlVariableName))]
        [TestMethod]
        public void TestFailToModifyExistingNameOfControlVariableOfComponent()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            industry.CreateControlVariableInComponent(industry.Installations.ElementAt(0), "Temperatura", 0, 10);
            industry.CreateControlVariableInComponent(industry.Installations.ElementAt(0), "Presion", 0, 10);
            Component selectedPlant = industry.Installations.ElementAt(0);
            ControlVariable selectedVariable = selectedPlant.ControlVariables.ElementAt(0);
            selectedPlant.ModifyNameOfControlVariable(selectedVariable, "Presion");
        }
        /*Tests ModifyRangesOfControlVariable*/
        [TestMethod]
        public void TestModifyRangesOfControlVariableOfComponent()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            industry.CreateControlVariableInComponent(industry.Installations.ElementAt(0), "Temperatura", 0, 10);
            Component selectedPlant = industry.Installations.ElementAt(0);
            ControlVariable selectedVariable = selectedPlant.ControlVariables.ElementAt(0);
            selectedPlant.ModifyRangesOfControlVariable(selectedVariable, -1000.31f, 30300.721f);
            List<float> expected = new List<float>();
            expected.Add(-1000.31f);
            expected.Add(30300.721f);
            float minModified = selectedVariable.MinRange;
            float maxModified = selectedVariable.MaxRange;
            List<float> result = new List<float>();
            result.Add(minModified);
            result.Add(maxModified);
            CollectionAssert.AreEqual(result, expected);
        }
        [TestMethod]
        public void TestModifyRangesToSameValueOfControlVariable()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            industry.CreateControlVariableInComponent(industry.Installations.ElementAt(0), "Temperatura", 0, 10);
            Component selectedPlant = industry.Installations.ElementAt(0);
            ControlVariable selectedVariable = selectedPlant.ControlVariables.ElementAt(0);
            selectedPlant.ModifyRangesOfControlVariable(selectedVariable, 0, 0);
            List<float> expected = new List<float>();
            expected.Add(0);
            expected.Add(0);
            List<float> result = new List<float>();
            result.Add(selectedVariable.MinRange);
            result.Add(selectedVariable.MaxRange);
            CollectionAssert.AreEqual(result, expected);
        }
        [ExpectedException(typeof(ExceptionInvalidFloatValue))]
        [TestMethod]
        public void TestFailToModifyRangesOfControlVariableToInvalidFloat()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            industry.CreateControlVariableInComponent(industry.Installations.ElementAt(0), "Temperatura", 0, 10);
            Component selectedPlant = industry.Installations.ElementAt(0);
            ControlVariable selectedVariable = selectedPlant.ControlVariables.ElementAt(0);
            selectedPlant.ModifyRangesOfControlVariable(selectedVariable, -30f, 10000000.1f);
        }
        [ExpectedException(typeof(ExceptionInconsistentMinMaxValue))]
        [TestMethod]
        public void TestFailToModifyRangesOfControlVariableToInconsistentRanges()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            industry.CreateControlVariableInComponent(industry.Installations.ElementAt(0), "Temperatura", 0, 10);
            Component installation = industry.Installations.ElementAt(0);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            installation.ModifyRangesOfControlVariable(variable, 200f, 0f);
        }
        /* Tests ModifyDeviceType */
        [TestMethod]
        public void TestModifyNameOfDeviceType()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("Tipo 1", "descrip. tipo 1");
            industry.ModifyDeviceType(industry.DeviceTypes.ElementAt(0), "Nuevo Nombre Tipo 1", "descrip. tipo 1");
            DeviceType expected = new DeviceType("Nuevo Nombre Tipo 1", "descrip. tipo 1");
            Assert.AreEqual(expected, industry.DeviceTypes.ElementAt(0));
        }
        [TestMethod]
        public void TestModifyDescriptionOfDeviceType()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("Tipo 1", "descrip. tipo 1");
            industry.ModifyDeviceType(industry.DeviceTypes.ElementAt(0), "Tipo 1", "Nueva descrip. tipo 1");
            Assert.AreEqual("Nueva descrip. tipo 1", industry.DeviceTypes.ElementAt(0).Description);
        }
        [TestMethod]
        public void TestModifyDescriptionOfDeviceTypeToEmptyDescription()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("Tipo 1", "descrip. tipo 1");
            industry.ModifyDeviceType(industry.DeviceTypes.ElementAt(0), "Tipo 1", "");
            Assert.AreEqual("", industry.DeviceTypes.ElementAt(0).Description);
        }
        [TestMethod]
        public void TestModifyNameOfDeviceTypeWithExistingOne()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("Tipo 1", "descrip. tipo 1");
            industry.CreateDeviceType("Tipo 2", "descrip. tipo 2");
            industry.ModifyDeviceType(industry.DeviceTypes.ElementAt(0), "Tipo 2", "descrip. tipo 1");
            Assert.AreEqual("Tipo 2", industry.DeviceTypes.ElementAt(0).Name);
        }
        [ExpectedException(typeof(ExceptionEmptyString))]
        [TestMethod]
        public void TestFailToModifyNameOfDeviceTypeWithEmptyName()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("Tipo 1", "descrip. tipo 1");
            industry.ModifyDeviceType(industry.DeviceTypes.ElementAt(0), "", "descrip. tipo 1");
        }
        [ExpectedException(typeof(NullReferenceException))]
        [TestMethod]
        public void TestFailToModifyDeviceTypeWithNullType()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.ModifyDeviceType(null, "Nombre", "Descripcion");
        }

        /*Tests erase device*/
        [TestMethod]
        public void TestEraseDevice()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("Tipo 1", "descrip. tipo 1");
            industry.CreateDevice("nombreDevice", industry.DeviceTypes.ElementAt(0));
            industry.EraseDevice(industry.Devices.ElementAt(0));
            Assert.AreEqual(0, industry.Devices.Count);
        }
        [ExpectedException(typeof(NullReferenceException))]
        [TestMethod]
        public void TestEraseNullDevice()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.EraseDevice(null);
        }

        [TestMethod]
        public void TestEraseDeviceWithControlVariable()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("Tipo 1", "descrip. tipo 1");
            industry.CreateDevice("nombreDevice", industry.DeviceTypes.ElementAt(0));
            Component device = industry.Devices.ElementAt(0);
            industry.CreateControlVariableInComponent(industry.Devices.ElementAt(0), "controlVar", 0, 10);
            industry.CreateControlVariableInComponent(industry.Devices.ElementAt(0), "controlVar2", 10, 20);
            industry.EraseDevice(industry.Devices.ElementAt(0));
            Assert.AreEqual(0, industry.Devices.Count);
            Assert.AreEqual(0, device.ControlVariables.Count);
        }
        [TestMethod]
        public void TestEraseControlVariable()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("Tipo 1", "descrip. tipo 1");
            industry.CreateDevice("nombreDevice", industry.DeviceTypes.ElementAt(0));
            industry.CreateControlVariableInComponent(industry.Devices.ElementAt(0), "var1", 1, 2);
            industry.EraseControlVariable(industry.Devices.ElementAt(0), industry.Devices.ElementAt(0).ControlVariables.ElementAt(0));
            Assert.AreEqual(0, industry.Devices.ElementAt(0).ControlVariables.Count);
        }
        [ExpectedException(typeof(NullReferenceException))]
        [TestMethod]
        public void TestEraseNullControlVariable()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("Tipo 1", "descrip. tipo 1");
            industry.CreateDevice("nombreDevice", industry.DeviceTypes.ElementAt(0));
            industry.EraseControlVariable(industry.Devices.ElementAt(0), null);
        }
        [ExpectedException(typeof(NullReferenceException))]
        [TestMethod]
        public void TestEraseNullControlVariableWithNullComponent()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.EraseControlVariable(null, null);
        }
        [ExpectedException(typeof(NullReferenceException))]
        [TestMethod]
        public void TestEraseControlVariableWithNullComponent()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            industry.CreateControlVariableInComponent(industry.Installations.ElementAt(0), "Temperatura", 0, 100);
            industry.EraseControlVariable(null, industry.Installations.ElementAt(0).ControlVariables.ElementAt(0));
        }

        [TestMethod]
        public void TestEraseControlVariableWithAlarm()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("Tipo 1", "descrip. tipo 1");
            industry.CreateDevice("nombreDevice", industry.DeviceTypes.ElementAt(0));
            Component device = industry.Devices.ElementAt(0);
            industry.CreateControlVariableInComponent(device, "var1", 1, 2);
            ControlVariable variable = device.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(device, variable, -5f);
            industry.EraseControlVariable(device, variable);
            Assert.AreEqual(0, industry.GetTotalAlarms(device));
        }
        /*Tests Installation erase*/
        [TestMethod]
        public void TestEraseInstallation()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            industry.EraseInstallation(industry.Installations.ElementAt(0));
            Assert.AreEqual(0, industry.Installations.Count);
        }
        [TestMethod]
        public void TestEraseInstallationWithChildInHierarchy()
        {
            IndustrialPlant industry = NewIndustrialPlantImp();
            EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            industry.CreateInstallation("Molienda", industry.Plants.ElementAt(0));
            industry.AssignChildToFatherHierarchy(industry.Installations.ElementAt(1), industry.Installations.ElementAt(0));
            industry.EraseInstallation(industry.Installations.ElementAt(0));
            Assert.AreEqual(null, industry.Installations.ElementAt(0).Father);
        }

        [ExpectedException (typeof(NullReferenceException))]
        [TestMethod]
        public void TestEraseNullPlant()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.EraseInstallation(null);
        }
        /*Tests erase device type*/
        [TestMethod]
        public void TestEraseDeviceType()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("Molino", "generador de energia");
            industry.EraseDeviceType(industry.DeviceTypes.ElementAt(0));
            Assert.AreEqual(0, industry.DeviceTypes.Count);
        }
        [ExpectedException (typeof(ExceptionErasingDeviceType))]
        [TestMethod]
        public void TestFailToEraseDeviceTypeAssignedToDevice()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateDeviceType("Molino", "generador de energia");
            industry.CreateDevice("Molino de viento", industry.DeviceTypes.ElementAt(0));
            industry.EraseDeviceType(industry.DeviceTypes.ElementAt(0));
        }
        [ExpectedException(typeof(NullReferenceException))]
        [TestMethod]
        public void TestFailToEraseNullDeviceType()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.EraseDeviceType(null);
        }

        [TestMethod]
        public void TestCreateControlVariableWithProperWarningRanges()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "Temperature", 0, 10, 4, 8);
            Assert.AreEqual(4, installation.ControlVariables.ElementAt(0).MinRangeWarning);
            Assert.AreEqual(8, installation.ControlVariables.ElementAt(0).MaxRangeWarning);
        }

        [ExpectedException(typeof(ExceptionInconsistentMinMaxValue))]
        [TestMethod]
        public void TestCreateControlVariableWithInconsistentWarningRanges()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "Temperature", 0, 10, 8, 4);
        }
        [ExpectedException(typeof(ExceptionInconsistentMinMaxValue))]
        [TestMethod]
        public void TestCreateControlVariableWithWarningRangesInconsistentToAlarmRanges()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "Temperature", 0, 10, -5, 15);
        }
        [ExpectedException(typeof(ExceptionInvalidFloatValue))]
        [TestMethod]
        public void TestCreateControlVariableWithInvalidFloatWarningRanges()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "city", "add");
            industry.CreateInstallation("Extraccion", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "Temperature", 0, 10, 2, Globals.MAX_FLOAT_VALUE + 1);
        }
        [ExpectedException(typeof(NullReferenceException))]
        [TestMethod]
        public void TestCreateControlVariableWithNullComponent()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreateControlVariableWithWarningRanges(null, "Temperature", 0, 10, 2, 5);
        }
        [ExpectedException(typeof(Exception))]
        [TestMethod]
        public void TestFailToCreateControlVariableWithPlant()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant 1", "montevideo", "dir");
            industry.CreateControlVariableWithWarningRanges(industry.Plants.ElementAt(0), "Temperature", 0, 10, 2, 5);
        }
        [TestMethod]
        public void TestNewHierarchyAssignment()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("Plant", "mont", "dir");
            industry.CreatePlant("Plant2", "mont", "dir2");

            industry.AssignChildToFatherHierarchy(industry.Plants.ElementAt(1), industry.Plants.ElementAt(0));
            Component child = industry.Plants.ElementAt(1);
            Component father = industry.Plants.ElementAt(0);
            List<Component> expected = new List<Component>();
            expected.Add(child);

            List<Component> obtained = industry.Hierarchy(father).ToList();
            CollectionAssert.AreEqual(expected, obtained);
        }
        [TestMethod]
        public void TestNewHierarchyAssignmentSimple1()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("Plant", "mont", "dir");
            industry.CreatePlant("Plant2", "mont", "dir2");
            industry.CreateInstallation("2 - Insta1", industry.Plants.ElementAt(1));

            Component plant2 = industry.Plants.ElementAt(1);
            Component plant1 = industry.Plants.ElementAt(0);
            Component installation = industry.Installations.ElementAt(0);

            industry.AssignChildToFatherHierarchy(plant2, plant1);
            
            List<Component> expected = new List<Component>();
            expected.Add(plant2);
            expected.Add(installation);

            List<Component> obtained = industry.Hierarchy(plant1).ToList();
            CollectionAssert.AreEqual(expected, obtained);
        }
        [TestMethod]
        public void TestNewHierarchyAssignmentSimple2()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("Plant", "mont", "dir");
            industry.CreatePlant("Plant2", "mont", "dir2");
            industry.CreateInstallation("2 - Insta1", industry.Plants.ElementAt(1));
            industry.CreateDeviceType("motor", "desc");
            industry.CreateDevice("Generador", industry.DeviceTypes.ElementAt(0));

            Component plant2 = industry.Plants.ElementAt(1);
            Component plant1 = industry.Plants.ElementAt(0);
            Component installation = industry.Installations.ElementAt(0);
            Component device = industry.Devices.ElementAt(0);

            industry.AssignChildToFatherHierarchy(plant2, plant1);
            industry.AssignChildToFatherHierarchy(device, installation);

            List<Component> expected = new List<Component>();
            expected.Add(plant2);
            expected.Add(installation);
            expected.Add(device);

            List<Component> obtained = industry.Hierarchy(plant1).ToList();
            CollectionAssert.AreEqual(expected, obtained);
        }
        [ExpectedException(typeof(ExceptionInconsistentMinMaxValue))]
        [TestMethod]
        public void FailToCreateControlVariableWithSameWarningRange()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("Plant", "mont", "dir");
            industry.CreateInstallation("instal", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "temp", 0, 4, 2, 2);
        }
        [ExpectedException(typeof(NullReferenceException))]
        [TestMethod]
        public void FailToGetHistoricValuesOfNullComponent()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.HistoricValues(null, new ControlVariable());
        }
        [ExpectedException(typeof(NullReferenceException))]
        [TestMethod]
        public void FailToGetHistoricValuesOfNullVariable()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.HistoricValues(new Component(), null);
        }
        [ExpectedException(typeof(NullReferenceException))]
        [TestMethod]
        public void FailToGetHistoricValuesOfNullVariableAndComponent()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.HistoricValues(null, null);
        }
        [TestMethod]
        public void GetHistoricValuesOfComponentWithoutHierarchy()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "mont", "dir");
            industry.CreateInstallation("insta1", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "temp", 0, 30, 10, 20);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(installation, variable, 10);
            industry.ControlVariableAssignValueInComponent(installation, variable, 15);
                       
            ICollection<AssignValue> historic = industry.HistoricValues(installation, variable);
            Assert.AreEqual(historic.ElementAt(0).Item1, 10);
            Assert.AreEqual(historic.ElementAt(1).Item1, 15);            
        }
 
        [ExpectedException(typeof(NullReferenceException))]
        [TestMethod]
        public void GetTotalWarningsWithNullComponent()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.GetTotalWarnings(null);
        }
        [TestMethod]
        public void GetTotalWarningsWithVariableWithWarning()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "mont", "dir");
            industry.CreateInstallation("insta1", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "temp", 0, 30, 10, 20);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(installation, variable, 5);            
            Assert.AreEqual(1, industry.GetTotalWarnings(installation));
        }
        [TestMethod]
        public void GetTotalWarningsWithVariableWithAlarm()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "mont", "dir");
            industry.CreateInstallation("insta1", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "temp", 0, 30, 10, 20);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(installation, variable, -1);
            Assert.AreEqual(0, industry.GetTotalWarnings(installation));
        }
        [TestMethod]
        public void GetTotalWarningsWithVariableWithNoNotice()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "mont", "dir");
            industry.CreateInstallation("insta1", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "temp", 0, 30, 10, 20);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(installation, variable, 15);
            Assert.AreEqual(0, industry.GetTotalWarnings(installation));
        }
        [TestMethod]
        public void GetTotalWarningsOfComponentWithHierarchyAndWarning()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "mont", "dir");
            industry.CreateInstallation("insta1", industry.Plants.ElementAt(0));
            industry.CreateInstallation("insta2", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            Component installation2 = industry.Installations.ElementAt(1);
            industry.AssignChildToFatherHierarchy(installation2, installation);
            industry.CreateControlVariableWithWarningRanges(installation, "temp", 0, 30, 10, 20);
            industry.CreateControlVariableWithWarningRanges(installation2, "pres", 0, 30, 10, 20);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            ControlVariable variable2 = installation2.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(installation, variable, 15);
            industry.ControlVariableAssignValueInComponent(installation2, variable2, 5);
            Assert.AreEqual(1, industry.GetTotalWarnings(installation));
        }
        [TestMethod]
        public void GetTotalWarningsOfComponentWithHierarchyAndAlarm()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "mont", "dir");
            industry.CreateInstallation("insta1", industry.Plants.ElementAt(0));
            industry.CreateInstallation("insta2", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            Component installation2 = industry.Installations.ElementAt(1);
            industry.AssignChildToFatherHierarchy(installation2, installation);
            industry.CreateControlVariableWithWarningRanges(installation, "temp", 0, 30, 10, 20);
            industry.CreateControlVariableWithWarningRanges(installation2, "pres", 0, 30, 10, 20);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            ControlVariable variable2 = installation2.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(installation, variable, 15);
            industry.ControlVariableAssignValueInComponent(installation2, variable2, -1);
            Assert.AreEqual(0, industry.GetTotalWarnings(installation));
        }
        [TestMethod]
        public void GetTotalWarningsOfComponentWithHierarchyAndNoNotice()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "mont", "dir");
            industry.CreateInstallation("insta1", industry.Plants.ElementAt(0));
            industry.CreateInstallation("insta2", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            Component installation2 = industry.Installations.ElementAt(1);
            industry.AssignChildToFatherHierarchy(installation2, installation);
            industry.CreateControlVariableWithWarningRanges(installation, "temp", 0, 30, 10, 20);
            industry.CreateControlVariableWithWarningRanges(installation2, "pres", 0, 30, 10, 20);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            ControlVariable variable2 = installation2.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(installation, variable, 15);
            industry.ControlVariableAssignValueInComponent(installation2, variable2, 15);
            Assert.AreEqual(0, industry.GetTotalWarnings(installation));
        }
        [TestMethod]
        public void TestModifyControlVariableWithWarningToRangesOk()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "mont", "dir");
            industry.CreateInstallation("insta1", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "temp", 0, 30, 10, 20);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ModifyControlVariableWithWarning(installation, variable, "temp", 0, 30, 20, 25);
            Assert.AreEqual(variable.MinRangeWarning, 20);
            Assert.AreEqual(variable.MaxRangeWarning, 25);
        }
        [ExpectedException(typeof(ExceptionInconsistentMinMaxValue))]
        [TestMethod]
        public void TestModifyControlVariableWithWarningToWarningInconsistentRanges()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "mont", "dir");
            industry.CreateInstallation("insta1", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "temp", 0, 30, 10, 20);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ModifyControlVariableWithWarning(installation, variable, "temp", 0, 30, 25, 20);
        }
        [ExpectedException(typeof(ExceptionInconsistentMinMaxValue))]
        [TestMethod]
        public void TestModifyControlVariableWithWarningToWarningInconsistentRangesWithAlarms()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "mont", "dir");
            industry.CreateInstallation("insta1", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "temp", 0, 30, 10, 20);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ModifyControlVariableWithWarning(installation, variable, "temp", 0, 30, -10, 20);
        }
        [ExpectedException(typeof(ExceptionInvalidFloatValue))]
        [TestMethod]
        public void TestModifyControlVariableWithWarningToWarningInvalidValues()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "mont", "dir");
            industry.CreateInstallation("insta1", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "temp", 0, 30, 10, 20);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ModifyControlVariableWithWarning(installation, variable, "temp", 0, 30, 20, Globals.MAX_FLOAT_VALUE + 1);
        }
        [ExpectedException(typeof(ExceptionInconsistentMinMaxValue))]
        [TestMethod]
        public void TestModifyControlVariableWithWarningToWarningWithSameRangeValues()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant", "mont", "dir");
            industry.CreateInstallation("insta1", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "temp", 0, 30, 10, 20);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ModifyControlVariableWithWarning(installation, variable, "temp", 0, 30, 20, 20);
        }
    }
}
