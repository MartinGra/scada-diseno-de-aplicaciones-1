﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SCADADomain;
namespace SCADATest
{
    [TestClass]
    public class UnitTestDeviceType
    {
        [TestMethod]
        public void TestPropertyGetName()
        {
            DeviceType devType = new DeviceType("tipo", "desc");
            Assert.AreEqual(devType.Name, "tipo");
        }
        [TestMethod]
        public void TestPropertySetName()
        {
            DeviceType devType = new DeviceType("tipo", "desc");
            devType.Name = "molino";
            Assert.AreEqual("molino", devType.Name);
        }
        [TestMethod]
        public void TestPropertyGetDescription()
        {
            DeviceType devType = new DeviceType("tipo", "desc");
            Assert.AreEqual(devType.Description, "desc");
        }
        [TestMethod]
        public void TestPropertySetDescription()
        {
            DeviceType devType = new DeviceType("tipo", "desc");
            devType.Description = "generador de energia";
            Assert.AreEqual(devType.Description, "generador de energia");
        }
        [TestMethod]
        public void TestEqualsDeviceType()
        {
            DeviceType devType1 = new DeviceType("tipo", "desc");
            DeviceType devType2 = new DeviceType("tipo", "desc");
            Assert.IsTrue(devType1.Equals(devType2));
        }
        [TestMethod]
        public void TestEqualsDeviceTypeDifferentName()
        {
            DeviceType devType1 = new DeviceType("molino", "generador de energia");
            DeviceType devType2 = new DeviceType("prensador", "modifica estado de objetos");
            Assert.IsFalse(devType1.Equals(devType2));
        }
        [TestMethod]
        public void TestEqualsDeviceTypeNull()
        {
            DeviceType devType1 = new DeviceType("molino", "generador de energia");
            DeviceType devType2 = null;
            Assert.IsFalse(devType1.Equals(devType2));
        }
        [TestMethod]
        public void TestGetHashCodeThatsEquals()
        {
            DeviceType devType1 = new DeviceType("molino", "generador de energia");
            DeviceType devType2 = new DeviceType("molino", "");
            Assert.AreEqual(devType1.GetHashCode(), devType2.GetHashCode());
        }
        [TestMethod]
        public void TestGetHashCodeThatsNotEquals()
        {
            DeviceType devType1 = new DeviceType("molino", "generador de energia");
            DeviceType devType2 = new DeviceType("prensador", "");
            Assert.AreNotEqual(devType1.GetHashCode(), devType2.GetHashCode());
        }
    }
}
