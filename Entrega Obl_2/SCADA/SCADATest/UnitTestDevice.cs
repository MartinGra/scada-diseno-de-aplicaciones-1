﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SCADADomain;

namespace SCADATest
{
    [TestClass]
    public class UnitTestDevice
    {
        [TestMethod]
        public void TestPropertyGetClassification()
        {
            DeviceType devType = new DeviceType("tipo1", "desc");
            Device device = new Device("tipo1", devType);
            Assert.AreEqual(devType, device.Classification);
        }
        [TestMethod]
        public void TestPropertySetClassificationChange()
        {
            DeviceType devType1 = new DeviceType("tipo1", "desc1");
            DeviceType devType2 = new DeviceType("tipo2", "desc2");
            Device device = new Device("tipo1", devType1);
            device.Classification = devType2;
            Assert.AreEqual(devType2, device.Classification);
        }
    }
}
