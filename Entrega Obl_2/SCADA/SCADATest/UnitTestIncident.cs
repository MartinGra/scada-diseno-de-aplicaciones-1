﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SCADADomain;
using SCADAException;

namespace SCADATest
{
    [TestClass]
    public class UnitTestIncident
    {
        [TestMethod]
        public void TestIncident()
        {
            Incident testIncident = new Incident();
            Assert.AreEqual(0, testIncident.Severity);
            Assert.AreEqual(null, testIncident.Description);
        }
        [TestMethod]
        public void TestIncidentWithParams()
        {
            Component comp = new Device();
            Incident testIncident = new Incident("5", "descripcion");
            Assert.AreEqual(5, testIncident.Severity);
            Assert.AreEqual("descripcion", testIncident.Description);
        }
        [ExpectedException(typeof(ExceptionEmptyString))]
        [TestMethod]
        public void TestIncidentFailEmptyDescription()
        {
            Component comp = new Device("device", new DeviceType("type1","desc"));
            Incident testIncident = new Incident("5", "");
        }
        [ExpectedException(typeof(FormatException))]
        [TestMethod]
        public void TestIncidentFailSeverityNonNumeric()
        {
            Component comp = new Device("device", new DeviceType("type1", "desc"));
            Incident testIncident = new Incident("asdb", "descripcion");
        }
    }
}
