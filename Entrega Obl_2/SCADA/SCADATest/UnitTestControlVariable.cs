﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SCADADomain;
using System.Linq;

namespace SCADATest
{
    [TestClass]
    public class UnitTestControlVariable
    {
        [TestMethod]
        public void TestEqualsControlVariable()
        {
            ControlVariable newControlVariable = new ControlVariable("Variable Name", 0, 10);
            ControlVariable secondControlVarible = new ControlVariable("Variable Name", 0, 10);
            Assert.IsTrue(newControlVariable.Equals(secondControlVarible));
        }
        [TestMethod]
        public void TestEqualsControlVariableDifferentName()
        {
            ControlVariable newControlVariable = new ControlVariable("Variable Name", 0, 10);
            ControlVariable secondControlVarible = new ControlVariable("Different Variable Name", 0, 10);
            Assert.IsFalse(newControlVariable.Equals(secondControlVarible));
        }
        [TestMethod]
        public void TestEqualsControlVariableNull()
        {
            ControlVariable newControlVariable = new ControlVariable("Variable Name", 0, 10);
            ControlVariable secondControlVarible = null;
            Assert.IsFalse(newControlVariable.Equals(secondControlVarible));
        }
        [TestMethod]
        public void TestEqualsControlVariableDifferentMinValue()
        {
            ControlVariable newControlVariable = new ControlVariable("Variable Name", 0, 10);
            ControlVariable secondControlVarible = new ControlVariable("Variable Name", 5, 10);
            Assert.IsTrue(newControlVariable.Equals(secondControlVarible));
        }
        [TestMethod]
        public void TestEqualsControlVariableDifferentMaxValue()
        {
            ControlVariable newControlVariable = new ControlVariable("Variable Name", 0, 10);
            ControlVariable secondControlVarible = new ControlVariable("Variable Name", 0, 15);
            Assert.IsTrue(newControlVariable.Equals(secondControlVarible));
        }
        [TestMethod]
        public void TestViewHistoricControlValues()
        {
            ControlVariable newControlVariable = new ControlVariable("Variable Name", 0, 10);
            newControlVariable.UpdateValue(3, DateTime.Now);
            newControlVariable.UpdateValue(4, DateTime.Now);
            newControlVariable.UpdateValue(-1f, DateTime.Now);
            Assert.IsTrue(3 == newControlVariable.ValueDatePair.ElementAt(0).Item1);
            Assert.IsTrue(4 == newControlVariable.ValueDatePair.ElementAt(1).Item1);
            Assert.IsTrue(-1f == newControlVariable.ValueDatePair.ElementAt(2).Item1);
        }
        [TestMethod]
        public void TestGetHashCodeThatsEquals()
        {
            ControlVariable variable1 = new ControlVariable("Temperatura", 0, 100);
            ControlVariable Variable2 = new ControlVariable("Temperatura", 0, 200);
            Assert.AreEqual(variable1.GetHashCode(), Variable2.GetHashCode());
        }
        [TestMethod]
        public void TestGetHashCodeThatsNotEquals()
        {
            ControlVariable variable1 = new ControlVariable("Temperatura", 0, 100);
            ControlVariable Variable2 = new ControlVariable("Carga", 0, 900);
            Assert.AreNotEqual(variable1.GetHashCode(), Variable2.GetHashCode());
        }
    }
}
