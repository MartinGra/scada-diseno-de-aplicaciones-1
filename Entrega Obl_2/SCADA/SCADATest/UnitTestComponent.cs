﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SCADADomain;
using SCADAException;
using SCADAFacade;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace SCADATest
{
    [TestClass]
    public class UnitTestComponent
    {
        private IndustrialPlant NewIndustrialPlantImp()
        {
            return IndustrialPlantDBImp.ObtainInstance;
        }

        private void EmptyDataBase(IndustrialPlant industrialPlant)
        {
            using (var db = new Context())
            {
                foreach (var entity in db.Components)
                    db.Components.Remove(entity);

                foreach (var entity in db.ControlVariables)
                    db.ControlVariables.Remove(entity);

                foreach (var entity in db.DeviceTypes)
                    db.DeviceTypes.Remove(entity);

                foreach (var entity in db.Incidents)
                    db.Incidents.Remove(entity);

                foreach (var entity in db.AssignValues)
                    db.AssignValues.Remove(entity);

                db.SaveChanges();
            }
        }

        [TestMethod]
        public void TestPropertyGetFather()
        {
            Component component = new Installation();
            Assert.IsNull(component.Father);
        }
        [TestMethod]
        public void TestPropertySetFather()
        {
            Component father = new Installation();
            Component son = new Installation();
            son.Father = father;
            Assert.AreEqual(father, son.Father);
        }

        [TestMethod]
        public void TestPropertyGetControlVariables()
        {
            Component comp = new Installation();
            Assert.AreEqual(0, comp.ControlVariables.Count);
        }

        [TestMethod]
        public void TestPropertySetControlVariables()
        {
            Component comp = new Installation();
            Collection<ControlVariable> variables = new Collection<ControlVariable>
            {
                new ControlVariable("temperatura", -10, 120)
            };
            comp.ControlVariables = variables;
            Assert.AreEqual(variables, comp.ControlVariables);
        }
        [TestMethod]
        public void TestPropertyGetName()
        {
            Component comp = new Installation("extraccion",new Plant());
            Assert.AreEqual(comp.Name, "extraccion");
        }
        [TestMethod]
        public void TestPropertySetName()
        {
            Component comp = new Installation();
            comp.Name = "molienda";
            Assert.AreEqual(comp.Name, "molienda");
        }
 
        [TestMethod]
        public void TestAddControlVariable()
        {
            Component comp = new Installation();
            ControlVariable variable = new ControlVariable("Temperatura", 0, 100);
            comp.AddControlVariable(variable);
            Assert.AreEqual(comp.ControlVariables.ElementAt(0), variable);
        }
        [ExpectedException(typeof(Exception))]
        [TestMethod]
        public void TestAddControlVariableNull()
        {
            Component comp = new Installation();
            comp.ControlVariables = null;
            ControlVariable variable = null;
            comp.AddControlVariable(variable);
        }
        [TestMethod]
        public void TestGetTotalAlarms()
        {
            Component comp = new Installation();
            Assert.AreEqual(comp.CurrentAlarms(), 0);
        }
        [TestMethod]
        public void TestEquals()
        {
            Component comp = new Installation();
            Assert.AreEqual(false, comp.Equals(null));
        }
        [TestMethod]
        public void TestValidModifyNameOfControlVariable()
        {
            Component comp = new Installation();
            comp.AddControlVariable(new ControlVariable("temperatura", 0, 100));
            comp.ModifyNameOfControlVariable(comp.ControlVariables.ElementAt(0),"presion");
            Assert.AreEqual("presion", comp.ControlVariables.ElementAt(0).Name);
        }

        [TestMethod]
        public void TestModifyNameOfControlVariableWithSameName()
        {
            Component comp = new Installation();
            comp.AddControlVariable(new ControlVariable("temperatura", 0, 100));
            comp.ModifyNameOfControlVariable(comp.ControlVariables.ElementAt(0), "temperatura");
            Assert.AreEqual("temperatura", comp.ControlVariables.ElementAt(0).Name);
        }
        [ExpectedException (typeof(ExceptionEmptyString))]
        [TestMethod]
        public void TestFailToModifyNameOfControlVariableWithEmptyName()
        {
            Component comp = new Installation();
            comp.AddControlVariable(new ControlVariable("temperatura", 0, 100));
            comp.ModifyNameOfControlVariable(comp.ControlVariables.ElementAt(0), "");
        }
        [ExpectedException(typeof(ExceptionExistingControlVariableName))]
        [TestMethod]
        public void TestFailToModifyNameOfControlVariableWithExistingName()
        {
            Component comp = new Installation();
            comp.AddControlVariable(new ControlVariable("temperatura", 0, 100));
            comp.AddControlVariable(new ControlVariable("presion", 30, 600));
            comp.ModifyNameOfControlVariable(comp.ControlVariables.ElementAt(0), "presion");
        }
        [TestMethod]
        public void TestValidModifyRangesOfControlVariable()
        {
            Component comp = new Installation();    
            ControlVariable cv = new ControlVariable("temperatura", 0, 100);
            comp.AddControlVariable(cv);
            comp.ModifyRangesOfControlVariable(comp.ControlVariables.ElementAt(0), 100, 300);
            Assert.AreEqual(comp.ControlVariables.ElementAt(0).MaxRange, 300);
        }
        [ExpectedException (typeof (ExceptionInconsistentMinMaxValue))]
        [TestMethod]
        public void TestFailToModifyRangesOfControlVariableWithNotConsistentRanges()
        {
            Component comp = new Installation();
            comp.AddControlVariable(new ControlVariable("temperatura", 0, 100));
            comp.ModifyRangesOfControlVariable(comp.ControlVariables.ElementAt(0), 300, 100);
        }
        [ExpectedException(typeof(ExceptionInvalidFloatValue))]
        [TestMethod]
        public void TestFailToModifyRangesOfControlVariableWithInvalidUpperRange()
        {
            Component comp = new Installation();
            comp.AddControlVariable(new ControlVariable("temperatura", 0, 100));
            comp.ModifyRangesOfControlVariable(comp.ControlVariables.ElementAt(0), 300, 99999999999999);
        }
        [ExpectedException(typeof(ExceptionInvalidFloatValue))]
        [TestMethod]
        public void TestFailToModifyRangesOfControlVariableWithInvalidLowerRange()
        {
            Component comp = new Installation();
            comp.AddControlVariable(new ControlVariable("temperatura", 0, 100));
            comp.ModifyRangesOfControlVariable(comp.ControlVariables.ElementAt(0), -999999999999, 3);
        }
        [ExpectedException(typeof(ExceptionInvalidFloatValue))]
        [TestMethod]
        public void TestFailToModifyRangesOfControlVariableWithInvalidRanges()
        {
            Component comp = new Installation();
            comp.AddControlVariable(new ControlVariable("temperatura", 0, 100));
            comp.ModifyRangesOfControlVariable(comp.ControlVariables.ElementAt(0), -999999999999, 99999999999999);
        }

        [TestMethod]
        public void TestModifyRangesOfVariableThatHasPreviousAlarmWithNewRangeGreen()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant1", "mont", "dir");
            industry.CreateInstallation("insta", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableInComponent(installation, "Temperatura", 0, 100);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(installation, variable, 120);
            installation.ModifyRangesOfControlVariable(variable, 100, 140);
            Assert.AreEqual(0, industry.GetTotalAlarms(installation));            
        }
        [TestMethod]
        public void TestModifyRangesOfVariableThatHasPreviousAlarmWithNewRangeRed()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant1", "mont", "dir");
            industry.CreateInstallation("insta", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableInComponent(installation, "Temperatura", 0, 100);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(installation, variable, 120);
            installation.ModifyRangesOfControlVariable(variable, 0, 80);
            Assert.AreEqual(1, industry.GetTotalAlarms(installation));
        }
        [TestMethod]
        public void TestModifyRangesOfVariableThatHasNoPreviousAlarmWithNewRangeGreen()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant1", "mont", "dir");
            industry.CreateInstallation("insta", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableInComponent(installation, "Temperatura", 0, 200);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(installation, variable, 120);
            installation.ModifyRangesOfControlVariable(variable, -10, 500);
            Assert.AreEqual(0, industry.GetTotalAlarms(installation));
        }
        [TestMethod]
        public void TestModifyRangesOfVariableThatHasNoPreviousAlarmWithNewRangeRed()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant1", "mont", "dir");
            industry.CreateInstallation("insta", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableInComponent(installation, "Temperatura", 0, 100);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(installation, variable, 80);
            installation.ModifyRangesOfControlVariable(variable, 0, 50);
            Assert.AreEqual(1, industry.GetTotalAlarms(installation));           
        }

        [TestMethod]
        public void TestModifyRangesOfVariableThatHasNoPreviousAlarmWithNewRangeRedLower()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant1", "mont", "dir");
            industry.CreateInstallation("insta", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableInComponent(installation, "Temperatura", 0, 100);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(installation, variable, 60);
            installation.ModifyRangesOfControlVariable(variable, -1300, -100);
            Assert.AreEqual(1, industry.GetTotalAlarms(installation));
        }

        [TestMethod]
        public void TestModifyControlVariableName()
        {
            Component comp = new Installation();
            comp.ControlVariables.Add(new ControlVariable("var",0,20));
            comp.ModifyNameOfControlVariable(comp.ControlVariables.ElementAt(0), "newName");
            Assert.AreEqual("newName", comp.ControlVariables.ElementAt(0).Name);
        }

        [TestMethod]
        public void TestModifyControlVaribleSameName()
        {
            Component comp = new Installation();
            comp.ControlVariables.Add(new ControlVariable("var", 0, 20));
            comp.ModifyNameOfControlVariable(comp.ControlVariables.ElementAt(0), "var");
            Assert.AreEqual("var", comp.ControlVariables.ElementAt(0).Name);
        }
        [ExpectedException(typeof(ExceptionEmptyString))]
        [TestMethod]
        public void TestModifyControlVaribleEmptyName()
        {
            Component comp = new Installation();
            comp.ControlVariables.Add(new ControlVariable("var", 0, 20));
            comp.ModifyNameOfControlVariable(comp.ControlVariables.ElementAt(0), "  ");
        }
        [ExpectedException(typeof(ExceptionExistingControlVariableName))]
        [TestMethod]
        public void TestModifyControlVaribleExistingName()
        {
            Component comp = new Installation();
            comp.ControlVariables.Add(new ControlVariable("var", 0, 20));
            comp.ControlVariables.Add(new ControlVariable("var2", 0, 20));
            comp.ModifyNameOfControlVariable(comp.ControlVariables.ElementAt(0), "var2");
        }
        
        [TestMethod]
        public void TestGetWarningsFromSingleControlVariableWithWarning() {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant1", "mont", "dir");
            industry.CreateInstallation("insta", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "Temperatura", 0, 100,30,60);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(installation, variable, 20);
            Assert.AreEqual(1, industry.GetTotalWarnings(installation));
        }
        [TestMethod]
        public void TestGetWarningsFromSingleVariableThatsNotAllowedToHaveWarnings()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant1", "mont", "dir");
            industry.CreateInstallation("insta", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "Temperatura", 0, 100, 0, 60);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(installation, variable, 20);
            Assert.AreEqual(0, industry.GetTotalWarnings(installation));
        }
        [TestMethod]
        public void TestGetWarningsFromSingleVariableWithValueInLowerLimitExpectedRange()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant1", "mont", "dir");
            industry.CreateInstallation("insta", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "Temperatura", 0, 100, 30, 60);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(installation, variable, 30);
            Assert.AreEqual(0, industry.GetTotalWarnings(installation));
        }
        [TestMethod]
        public void TestGetWarningsFromSingleVariableWithValueInUpperLimitExpectedRange()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant1", "mont", "dir");
            industry.CreateInstallation("insta", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "Temperatura", 0, 100, 30, 60);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(installation, variable, 60);
            Assert.AreEqual(0, industry.GetTotalWarnings(installation));
        }
        [TestMethod]
        public void TestGetWarningsFromSingleVariableWithValueInLowerLimitWarningRange()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant1", "mont", "dir");
            industry.CreateInstallation("insta", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "Temperatura", 0, 100, 30, 60);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(installation, variable, 29);
            Assert.AreEqual(1, industry.GetTotalWarnings(installation));
        }
        [TestMethod]
        public void TestGetWarningsFromSingleVariableWithValueInUpperLimitWarningRange()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant1", "mont", "dir");
            industry.CreateInstallation("insta", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "Temperatura", 0, 100, 30, 60);
            ControlVariable variable = installation.ControlVariables.ElementAt(0);
            industry.ControlVariableAssignValueInComponent(installation, variable, 61);
            Assert.AreEqual(1, industry.GetTotalWarnings(installation));
        }

        [TestMethod]
        public void TestGetWarningsFromVariousVariablesSimple()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant1", "mont", "dir");
            industry.CreateInstallation("insta", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "Temperatura", 0, 100, 30, 60);
            industry.CreateControlVariableWithWarningRanges(installation, "Presion", -10, 30, -10, 20);
            ControlVariable variable1 = installation.ControlVariables.ElementAt(0);
            ControlVariable variable2 = installation.ControlVariables.ElementAt(1);

            industry.ControlVariableAssignValueInComponent(installation, variable1, 20);
            industry.ControlVariableAssignValueInComponent(installation, variable2, 25);
            Assert.AreEqual(1, industry.GetTotalWarnings(installation));
        }
        [TestMethod]
        public void TestGetWarningsFromVariousVariablesComplex1()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant1", "mont", "dir");
            industry.CreateInstallation("insta", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);

            industry.CreateControlVariableWithWarningRanges(installation, "Temperatura", 0, 100, 30, 60);
            industry.CreateControlVariableWithWarningRanges(installation, "Presion", -10, 30, -10, 20);
            industry.CreateControlVariableWithWarningRanges(installation, "Revoluciones", 0, 40, 5, 30);
            industry.CreateControlVariableWithWarningRanges(installation, "Peso", 5, 70, 15, 50);

            ControlVariable variable1 = installation.ControlVariables.ElementAt(0);
            ControlVariable variable2 = installation.ControlVariables.ElementAt(1);
            ControlVariable variable3 = installation.ControlVariables.ElementAt(2);
            ControlVariable variable4 = installation.ControlVariables.ElementAt(3);

            industry.ControlVariableAssignValueInComponent(installation, variable1, 20); //Warning
            industry.ControlVariableAssignValueInComponent(installation, variable2, 25); //Only generates alarms
            industry.ControlVariableAssignValueInComponent(installation, variable3, 20); //Expected
            industry.ControlVariableAssignValueInComponent(installation, variable4, -4); //Alarm

            Assert.AreEqual(1, industry.GetTotalWarnings(installation));
        }
        [TestMethod]
        public void TestGetWarningsFromVariousVariablesComplex2()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant1", "mont", "dir");
            industry.CreateInstallation("insta", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);

            industry.CreateControlVariableWithWarningRanges(installation, "Temperatura", 0, 100, 30, 60);
            industry.CreateControlVariableWithWarningRanges(installation, "Presion", -10, 30, -10, 20);
            industry.CreateControlVariableWithWarningRanges(installation, "Revoluciones", 0, 40, 5, 30);
            industry.CreateControlVariableWithWarningRanges(installation, "Peso", 5, 70, 15, 50);

            ControlVariable variable1 = installation.ControlVariables.ElementAt(0);
            ControlVariable variable2 = installation.ControlVariables.ElementAt(1);
            ControlVariable variable3 = installation.ControlVariables.ElementAt(2);
            ControlVariable variable4 = installation.ControlVariables.ElementAt(3);

            industry.ControlVariableAssignValueInComponent(installation, variable1, 20); //Warning
            industry.ControlVariableAssignValueInComponent(installation, variable2, 25); //Only generates alarms
            industry.ControlVariableAssignValueInComponent(installation, variable3, 35); //Warning
            industry.ControlVariableAssignValueInComponent(installation, variable4, -4); //Alarm

            Assert.AreEqual(2, industry.GetTotalWarnings(installation));
        }
        [TestMethod]
        public void TestGetAlarmsFromVariousVariablesSimple()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant1", "mont", "dir");
            industry.CreateInstallation("insta", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);
            industry.CreateControlVariableWithWarningRanges(installation, "Temperatura", 0, 100, 30, 60);
            industry.CreateControlVariableWithWarningRanges(installation, "Presion", -10, 30, -10, 20);
            ControlVariable variable1 = installation.ControlVariables.ElementAt(0);
            ControlVariable variable2 = installation.ControlVariables.ElementAt(1);

            industry.ControlVariableAssignValueInComponent(installation, variable1, 20);//Warning
            industry.ControlVariableAssignValueInComponent(installation, variable2, -20); //Alarm

            Assert.AreEqual(1, industry.GetTotalAlarms(installation));
        }
        [TestMethod]
        public void TestGetAlarmsFromVariousVariablesComplex1()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant1", "mont", "dir");
            industry.CreateInstallation("insta", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);

            industry.CreateControlVariableWithWarningRanges(installation, "Temperatura", 0, 100, 30, 60);
            industry.CreateControlVariableWithWarningRanges(installation, "Presion", -10, 30, -10, 20);
            industry.CreateControlVariableWithWarningRanges(installation, "Revoluciones", 0, 40, 5, 30);
            industry.CreateControlVariableWithWarningRanges(installation, "Peso", 5, 70, 15, 50);

            ControlVariable variable1 = installation.ControlVariables.ElementAt(0);
            ControlVariable variable2 = installation.ControlVariables.ElementAt(1);
            ControlVariable variable3 = installation.ControlVariables.ElementAt(2);
            ControlVariable variable4 = installation.ControlVariables.ElementAt(3);

            industry.ControlVariableAssignValueInComponent(installation, variable1, 20); //Warning
            industry.ControlVariableAssignValueInComponent(installation, variable2, -20); //Alarm
            industry.ControlVariableAssignValueInComponent(installation, variable3, 20); //Expected
            industry.ControlVariableAssignValueInComponent(installation, variable4, -4); //Alarm

            Assert.AreEqual(2, industry.GetTotalAlarms(installation));
        }
        [TestMethod]
        public void TestGetAlarmsFromVariousVariablesComplex2()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            industry.CreatePlant("plant1", "mont", "dir");
            industry.CreateInstallation("insta", industry.Plants.ElementAt(0));
            Component installation = industry.Installations.ElementAt(0);

            industry.CreateControlVariableWithWarningRanges(installation, "Temperatura", 0, 100, 30, 60);
            industry.CreateControlVariableWithWarningRanges(installation, "Presion", -10, 30, -10, 20);
            industry.CreateControlVariableWithWarningRanges(installation, "Revoluciones", 0, 40, 5, 30);
            industry.CreateControlVariableWithWarningRanges(installation, "Peso", 5, 70, 15, 50);

            ControlVariable variable1 = installation.ControlVariables.ElementAt(0);
            ControlVariable variable2 = installation.ControlVariables.ElementAt(1);
            ControlVariable variable3 = installation.ControlVariables.ElementAt(2);
            ControlVariable variable4 = installation.ControlVariables.ElementAt(3);

            industry.ControlVariableAssignValueInComponent(installation, variable1, 20); //Warning
            industry.ControlVariableAssignValueInComponent(installation, variable2, 25); //Only generates alarms
            industry.ControlVariableAssignValueInComponent(installation, variable3, 35); //Warning
            industry.ControlVariableAssignValueInComponent(installation, variable4, -4); //Alarm

            Assert.AreEqual(1, industry.GetTotalAlarms(installation));
        }

        [TestMethod]
        public void TestGetHashCode()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            Component c = new Component();
            Assert.IsNotNull(c.GetHashCode());
        }

        [TestMethod]
        public void TestGetHistorics()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            Component c = new Component();
            Assert.IsNotNull(c.HistoricValues(new ControlVariable()));
        }

        [TestMethod]
        public void TestModifyWarningRangesOfControlVariable()
        {
            IndustrialPlant industry = NewIndustrialPlantImp(); EmptyDataBase(industry);
            Component c = new Component();
            ControlVariable cv = new ControlVariable("1", -5, 20, 0, 15);
            c.ModifyWarningRangesOfControlVariable(cv, 1, 10);
            Assert.AreEqual(1, cv.MinRangeWarning);
            Assert.AreEqual(10, cv.MaxRangeWarning);
        }
    }
}
