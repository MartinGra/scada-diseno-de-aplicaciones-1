﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SCADADomain;
using SCADAException;
using System.Collections.Generic;

namespace SCADATest
{
    [TestClass]
    public class UnitTestGlobals
    {
        [TestMethod]
        public void TestIncidentsPlainTextPath()
        {
            Assert.IsNotNull(Globals.IncidentsPlainTextPath());
        }
        [TestMethod]
        public void TestValidLineFromTxt()
        {
            Component c = new Component();
            Assert.IsTrue(Globals.ValidLineFromTxt("1_1_1_"+c.ComponentId.ToString(), c));
            Assert.IsFalse(Globals.ValidLineFromTxt("1_11", new Component()));
            Assert.IsFalse(Globals.ValidLineFromTxt("1_1_1_1", new Component()));
        }
        [TestMethod]
        public void TestEmptyStringWithEmptyString()
        {
            String emptyStr = "";
            Assert.IsTrue(Globals.EmptyString(emptyStr));
        }
        [TestMethod]
        public void TestEmptyStringWithNonEmptyString()
        {
            String nonEmptyStr = "dispositivo1";
            Assert.IsFalse(Globals.EmptyString(nonEmptyStr));
        }
        [TestMethod]
        public void TestConsistentMinMaxValuesWithValidValues()
        {
            float minValue = -4000f;
            float maxValue = 35023.231f;
            Assert.IsTrue(Globals.ConsistentMinMaxValues(minValue, maxValue));
        }
       [TestMethod]
       public void TestConsistentMinMaxValuesWithInvalidValues()
        {
            float minValue = 30000.04f;
            float maxValue = -30000.88f;
            Assert.IsFalse(Globals.ConsistentMinMaxValues(minValue, maxValue));
        }
       [TestMethod]
       public void TestConsistentMinMaxValuesWithSameValues()
        {
            float minValue = 2340f;
            float maxValue = 2340f;
            Assert.IsTrue(Globals.ConsistentMinMaxValues(minValue, maxValue));
        }
        [TestMethod]
        public void TestValidControlFloatRangeWithValidFloat()
        {
            float rangeNumber = 0f;
            Assert.IsTrue(Globals.ValidControlFloatRange(rangeNumber));
        }
        [TestMethod]
        public void TestValidControlFloatRangeWithInvalidFloat()
        {
            float rangeNumber = -99999999999f;
            Assert.IsFalse(Globals.ValidControlFloatRange(rangeNumber));
        }
    }
}
