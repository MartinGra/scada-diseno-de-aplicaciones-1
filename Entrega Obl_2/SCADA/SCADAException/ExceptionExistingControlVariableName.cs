﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCADAException
{
    public class ExceptionExistingControlVariableName : Exception
    {
        public ExceptionExistingControlVariableName(string message) : base(message)
        {
        }
    }
}