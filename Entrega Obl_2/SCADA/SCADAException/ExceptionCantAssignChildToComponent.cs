﻿using System;
using System.Runtime.Serialization;

namespace SCADAException
{
    [Serializable]
    public class ExceptionCantAssignChildToComponent : Exception
    {
        public ExceptionCantAssignChildToComponent(string message) : base(message)
        {
        }
        
    }
}