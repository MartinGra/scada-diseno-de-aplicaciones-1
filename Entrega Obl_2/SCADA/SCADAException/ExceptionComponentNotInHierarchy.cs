﻿using System;
using System.Runtime.Serialization;

namespace SCADAException
{
    [Serializable]
    public class ExceptionComponentNotInHierarchy : Exception
    {

        public ExceptionComponentNotInHierarchy(string message) : base(message)
        {
        }
    }
}