﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCADAException
{
    public class ExceptionEmptyString : Exception
    {
        public ExceptionEmptyString()
        {
        }

        public ExceptionEmptyString(string message) : base(message)
        {
        }
    }
}
