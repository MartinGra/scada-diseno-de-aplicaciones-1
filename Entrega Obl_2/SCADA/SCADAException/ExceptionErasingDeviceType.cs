﻿using System;

namespace SCADAException
{
    [Serializable]
    public class ExceptionErasingDeviceType : Exception
    {
        public ExceptionErasingDeviceType(string message) : base(message)
        {
        }

    }
}
