﻿using System;
using System.Runtime.Serialization;

namespace SCADAException
{
    [Serializable]
    public class ExceptionInvalidFloatValue : Exception
    {
        public ExceptionInvalidFloatValue(string message) : base(message)
        {
        }

    }
}