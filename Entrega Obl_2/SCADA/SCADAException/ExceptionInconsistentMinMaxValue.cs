﻿using System;

namespace SCADAException
{
    public class ExceptionInconsistentMinMaxValue : Exception
    {
        public ExceptionInconsistentMinMaxValue(string message) : base(message)
        {
        }
    }
}