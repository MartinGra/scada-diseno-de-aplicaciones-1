﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCADAException
{
    public class ExceptionCantAssignComponentToItself : Exception
    {
        public ExceptionCantAssignComponentToItself(string message) : base(message)
        {
        }
    }
}