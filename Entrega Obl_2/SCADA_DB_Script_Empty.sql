USE [master]
GO
/****** Object:  Database [ScadaDB]    Script Date: 6/16/2016 8:59:50 PM ******/
CREATE DATABASE [ScadaDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ScadaDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\ScadaDB.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ScadaDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\ScadaDB_log.ldf' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ScadaDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ScadaDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ScadaDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ScadaDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ScadaDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ScadaDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ScadaDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [ScadaDB] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [ScadaDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ScadaDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ScadaDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ScadaDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ScadaDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ScadaDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ScadaDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ScadaDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ScadaDB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [ScadaDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ScadaDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ScadaDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ScadaDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ScadaDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ScadaDB] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [ScadaDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ScadaDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ScadaDB] SET  MULTI_USER 
GO
ALTER DATABASE [ScadaDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ScadaDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ScadaDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ScadaDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [ScadaDB] SET DELAYED_DURABILITY = DISABLED 
GO
USE [ScadaDB]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 6/16/2016 8:59:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AssingValues]    Script Date: 6/16/2016 8:59:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssingValues](
	[AssignValueId] [int] IDENTITY(1,1) NOT NULL,
	[Item1] [real] NOT NULL,
	[Item2] [datetime] NOT NULL,
	[ControlVariable_ControlVariableId] [int] NULL,
 CONSTRAINT [PK_dbo.AssingValues] PRIMARY KEY CLUSTERED 
(
	[AssignValueId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Components]    Script Date: 6/16/2016 8:59:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Components](
	[ComponentId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[HierarchyLevel] [int] NOT NULL,
	[Father_ComponentId] [int] NULL,
 CONSTRAINT [PK_dbo.Components] PRIMARY KEY CLUSTERED 
(
	[ComponentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ControlVariables]    Script Date: 6/16/2016 8:59:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ControlVariables](
	[ControlVariableId] [int] IDENTITY(1,1) NOT NULL,
	[MaxRange] [real] NOT NULL,
	[MinRange] [real] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[MinRangeWarning] [real] NOT NULL,
	[MaxRangeWarning] [real] NOT NULL,
	[Component_ComponentId] [int] NULL,
 CONSTRAINT [PK_dbo.ControlVariables] PRIMARY KEY CLUSTERED 
(
	[ControlVariableId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Devices]    Script Date: 6/16/2016 8:59:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Devices](
	[ComponentId] [int] NOT NULL,
	[Classification_DeviceTypeId] [int] NULL,
 CONSTRAINT [PK_dbo.Devices] PRIMARY KEY CLUSTERED 
(
	[ComponentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DeviceTypes]    Script Date: 6/16/2016 8:59:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeviceTypes](
	[DeviceTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.DeviceTypes] PRIMARY KEY CLUSTERED 
(
	[DeviceTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Incidents]    Script Date: 6/16/2016 8:59:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Incidents](
	[IncidentId] [int] IDENTITY(1,1) NOT NULL,
	[OccurrenceDate] [datetime] NOT NULL,
	[Severity] [int] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[Component_ComponentId] [int] NULL,
 CONSTRAINT [PK_dbo.Incidents] PRIMARY KEY CLUSTERED 
(
	[IncidentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Installations]    Script Date: 6/16/2016 8:59:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Installations](
	[ComponentId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Installations] PRIMARY KEY CLUSTERED 
(
	[ComponentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Plants]    Script Date: 6/16/2016 8:59:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Plants](
	[ComponentId] [int] NOT NULL,
	[Address] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Plants] PRIMARY KEY CLUSTERED 
(
	[ComponentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Index [IX_ControlVariable_ControlVariableId]    Script Date: 6/16/2016 8:59:50 PM ******/
CREATE NONCLUSTERED INDEX [IX_ControlVariable_ControlVariableId] ON [dbo].[AssingValues]
(
	[ControlVariable_ControlVariableId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Father_ComponentId]    Script Date: 6/16/2016 8:59:50 PM ******/
CREATE NONCLUSTERED INDEX [IX_Father_ComponentId] ON [dbo].[Components]
(
	[Father_ComponentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Component_ComponentId]    Script Date: 6/16/2016 8:59:50 PM ******/
CREATE NONCLUSTERED INDEX [IX_Component_ComponentId] ON [dbo].[ControlVariables]
(
	[Component_ComponentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Classification_DeviceTypeId]    Script Date: 6/16/2016 8:59:50 PM ******/
CREATE NONCLUSTERED INDEX [IX_Classification_DeviceTypeId] ON [dbo].[Devices]
(
	[Classification_DeviceTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ComponentId]    Script Date: 6/16/2016 8:59:50 PM ******/
CREATE NONCLUSTERED INDEX [IX_ComponentId] ON [dbo].[Devices]
(
	[ComponentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Component_ComponentId]    Script Date: 6/16/2016 8:59:50 PM ******/
CREATE NONCLUSTERED INDEX [IX_Component_ComponentId] ON [dbo].[Incidents]
(
	[Component_ComponentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ComponentId]    Script Date: 6/16/2016 8:59:50 PM ******/
CREATE NONCLUSTERED INDEX [IX_ComponentId] ON [dbo].[Installations]
(
	[ComponentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ComponentId]    Script Date: 6/16/2016 8:59:50 PM ******/
CREATE NONCLUSTERED INDEX [IX_ComponentId] ON [dbo].[Plants]
(
	[ComponentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AssingValues]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AssingValues_dbo.ControlVariables_ControlVariable_ControlVariableId] FOREIGN KEY([ControlVariable_ControlVariableId])
REFERENCES [dbo].[ControlVariables] ([ControlVariableId])
GO
ALTER TABLE [dbo].[AssingValues] CHECK CONSTRAINT [FK_dbo.AssingValues_dbo.ControlVariables_ControlVariable_ControlVariableId]
GO
ALTER TABLE [dbo].[Components]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Components_dbo.Components_Father_ComponentId] FOREIGN KEY([Father_ComponentId])
REFERENCES [dbo].[Components] ([ComponentId])
GO
ALTER TABLE [dbo].[Components] CHECK CONSTRAINT [FK_dbo.Components_dbo.Components_Father_ComponentId]
GO
ALTER TABLE [dbo].[ControlVariables]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ControlVariables_dbo.Components_Component_ComponentId] FOREIGN KEY([Component_ComponentId])
REFERENCES [dbo].[Components] ([ComponentId])
GO
ALTER TABLE [dbo].[ControlVariables] CHECK CONSTRAINT [FK_dbo.ControlVariables_dbo.Components_Component_ComponentId]
GO
ALTER TABLE [dbo].[Devices]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Devices_dbo.Components_ComponentId] FOREIGN KEY([ComponentId])
REFERENCES [dbo].[Components] ([ComponentId])
GO
ALTER TABLE [dbo].[Devices] CHECK CONSTRAINT [FK_dbo.Devices_dbo.Components_ComponentId]
GO
ALTER TABLE [dbo].[Devices]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Devices_dbo.DeviceTypes_Classification_DeviceTypeId] FOREIGN KEY([Classification_DeviceTypeId])
REFERENCES [dbo].[DeviceTypes] ([DeviceTypeId])
GO
ALTER TABLE [dbo].[Devices] CHECK CONSTRAINT [FK_dbo.Devices_dbo.DeviceTypes_Classification_DeviceTypeId]
GO
ALTER TABLE [dbo].[Incidents]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Incidents_dbo.Components_Component_ComponentId] FOREIGN KEY([Component_ComponentId])
REFERENCES [dbo].[Components] ([ComponentId])
GO
ALTER TABLE [dbo].[Incidents] CHECK CONSTRAINT [FK_dbo.Incidents_dbo.Components_Component_ComponentId]
GO
ALTER TABLE [dbo].[Installations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Installations_dbo.Components_ComponentId] FOREIGN KEY([ComponentId])
REFERENCES [dbo].[Components] ([ComponentId])
GO
ALTER TABLE [dbo].[Installations] CHECK CONSTRAINT [FK_dbo.Installations_dbo.Components_ComponentId]
GO
ALTER TABLE [dbo].[Plants]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Plants_dbo.Components_ComponentId] FOREIGN KEY([ComponentId])
REFERENCES [dbo].[Components] ([ComponentId])
GO
ALTER TABLE [dbo].[Plants] CHECK CONSTRAINT [FK_dbo.Plants_dbo.Components_ComponentId]
GO
USE [master]
GO
ALTER DATABASE [ScadaDB] SET  READ_WRITE 
GO
